module.exports = {
    pattern: '^(develop|master)$|^(feature|hotfix|bugfix|chores)/.+$',
    errorMsg:
        '🤨 La branche que tu essaies de pusher ne respecte pas nos conventions, tu peux la renommer avec `git branch -m <nom-actuel> <nouveau-nom>`',
};
