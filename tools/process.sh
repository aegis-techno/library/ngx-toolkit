#!/bin/sh
# This script is used to run a command on all apps and libs.
# It is used by the CI to run tests and linting on all apps and libs.
#
# Usage:
# ./tools/process.sh [list of path] --command [command]
#
# Example:
# ./tools/process.sh apps --command build
# ./tools/process.sh apps libs --command test!no-watch
# ./tools/process.sh libs apps  --command lint

#Init arguments
FOLDERS=""
COMMAND=""

# Loop on arguments
while [ $# -gt 0 ]; do
    if [ "$1" = "--command" ]; then
        COMMAND=$2
        shift
        shift
        continue # skip the rest of the loop
    fi
    FOLDERS="$FOLDERS $1"
    shift
done

# Check arguments
if [ "$FOLDERS" = "" ]; then
    echo "You must specify at least one path"
    exit 1
fi
if [ "$COMMAND" = "" ]; then
    echo "You must specify a command"
    exit 1
fi

# Run command on apps
for FOLDER in $FOLDERS; do
    if [ ! -d "$FOLDER" ]; then
        echo "Folder $FOLDER does not exist"
        exit 1
    fi
    #Find all npm package in the folder and subfolder
    for PACKAGE in $(find "$FOLDER" -name package.json); do
        #Get the package name
        # exemple : apps/catalog/catalog/package.json => catalog-catalog
        # exemple : apps/util/testing/package.json => util-testing
        PACKAGE_NAME=$(dirname "$PACKAGE" | sed "s/$FOLDER\///" | sed "s/\//-/g")

        #Run the command
        echo "Run $COMMAND on $PACKAGE_NAME"
        yarn run "$COMMAND" "$PACKAGE_NAME"
        if [ $? -ne 0 ]; then
            echo "Error while running $COMMAND on $PACKAGE_NAME"
            exit 1
        fi
    done
done
