#!/bin/sh
# This script is used to run a compodoc on all libs.
#
# Usage:
# ./tools/runCompodoc.sh [path] [module]
#
# Example:
# ./tools/runCompodoc.sh libs core

#Init arguments
CURRENT_PATH="$(pwd)"
FOLDER="$1"
MODULE="$2"

# Check arguments
if [ "$FOLDER" = "" ]; then
    echo "You must specify one path"
    exit 1
fi
if [ "$MODULE" = "" ]; then
    echo "You must specify one module"
    exit 1
fi

# Run compodoc on path
cd $FOLDER/$MODULE
compodoc -c .compodocrc.json
cd $CURRENT_PATH