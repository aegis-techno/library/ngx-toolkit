# ngx-model-forms

An Angular vest wrapper.

-   [Installation](#installation)
-   [Usage](#usage)
    -   [Serialization](#serialization)
-   [API](#api)

## Installation

Install via npm:

```
npm install @aegis-techno/ngx-model-forms --save
```

Install manually:

-   Add `@aegis-techno/ngx-model-forms` into `package.json`.
-   Run `npm install`.

## Usage
