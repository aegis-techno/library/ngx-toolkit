import {Indexable} from '@aegis-techno/ngx-core';
import {AbstractControl, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {ValidationContext} from '../models/validation-context.model';
import {ValidationSuite} from '../models/validation-suite.model';

/** Create Angular synchronous validator function for a single field of the model in the validation context.
 * When validation fails, the function returns a validation errors with two properties:
 * `error` - the first vest validation error,
 * `errors` - all of the vest validation errors for that field.
 * @param suite the vest suite with validation rules
 * @param field to validate
 * @param [model] the view model data to validate or a function returning such a model
 * Merges the control.value as the field property of that model.
 * @param [group] the name of a group of tests; only process tests in this group.
 * @param [context] global contextual data passed to vest validation rules.
 */
export function vestSyncFieldValidator(
    suite: ValidationSuite, // the validations
    field: string, // the property to validate
    model?: Indexable | (() => Indexable), // the view model with the data to validate
    group?: string, // the group of properties to validate (if specified)
    context?: ValidationContext // extra stuff validators could use
): ValidatorFn | ValidatorFn[] {
    const vestValidator = (control: AbstractControl): ValidationErrors | null => {
        let mod: Indexable = typeof model == 'function' ? model() : model;
        // Merge `control.value` because ngModel will not have updated the viewModel yet.
        mod = {...mod, [field]: control.value};
        const result = suite(mod, field, group, context).getErrors();
        const errors = result[field];
        return errors ? {error: errors[0], errors} : null;
    };

    // HACK to find out if the field is required.
    // Angular Material  looks for an Angular required validator and draws the component label accordingly.
    // So if we discover that the field is required, "compose" with that Angular required validator
    const testRun = suite({[field]: null}, field, group, context).getErrors();
    const isRequired = (testRun[field] ?? []).some((err: any) => err.includes('required'));
    return isRequired ? [vestValidator, Validators.required] : vestValidator;
}
