import {Directive, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Indexable} from '@aegis-techno/ngx-core';
import {Subject} from 'rxjs';
import {ValidationContext} from '../models/validation-context.model';

/** Set the validation scope variables on a form, ngModelGroup, or an element with a `ngxValModel` attribute.
 * Must specify model and modelType to match this directive.
 */
@Directive({
    selector:
        'form[ngxModel][modelType]:not([ngNoForm]):not([formGroup]),ng-form[ngxModel][modelType],[ngForm][ngxModel][modelType],[ngModelGroup][ngxModel][modelType],[ngxValModel]',
    standalone: true,
})
export class FormValidationScopeDirective implements OnChanges {
    /** data model whose properties will be validated. Required. */
    @Input() ngxModel: Indexable | undefined;

    /** Name of the type of model to be validated. Identifies the suite of validators to apply. Required.  */
    @Input() modelType: string | undefined;

    /** Name of the type of model to be validated. Identifies the suite of validators to apply. Required.  */
    @Input() readonly = false;

    /** Group name of tests within the validaotion suite. Only process tests in that group. */
    @Input() group?: string;

    /** Context that validators may reference for external information and services. */
    @Input() context?: ValidationContext;

    private changeSubject = new Subject<any>();

    /** Observable that emits when any of the validation scope inputs changed. */
    changed = this.changeSubject.asObservable();

    ngOnChanges(_changes: SimpleChanges): void {
        this.changeSubject.next(null);
    }
}
