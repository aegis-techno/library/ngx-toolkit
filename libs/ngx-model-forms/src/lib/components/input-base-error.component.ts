import {CommonModule} from '@angular/common';
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, ValidationErrors} from '@angular/forms';
import {Subscription} from 'rxjs';

@Component({
    selector: 'ngx-input-base-error',
    standalone: true,
    imports: [CommonModule],
    template: '',
})
export class InputBaseErrorComponent implements OnDestroy, OnInit {
    @Input() control?: AbstractControl;

    error?: ValidationErrors | null;
    pending = false;

    private sub?: Subscription;

    get show() {
        return !!this.control?.touched;
    }

    ngOnInit(): void {
        if (this.control) {
            const control = this.control;
            this.sub = control.statusChanges.subscribe((changes) => {
                this.error = null;
                this.pending = false;
                if (changes === 'INVALID') {
                    if (control.errors) {
                        const newError: any = control.errors['error'];
                        this.error = typeof newError == 'string' ? {message: newError} : newError;
                    } else {
                        this.error = null;
                    }
                } else {
                    this.pending = changes === 'PENDING';
                    this.error = null;
                }
            });
        }
    }

    ngOnDestroy(): void {
        this.sub?.unsubscribe();
    }
}
