import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    HostBinding,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Optional,
    Output,
    ViewChild,
} from '@angular/core';
import {FormControl, NgModel, Validators} from '@angular/forms';
import {Booleans, Indexable, Objects} from '@aegis-techno/ngx-core';
import {Subscription} from 'rxjs';
import {FormValidationScopeDirective} from '../directive/form-validation-scope.directive';
import {ValidationContext} from '../models/validation-context.model';

export type FormHooks = 'change' | 'blur' | 'submit';

export interface NgModelOptions {
    name?: string;
    standalone?: boolean;
    updateOn?: FormHooks;
}

export function trim(value: any | null) {
    return typeof value === 'string' ? value.trim() : value;
}

class NameCounter {
    counter = 0;

    get next() {
        return this.counter++;
    }
}

export const nameCounter = new NameCounter();

/** Base component for input element wrapper classes. Never instantiated! */
// Shouldn't have a Component decorator but Angular won't compile w/o it.
@Component({
    standalone: true,
    selector: 'ngx-input-base',
    template: '',
})
export abstract class InputBaseComponent<ItemType> implements AfterViewInit, OnInit, OnChanges, OnDestroy {
    @HostBinding('class')
    public readonly class = 'tw-flex-col tw-w-full input-base';

    @Input() context?: ValidationContext;

    @HostBinding('class.input-base-disabled')
    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(value: any) {
        this._disabled = Booleans.coerceBoolean(value);
    }

    @HostBinding('class.input-base-readonly')
    @Input()
    get readonly(): boolean {
        return this._readonly;
    }

    set readonly(value: any) {
        this._readonly = Booleans.coerceBoolean(value);
    }

    protected _disabled = false;
    protected _readonly = false;

    @Input() field?: string;
    @Input() group?: string;
    @Input() label?: string;
    @Input()
    public suffixIcon?: string;
    // eslint-disable-next-line @angular-eslint/no-input-rename
    @Input('model') inputModel?: Indexable;
    @Input() modelType?: string;
    @Input() name?: string;
    @Input() placeholder = '';
    @Input() type = 'text';
    @Input() updateOn?: FormHooks;
    @Input() className = 'tw-flex-col tw-w-full';
    @Input() required = false;

    /** Emit when the field value changes. */
    @Output() changed = new EventEmitter<ItemType | null>();

    @ViewChild('input') inputElRef?: ElementRef;
    @ViewChild('ngModel') ngModel?: NgModel;

    control?: FormControl;
    private hostEl: HTMLInputElement = this.hostElRef.nativeElement;
    model?: Indexable;
    protected ngModelOptions?: NgModelOptions;
    originalValue: ItemType | null = null;
    scopeSub?: Subscription;

    get value(): ItemType | null {
        return this.model![this.field!];
    }

    set value(val: ItemType | null) {
        // TODO: coerce value to type of original value
        val = trim(val);
        if (this.field) {
            this.model![this.field] = val;
        }
        this.changed.emit(val);
        if (!!this.ngModel && !!this.ngModel.control) {
            this.ngModel.control.markAsDirty();
            this.ngModel.control.markAsTouched();
        }
    }

    protected constructor(
        @Optional() protected formValidationScope: FormValidationScopeDirective,
        protected hostElRef: ElementRef
    ) {
        // Apply the class attribute on the host; if none, apply app standard CSS classes
        this.scopeSub = formValidationScope?.changed.subscribe((_: any) => this.ngOnChanges()); // might reset the model
    }

    ngOnChanges(): void {
        // only care about inputModel and updateOn changes; changes to other input vars are ignored.
        // inputModel trumps validation scope model
        this.model = this.inputModel ?? this.formValidationScope?.ngxModel ?? {};
        if (this.updateOn) {
            this.ngModelOptions = {updateOn: this.updateOn};
        }
    }

    ngOnInit(): void {
        this.field = this.field ?? this.name;
        this.name = this.name ?? `${this.field ?? ''}$${nameCounter.next}`;
        this.originalValue = trim(this.model![this.field!]);
    }

    ngAfterViewInit() {
        this.control = this.ngModel!.control;
        if (this.hostEl.hasAttribute('required')) {
            // Pass it along to the nested input element
            // Wait a tick to bypass ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                // Adding the required validator triggers Angular Material
                // to add the required attribute and update the label with the required marker.
                this.control!.addValidators(Validators.required);
                this.control!.updateValueAndValidity({onlySelf: true});
            });
        }
    }

    ngOnDestroy(): void {
        this.scopeSub?.unsubscribe();
    }

    /** User pressed escape. Restores original value */
    protected escaped(inputElm?: HTMLElement) {
        const elRef: HTMLInputElement = this.inputElRef?.nativeElement ?? inputElm;
        if (Objects.isDefined(elRef)) {
            const elementValue = elRef.value;
            if (elementValue !== this.originalValue) {
                this.value = this.originalValue;
            }
        }
    }
}
