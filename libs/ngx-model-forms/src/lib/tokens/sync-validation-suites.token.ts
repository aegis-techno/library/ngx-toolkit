import {InjectionToken} from '@angular/core';

export const SYNC_VALIDATION_SUITES = new InjectionToken('Synchronous Validation Suites');
