import {Suite} from 'vest';
import {ValidationSuiteFn} from './validation-suite-fn.model';

/** A vest sync or async validation suite, shaped for this validation implementation. */
export type ValidationSuite = Suite<ValidationSuiteFn>;
