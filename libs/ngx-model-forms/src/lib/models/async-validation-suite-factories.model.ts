import {Indexable} from '@aegis-techno/ngx-core';
import {AsyncValidationSuiteFactory} from './async-validation-suite-factory.model';

/** Map of asynchronous vest validation suite factories (creators), keyed by model type. */
export type AsyncValidationSuiteFactories = Indexable<AsyncValidationSuiteFactory>;
