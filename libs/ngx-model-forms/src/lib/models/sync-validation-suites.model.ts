import {Indexable} from '@aegis-techno/ngx-core';
import {ValidationSuite} from './validation-suite.model';

/** Map of synchronous vest validation suites, keyed by model type. */
export type SyncValidationSuites = Indexable<ValidationSuite>;
