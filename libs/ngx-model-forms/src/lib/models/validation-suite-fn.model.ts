import {Indexable} from '@aegis-techno/ngx-core';
import {ValidationContext} from './validation-context.model';

/** Vest validation suite function. Pass to vest `create()` to make a vest suite. */
export type ValidationSuiteFn = (model: Indexable, field?: string, group?: string, vc?: ValidationContext) => void;
