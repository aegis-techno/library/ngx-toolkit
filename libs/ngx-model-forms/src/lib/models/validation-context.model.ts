import {Indexable} from '@aegis-techno/ngx-core';

export type ValidationContext = Indexable;
