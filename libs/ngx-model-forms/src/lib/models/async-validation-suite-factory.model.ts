import {ValidationSuite} from './validation-suite.model';

/** Creates AsyncValidationSuite */
export type AsyncValidationSuiteFactory = () => ValidationSuite;
