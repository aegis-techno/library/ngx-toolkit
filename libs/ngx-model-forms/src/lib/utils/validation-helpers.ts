import {Indexable, Numbers, Objects} from '@aegis-techno/ngx-core';
import {enforce, omitWhen, test} from 'vest';

export function testRequired(model: Partial<Indexable>, fieldName: string): void {
    test(fieldName, `validationsErrors.global.required`, () => {
        enforce(Objects.isDefined(model[fieldName])).isTruthy();
    });
}

export function testMaxLength(model: Partial<Indexable>, fieldName: string, max: number): void {
    omitWhen(
        () => !model[fieldName],
        () => {
            test(fieldName, `validationsErrors.global.maxLength__${max}`, () => {
                enforce(model[fieldName].length < max).isTruthy();
            });
        }
    );
}

export function testMin(model: Partial<Indexable>, fieldName: string, min: number): void {
    omitWhen(
        () => !model[fieldName],
        () => {
            test(fieldName, `validationsErrors.global.min__${min}`, () => {
                enforce(Numbers.parseFormatted(model[fieldName]) ?? 0).greaterThanOrEquals(min);
            });
        }
    );
}
