import {SuiteResult, SuiteRunResult} from 'vest';

export type MatcherResult = {
    pass: boolean;
    message?: string;
};

export function hasExpectedFieldError(actual: SuiteRunResult | SuiteResult, field: string, errRegExp?: RegExp): MatcherResult {
    const errors = actual.getErrors()[field];
    let message: string | undefined;
    if (errors) {
        if (errRegExp && !errors.some((e) => errRegExp.test(e))) {
            message = `Expected one of the "${field}" errors to match ${errRegExp}; got ${JSON.stringify(errors)}`;
        }
    } else {
        message = `Expected errors for field "${field}" but got none`;
    }
    const pass = message == null;
    expect(pass).toBe(true);
    return {
        pass,
        message: pass ? `has expected error for "${field}"` : message,
    };
}

export function hasNoFieldErrors(actual: SuiteRunResult | SuiteResult, field: string): MatcherResult {
    const errors = actual.getErrors()[field];
    let message: string | undefined;
    if (errors) {
        message = `Expected no errors for field "${field}" but got ${JSON.stringify(errors)}`;
    }
    const pass = message == null;
    // Result and message generation.
    expect(pass).toBe(true);
    return {
        pass,
        message: pass ? `"${field}" has no errors` : message,
    };
}
