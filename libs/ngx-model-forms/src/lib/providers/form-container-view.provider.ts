import {inject, Provider} from '@angular/core';
import {ControlContainer, NgForm, NgModelGroup} from '@angular/forms';

export const formContainerViewProvider: Provider = {
    provide: ControlContainer,
    useFactory: formContainerFactory,
};

export function formContainerFactory() {
    const ngModelGroup = inject(NgModelGroup, {optional: true});
    const ngForm = inject(NgForm, {optional: true});
    return ngModelGroup ?? ngForm ?? null;
}
