import {create, enforce, omitWhen, only, test} from 'vest';
import {ValidationContext} from '../../lib/models/validation-context.model';
import {ValidationSuiteFn} from '../../lib/models/validation-suite-fn.model';
import {ValidationSuite} from '../../lib/models/validation-suite.model';
import {Address} from '../models/address.model';
import {UsStates} from '../models/us-states.model';

export const addressSyncValidationSuite: ValidationSuite = create(
    'AddressSyncValidationSuite',
    (model: Partial<Address>, field?: string, groupName?: string, context?: ValidationContext) => {
        only(field); // if field defined, limit to tests of this field
        addressSyncValidations(model, field, groupName, context);
    }
);

export const addressSyncValidations: ValidationSuiteFn = (model: Partial<Address>) => {
    model = model ?? {};

    test('street', 'Street is required', () => {
        enforce(model.street).isNotBlank();
    });

    omitWhen(!model.street, () => {
        test('street', 'Street must be at least 3 characters long', () => {
            enforce(model.street).longerThanOrEquals(3);
        });

        test('street', 'Street may be at most 50 characters long', () => {
            enforce(model.street).shorterThanOrEquals(50);
        });
    });

    test('city', 'City is required', () => {
        enforce(model.city).isNotBlank();
    });

    test('state', 'State is required', () => {
        enforce(model.state).isNotBlank();
    });

    test('state', 'State must be one of the supported US states', () => {
        enforce(model.state).condition((state: string) => UsStates.some((s) => s.abbreviation === state && !s.disabled));
    });

    test('postalCode', 'Postal Code is required', () => {
        enforce(model.postalCode).isNotBlank();
    });

    omitWhen(!model.postalCode, () => {
        test('postalCode', 'Postal Code must be 5 digit zip (12345) or a zip+4 (12345-1234)', () => {
            enforce(/^\d{5}(-\d{4}){0,1}$/.test(model.postalCode ?? '')).isTruthy();
        });
    });
};
