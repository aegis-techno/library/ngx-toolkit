import {hasExpectedFieldError, hasNoFieldErrors} from '../../lib/utils/unit-test-validation-helpers';
import {UsStates} from '../models/us-states.model';
import {addressSyncValidationSuite} from './address-sync.validations-suite';

const disabledState = UsStates.find((s) => s.disabled === false);
const supportedState = UsStates.find((s) => s.name === 'California');

describe('addressSyncValidationSuite', () => {
    it('has errors when address model is empty', () => {
        const result = addressSyncValidationSuite({});
        expect(result.hasErrors()).toBe(true);
    });

    describe('when street', () => {
        it('is a good street it passes', () => {
            const result = addressSyncValidationSuite({street: '123 main'});
            hasNoFieldErrors(result, 'street');
        });

        it('is null it has a required error', () => {
            const result = addressSyncValidationSuite({street: null});
            hasExpectedFieldError(result, 'street', /required/);
        });

        it('is empty it has a required error', () => {
            const result = addressSyncValidationSuite({street: ''});
            hasExpectedFieldError(result, 'street', /required/);
        });

        it('is fewer than 3 chars it has the expected min length error', () => {
            const result = addressSyncValidationSuite({street: '12'});
            hasExpectedFieldError(result, 'street', /at least 3/);
        });

        it('is more than 50 chars it has the expected max length error', () => {
            const result = addressSyncValidationSuite({
                street: ' 234567890 234567890 234567890 234567890 !TOO LONG!',
            });
            hasExpectedFieldError(result, 'street', /at most 50/);
        });
    });

    describe('when street2', () => {
        // There should be no restrictions on Street2
        it('is provided it passes', () => {
            const result = addressSyncValidationSuite({street2: 'Apt. A'});
            hasNoFieldErrors(result, 'street2');
        });

        it('is null it passes', () => {
            const result = addressSyncValidationSuite({street2: null});
            hasNoFieldErrors(result, 'street2');
        });

        it('is empty it passes', () => {
            const result = addressSyncValidationSuite({street2: ''});
            hasNoFieldErrors(result, 'street2');
        });
    });

    describe('when city', () => {
        it('is provided it passes', () => {
            const result = addressSyncValidationSuite({city: 'small town'});
            hasNoFieldErrors(result, 'city');
        });

        it('is null it has a required error', () => {
            const result = addressSyncValidationSuite({city: null});
            hasExpectedFieldError(result, 'city', /required/);
        });

        it('is empty it has a required error', () => {
            const result = addressSyncValidationSuite({city: ''});
            hasExpectedFieldError(result, 'city', /required/);
        });
    });

    describe('when state', () => {
        it('is one of the enabled U.S. States it passes', () => {
            const result = addressSyncValidationSuite({
                state: supportedState?.abbreviation,
            });
            hasNoFieldErrors(result, 'state');
        });

        it('is one of the disabled U.S. States it has the expected error', () => {
            const result = addressSyncValidationSuite({
                state: disabledState?.abbreviation,
            });
            hasExpectedFieldError(result, 'state', /supported/);
        });

        it('is null it has a required error', () => {
            const result = addressSyncValidationSuite({state: null});
            hasExpectedFieldError(result, 'state', /required/);
        });

        it('is empty it has a required error', () => {
            const result = addressSyncValidationSuite({state: ''});
            hasExpectedFieldError(result, 'state', /required/);
        });
    });

    describe('when postal code', () => {
        it('is 5 digits it passes', () => {
            const result = addressSyncValidationSuite({postalCode: '12345'});
            hasNoFieldErrors(result, 'postalCode');
        });

        it('is zip+4 it passes', () => {
            const result = addressSyncValidationSuite({
                postalCode: '12345-1234',
            });
            hasNoFieldErrors(result, 'postalCode');
        });

        it('is null it has a required error', () => {
            const result = addressSyncValidationSuite({postalCode: null});
            hasExpectedFieldError(result, 'postalCode', /required/);
        });

        it('is empty it has a required error', () => {
            const result = addressSyncValidationSuite({postalCode: ''});
            hasExpectedFieldError(result, 'postalCode', /required/);
        });

        it('has non-digits it has a format error', () => {
            const result = addressSyncValidationSuite({postalCode: 'abcde'});
            hasExpectedFieldError(result, 'postalCode', /must be 5 digit/);
        });

        it('has zip+4 without the dash it has a format error', () => {
            const result = addressSyncValidationSuite({
                postalCode: '123451234',
            });
            hasExpectedFieldError(result, 'postalCode', /must be 5 digit/);
        });
    });
});
