import {ValidationSuite} from '../../lib/models/validation-suite.model';
import {hasExpectedFieldError, hasNoFieldErrors} from '../../lib/utils/unit-test-validation-helpers';
import {AppValidationContext} from '../providers/app-validation-context.provider';
import {FeinValidationService, isGoodFein} from '../services/fein-validation.service';
import {FeinValidationResponse} from '../services/remote-fein-validation.service';
import {createCompanyAsyncValidationSuite} from './company-async.validations-suite';

class TestFeinValidationService {
    /** Check if an FEIN/Legal Name combination is valid
     * @returns Promise of the faked service response.
     */
    check(fein: string): Promise<FeinValidationResponse> {
        if (!isGoodFein(fein)) {
            return Promise.reject(`Bad FEIN "${fein}"`);
        }

        const lead = fein.substring(0, 2); // mock response based on lead 2 digits.
        if (fein === '13-1234567') {
            return Promise.resolve({
                fein,
                feinName: 'MARVELOUS PRODUCTS, INC.',
            });
        } else if (lead === '22') {
            return Promise.resolve({fein, feinName: '*'}); // Always OK
        } else {
            return Promise.resolve({fein}); // Always BAD
        }
    }
}

const testAppContext: AppValidationContext = {
    feinValidationService: new TestFeinValidationService() as FeinValidationService,
};

describe('companyAsyncValidationSuite', () => {
    let companyAsyncValidationSuite: ValidationSuite;

    beforeEach(() => {
        companyAsyncValidationSuite = createCompanyAsyncValidationSuite();
    });

    describe('createCompanyAsyncValidationSuite', () => {
        it('creates a "companyAsyncValidationSuite"', () => {
            expect(companyAsyncValidationSuite).toBeDefined();
        });
    });

    describe('when fein', () => {
        it('is registered with the IRS, it passes', (done) => {
            companyAsyncValidationSuite({fein: '13-1234567'}, 'fein', undefined, testAppContext).done((result) => {
                hasNoFieldErrors(result, 'fein');
                done();
            });
        });

        it('is not registered with the IRS, it has the expected error', (done) => {
            companyAsyncValidationSuite({fein: '66-1234567'}, 'fein', undefined, testAppContext).done((result) => {
                hasExpectedFieldError(result, 'fein', /not registered/);
                done();
            });
        });

        it('is empty, it passes', (done) => {
            companyAsyncValidationSuite({fein: ''}, 'fein', undefined, testAppContext).done((result) => {
                expect(result.hasErrors()).toBe(false);
                done();
            });
        });

        it('is badly formed, it passes', (done) => {
            companyAsyncValidationSuite({fein: 'no good'}, 'fein', undefined, testAppContext).done((result) => {
                expect(result.hasErrors()).toBe(false);
                done();
            });
        });
    });

    describe('when legal name', () => {
        it('matches the name registered with the IRS, it passes', (done) => {
            companyAsyncValidationSuite(
                {fein: '13-1234567', legalName: 'MARVELOUS PRODUCTS, INC.'},
                'legalName',
                undefined,
                testAppContext
            ).done((result) => {
                hasNoFieldErrors(result, 'legalName');
                done();
            });
        });

        it('does not exactly match the name registered with the IRS, it has the "not matched" error', (done) => {
            companyAsyncValidationSuite(
                {fein: '13-1234567', legalName: 'MARVELOUS PRODUCTS'},
                'legalName',
                undefined,
                testAppContext
            ).done((result) => {
                hasExpectedFieldError(result, 'legalName', /not match/);
                done();
            });
        });

        it('is empty, it passes even if FEIN is registered', (done) => {
            companyAsyncValidationSuite({fein: '13-1234567', legalName: ''}, 'legalName', undefined, testAppContext).done(
                (result) => {
                    hasNoFieldErrors(result, 'legalName');
                    done();
                }
            );
        });

        it('is provided but the FEIN is not registered with the IRS, it passes', (done) => {
            companyAsyncValidationSuite({fein: '66-1234567', legalName: 'any name'}, 'legalName', undefined, testAppContext).done(
                (result) => {
                    hasNoFieldErrors(result, 'legalName');
                    done();
                }
            );
        });
    });
});
