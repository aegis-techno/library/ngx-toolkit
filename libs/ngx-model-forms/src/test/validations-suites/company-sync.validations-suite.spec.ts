import {Address} from '../models/address.model';
import {companySyncValidationSuite} from './company-sync.validations-suite';
import {hasExpectedFieldError, hasNoFieldErrors} from '../../lib/utils/unit-test-validation-helpers';

describe('companySyncValidationSuite', () => {
    it('has errors when company model is empty', () => {
        const result = companySyncValidationSuite({});
        expect(result.hasErrors()).toBe(true);
    });

    describe('when legal name', () => {
        it('is a good name it passes', () => {
            const result = companySyncValidationSuite({legalName: 'Acme'});
            hasNoFieldErrors(result, 'legalName');
        });

        it('is null it has a required error', () => {
            const result = companySyncValidationSuite({legalName: null});
            hasExpectedFieldError(result, 'legalName', /required/);
        });

        it('is empty it has a required error', () => {
            const result = companySyncValidationSuite({legalName: ''});
            hasExpectedFieldError(result, 'legalName', /required/);
        });

        it('is fewer than 3 chars it has the expected min length error', () => {
            const result = companySyncValidationSuite({legalName: 'ab'});
            hasExpectedFieldError(result, 'legalName', /at least 3/);
        });

        it('is more than 30 chars it has the expected max length error', () => {
            const result = companySyncValidationSuite({
                legalName: ' 234567890 234567890 !TOO LONG!',
            });
            hasExpectedFieldError(result, 'legalName', /at most 30/);
        });
    });

    describe('when fein', () => {
        it('is well-formatted it passes', () => {
            const result = companySyncValidationSuite({fein: '12-1234567'});
            hasNoFieldErrors(result, 'fein');
        });

        it('is missing the dash it has a format error', () => {
            const result = companySyncValidationSuite({fein: '121234567'});
            hasExpectedFieldError(result, 'fein', /format/);
        });

        it('is too short it has a format error', () => {
            const result = companySyncValidationSuite({fein: '12-123456'});
            hasExpectedFieldError(result, 'fein', /format/);
        });

        it('is too long it has a format error', () => {
            const result = companySyncValidationSuite({fein: '12-12345678'});
            hasExpectedFieldError(result, 'fein', /format/);
        });

        it('has alpha chars it has a format error', () => {
            const result = companySyncValidationSuite({fein: '12-123A567'});
            hasExpectedFieldError(result, 'fein', /format/);
        });

        it('is null it has a required error', () => {
            const result = companySyncValidationSuite({fein: null});
            hasExpectedFieldError(result, 'fein', /required/);
        });

        it('is empty it has a required error', () => {
            const result = companySyncValidationSuite({fein: ''});
            hasExpectedFieldError(result, 'fein', /required/);
        });
    });

    describe('when work address', () => {
        // Leave most of the address testing to the addressSyncValidationSuite tests.
        // Test just enough to show that address validation is happening for the companySyncValidation suite.

        it('is valid, there are no work address errors', () => {
            const result = companySyncValidationSuite({
                workAddress: {
                    street: '123 Main',
                    city: 'Somewhere',
                    state: 'CA',
                    postalCode: '94123',
                } as Address,
            });
            const hasAddressErrors = result.hasErrorsByGroup('workAddress');
            expect(hasAddressErrors).toBe(false);
        });

        it('is empty, it has errors', () => {
            const result = companySyncValidationSuite({});
            const hasAddressErrors = result.hasErrorsByGroup('workAddress');
            expect(hasAddressErrors).toBe(true);
        });

        it('is incomplete, it has errors', () => {
            const result = companySyncValidationSuite({
                workAddress: {
                    street: '123 Main',
                    city: 'Somewhere',
                    // state: 'CA',
                    // postalCode: '94123'
                } as Address,
            });
            const hasAddressErrors = result.hasErrorsByGroup('workAddress');
            expect(hasAddressErrors).toBe(true);
        });
    });
});
