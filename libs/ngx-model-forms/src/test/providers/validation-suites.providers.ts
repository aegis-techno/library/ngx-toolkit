import {Provider} from '@angular/core';
import {ASYNC_VALIDATION_SUITE_FACTORIES} from '../../lib/tokens/async-validation-suite-factories.token';
import {SYNC_VALIDATION_SUITES} from '../../lib/tokens/sync-validation-suites.token';
import {addressSyncValidationSuite} from '../validations-suites/address-sync.validations-suite';
import {createCompanyAsyncValidationSuite} from '../validations-suites/company-async.validations-suite';
import {companySyncValidationSuite} from '../validations-suites/company-sync.validations-suite';

export const validationSuiteProvidersMock: Provider[] = [
    {
        provide: ASYNC_VALIDATION_SUITE_FACTORIES,
        useValue: {
            company: createCompanyAsyncValidationSuite,
        },
    },
    {
        provide: SYNC_VALIDATION_SUITES,
        useValue: {
            address: addressSyncValidationSuite,
            company: companySyncValidationSuite,
        },
    },
];
