import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {InputBaseErrorComponent} from '../../lib/components/input-base-error.component';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'ngx-test-input-error',
    standalone: true,
    imports: [CommonModule],
    template: `
        <div *ngIf="show" class="full-width">
            <span *ngIf="error" class="error">{{ error }}</span>
            <span *ngIf="pending" class="pending">validating...</span>
        </div>
    `,
})
export class InputErrorTestComponent extends InputBaseErrorComponent {}
