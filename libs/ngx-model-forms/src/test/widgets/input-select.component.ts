import {CommonModule} from '@angular/common';
import {Component, ElementRef, Input, Optional} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {InputBaseComponent} from '../../lib/components/input-base.component';
import {FormFieldNgModelDirective} from '../../lib/directive/form-field-ng-model.directive';
import {FormValidationScopeDirective} from '../../lib/directive/form-validation-scope.directive';
import {formContainerViewProvider} from '../../lib/providers/form-container-view.provider';
import {SelectOption} from '../models/us-states.model';
import {InputErrorTestComponent} from './input-error.component';

@Component({
    selector: 'ngx-test-input-select',
    standalone: true,
    imports: [CommonModule, FormsModule, FormFieldNgModelDirective, InputErrorTestComponent],
    viewProviders: [formContainerViewProvider],

    template: `
        <select
            #input
            #ngModel="ngModel"
            [disabled]="disabled"
            [field]="field"
            [model]="model"
            [modelType]="modelType"
            [group]="group"
            [name]="name!"
            [ngModelOptions]="ngModelOptions!"
            [(ngModel)]="value"
            (keyup.escape)="escaped()">
            <option *ngIf="showPlaceholderOption" [value]="value" disabled>
                {{ placeholderOption }}
            </option>
            <option *ngFor="let option of options" [value]="option.value" [disabled]="option.disabled">
                {{ option.name }}
            </option>
        </select>
        <ngx-test-input-error [control]="ngModel.control"></ngx-test-input-error>
    `,
})
export class InputSelectTestComponent extends InputBaseComponent<string> {
    @Input() options: SelectOption[] = [];

    /** Display this text as the (disabled) first option when the model.field is "empty" or not among the choices. */
    @Input() placeholderOption = '';

    constructor(@Optional() formValidationScope: FormValidationScopeDirective, hostElRef: ElementRef) {
        super(formValidationScope, hostElRef);
    }

    get showPlaceholderOption() {
        return (
            this.placeholderOption &&
            (this.value === '' || this.value == null || this.options.every((opt) => opt.value !== this.value))
        );
    }
}
