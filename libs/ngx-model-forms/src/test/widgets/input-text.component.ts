import {CommonModule} from '@angular/common';
import {Component, ElementRef, Optional} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {InputBaseComponent} from '../../lib/components/input-base.component';
import {FormFieldNgModelDirective} from '../../lib/directive/form-field-ng-model.directive';
import {FormValidationScopeDirective} from '../../lib/directive/form-validation-scope.directive';
import {formContainerViewProvider} from '../../lib/providers/form-container-view.provider';
import {InputErrorTestComponent} from './input-error.component';

@Component({
    selector: 'ngx-test-input-text',
    standalone: true,
    imports: [CommonModule, FormFieldNgModelDirective, FormsModule, InputErrorTestComponent],
    viewProviders: [formContainerViewProvider],
    template: `
        <input
            #input
            #ngModel="ngModel"
            [disabled]="disabled"
            [field]="field"
            [model]="model"
            [modelType]="modelType"
            [group]="group"
            [name]="name!"
            [ngModelOptions]="ngModelOptions!"
            [placeholder]="placeholder!"
            [type]="type"
            [required]="required"
            [(ngModel)]="value"
            (keyup.escape)="escaped()"
            (blur)="onBlur()" />
        <ngx-test-input-error [control]="ngModel.control"></ngx-test-input-error>
    `,
})
export class InputTextTestComponent extends InputBaseComponent<string> {
    constructor(@Optional() formValidationScope: FormValidationScopeDirective, hostElRef: ElementRef) {
        super(formValidationScope, hostElRef);
    }

    protected onBlur() {
        this.trimControlValue();
    }

    /** Trim the value in the ngModel control, which updates the screen and the value of the field in the parent form. */
    private trimControlValue() {
        if (this.ngModel?.control.value !== this.value) {
            this.ngModel?.control.setValue(this.value, {emitEvent: false});
        }
    }
}
