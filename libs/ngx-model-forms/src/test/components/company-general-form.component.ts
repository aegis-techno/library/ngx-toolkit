import {Component, Input, ViewChild} from '@angular/core';
import {formContainerViewProvider} from '../../lib/providers/form-container-view.provider';
import {Company} from '../models/company.model';
import {InputTextTestComponent} from '../widgets/input-text.component';

@Component({
    selector: 'ngx-test-company-general-form',
    standalone: true,
    imports: [InputTextTestComponent],
    templateUrl: './company-general-form.component.html',
    viewProviders: [formContainerViewProvider],
})
export class CompanyGeneralFormComponent {
    @Input() vm?: Partial<Company>;
    @ViewChild('legalName') legalNameComponent!: InputTextTestComponent;

    /** Revalidate LegalName, updating its error message, when FEIN changes.
     * A change to FEIN should invalidate the Legal Name if that name does not match the registered IRS name.
     *
     * Problem: Angular won't automatically revalidate one field when another changes.
     * A model group level validation won't help; can't use that to control the legal name message output
     *
     * Solution: when FEIN changes, force revalidation of Legal Name
     */
    protected feinChanged(_fein: string | null) {
        // console.log('fein changed to ' + _fein);
        const control = this.legalNameComponent.control;
        control?.markAsTouched(); // ensure display
        setTimeout(() => {
            // wait one tick for ngModel to update the model, then re-validate.
            control?.updateValueAndValidity();
        });
    }
}
