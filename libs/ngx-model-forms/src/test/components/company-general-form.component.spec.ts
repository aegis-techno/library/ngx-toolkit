import {configureTestBed, createTestContext, TestContext, Then, When} from '@aegis-techno/ngx-testing-tools';
import {validationSuiteProvidersMock} from '../providers/validation-suites.providers';
import {CompanyGeneralFormComponent} from './company-general-form.component';

describe('CompanyGeneralFormComponent', () => {
    let testContext: TestContext<CompanyGeneralFormComponent>;

    configureTestBed({
        providers: [validationSuiteProvidersMock],
    });

    When(async () => {
        testContext = createTestContext(CompanyGeneralFormComponent);
        testContext.detectChanges();
    });

    Then('should compile', () => {
        expect(testContext.component).toBeTruthy();
    });
});
