import {CommonModule} from '@angular/common';
import {Component, ViewChild} from '@angular/core';
import {FormsModule, NgForm} from '@angular/forms';
import {FormFieldNgModelDirective} from '../../lib/directive/form-field-ng-model.directive';
import {FormValidationScopeDirective} from '../../lib/directive/form-validation-scope.directive';
import {Company} from '../models/company.model';
import {validationSuiteProvidersMock} from '../providers/validation-suites.providers';
import {InputTextTestComponent} from '../widgets/input-text.component';
import {AddressFormComponent} from './address-form.component';
import {CompanyGeneralFormComponent} from './company-general-form.component';

@Component({
    selector: 'ngx-test-company-form',
    standalone: true,
    templateUrl: './company-form.component.html',
    viewProviders: [],
    imports: [
        CommonModule,
        FormsModule,
        InputTextTestComponent,
        AddressFormComponent,
        CompanyGeneralFormComponent,
        FormFieldNgModelDirective,
        FormValidationScopeDirective,
    ],
    providers: [...validationSuiteProvidersMock],
})
export class CompanyFormComponent {
    /** The NgForm, exposed for testing only. */
    @ViewChild('form') form!: NgForm;

    vm: Company = {workAddress: {} as any};
}
