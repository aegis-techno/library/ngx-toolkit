import {FormGroup} from '@angular/forms';
import {configureTestBed, createTestContext, TestContext, Then, When} from '@aegis-techno/ngx-testing-tools';
import {CompanyFormComponent} from './company-form.component';

describe('CompanyFormComponent', () => {
    let testContext: TestContext<CompanyFormComponent>;

    configureTestBed();

    When(async () => {
        testContext = createTestContext(CompanyFormComponent);
        testContext.detectChanges();
    });

    Then('should compile', () => {
        expect(testContext.component).toBeTruthy();
    });
    Then('should have a populated form control', () => {
        const controls = testContext.component.form!.controls;
        expect(Object.keys(controls).length).toBeGreaterThan(1);
    });

    Then('should have company fields validated with the company modelType', () => {
        const controls = testContext.component.form!.controls;
        expect((controls['legalName'] as any)._validationMetadata?.modelType).toBe('company');
        expect((controls['fein'] as any)._validationMetadata?.modelType).toBe('company');
    });

    Then('should have company.workAddress fields validated with the address modelType', () => {
        const workAddressGroup = testContext.component.form!.controls['workAddress'] as FormGroup;
        const controls = workAddressGroup?.controls;
        Object.keys(controls).forEach((key) => {
            expect((controls[key] as any)._validationMetadata?.modelType).toBe('address');
        });
    });
});
