import {CommonModule} from '@angular/common';
import {Component, Input} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {FormFieldNgModelDirective} from '../../lib/directive/form-field-ng-model.directive';
import {FormValidationScopeDirective} from '../../lib/directive/form-validation-scope.directive';
import {formContainerViewProvider} from '../../lib/providers/form-container-view.provider';
import {Address} from '../models/address.model';
import {UsStatesSelectOptions} from '../models/us-states.model';
import {InputSelectTestComponent} from '../widgets/input-select.component';
import {InputTextTestComponent} from '../widgets/input-text.component';

@Component({
    selector: 'ngx-test-address-form',
    standalone: true,
    templateUrl: './address-form.component.html',
    viewProviders: [formContainerViewProvider],
    imports: [
        CommonModule,
        FormsModule,
        InputTextTestComponent,
        InputSelectTestComponent,
        FormFieldNgModelDirective,
        FormValidationScopeDirective,
    ],
})
export class AddressFormComponent {
    @Input() vm?: Address;

    /** Name for the formGroup when added to the parent form. Defaults to 'address'. */
    // eslint-disable-next-line @angular-eslint/no-input-rename
    @Input('name') ngModelGroupName = 'address';

    states = UsStatesSelectOptions;
}
