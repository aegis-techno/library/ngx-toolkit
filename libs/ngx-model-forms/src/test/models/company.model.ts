import {Indexable} from '@aegis-techno/ngx-core';
import {Address} from './address.model';

export interface Company extends Indexable {
    /** Federal Employer Identification Number */
    fein?: string;
    legalName?: string;
    comment?: string;
    workAddress: Address;
}
