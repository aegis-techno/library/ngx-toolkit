# NgxTestingTools

This library is a helper that encourages leaner, meaner specs.

It provide Given When Then Concepts and mock tool for Jest and Angular

## Running unit tests

Run `ng test ngx-testing-tools` to execute the unit tests via jest.

## Given When Then Concepts

This library is a helper that encourages leaner, meaner specs using Given, When, and Then.

The basic idea behind this is a humble acknowledgement of given-when-then as the best English language analogue we have
to arrange-act-assert

> Prior Art + Credit
>
> The concept (and implementation) is based on the work of [HiRez.io](https://twitter.com/hirez_io), who've done an
> amazing implementation of the original [jasmine-given](https://github.com/searls/jasmine-given) library
> by [Justin Searls](https://twitter.com/searls), which is in turn based
> on [rspec-given](https://github.com/jimweirich/rspec-given) by [Jim Weirich](https://twitter.com/jimweirich).

> Example

```TypeScript
describe('A context', () => {
    let fakeNumber: number;
    let actualResult: any;

    function addTwo(num: number) {
        return num + 2;
    }

    Given(() => {
        fakeNumber = undefined;
        actualResult = undefined;
    });

    describe('should run "Given" before "When" and "When" before "Then"', () => {
        Given(() => {
            fakeNumber = 2;
        });

        When(() => {
            actualResult = addTwo(fakeNumber);
        });

        Then(() => {
            expect(actualResult).toBe(4);
        });
    });

    describe("redundant test execution example", () => {
        context("a traditional spec with numerous Then statements", () => {
            var timesGivenWasInvoked = 0,
                timesWhenWasInvoked = 0;
            Given(() => {
                timesGivenWasInvoked++;
            });
            When(() => {
                timesWhenWasInvoked++;
            });
            Then(() => expect(timesGivenWasInvoked).toBe(1));
            Then(() => expect(timesWhenWasInvoked).toBe(2));
            Then(() => expect(timesGivenWasInvoked).toBe(3));
            Then(() => expect(timesWhenWasInvoked).toBe(4));
        });


        context("a spec with one Then and multiple And statements", () => {
            var timesGivenWasInvoked = 0,
                timesWhenWasInvoked = 0;
            Given(() => {
                timesGivenWasInvoked++;
            });
            When(() => {
                timesWhenWasInvoked++;
            });
            Then(() => expect(timesGivenWasInvoked).toBe(1));
            And(() => expect(timesWhenWasInvoked).toBe(1));
            And(() => expect(timesGivenWasInvoked).toBe(1));
            And(() => expect(timesWhenWasInvoked).toBe(1));
            Then(() => expect(timesWhenWasInvoked).toBe(2));
        });
    });
```

### Given

The Given section specifies a starting point, a set of preconditions that must be true before the code under test is
allowed to be run. In standard test frameworks the preconditions are established with a combination of setup methods

### When

The When clause specifies the code to be tested ... oops, excuse me ... specified. After the preconditions in the given
section are met, the when code block is run.

### Then / ThenExpect

The Then clauses are the postconditions of the specification. These then conditions must be true after the code under
test (the When clause) is run.

### And

The And clauses are play after the Then clause find before. The And clauses don't support "random order running test".

## Mock

Utils method :

-   `configureTestBed(moduleDef?: TestModuleMetadata)`, for the configuration of the TestBed in a Given.
-   `createTestContext(component: Type<T>)`, for create un wrapper of a fixture for the component class given.
-   `provideSpyObjForClass`, create a SpyObj.
-   `injectMock(classType: Type<T> | AbstractType<T>)`, for retreive a mock.

### Usage

```TypeScript
describe('MyComponent', () => {
    let testContext: TestContext<MyComponent>;

    configureTestBed({
            providers: [
                provideSpyObjForClass(MyService)
            ]
        },
    );

    Given(() => {
        injectMock(MyService).foo.and.returnValue(["TEST"]);
    })

    When(async () => {
        testContext = createTestContext(MyComponent);
        await testContext.whenStable();
    });

    Then('should be created', () => {
        expect(testContext.component).toBeTruthy();
    });

    Then('should have retrieve data from service', () => {
        expect(testContext.component.bar).toBe("TEST");
    });
});

```
