export const NO_SPEC_FUNCTION_ERROR = 'You must provide a function to Then';
export const NEED_THEN_CALL_BEFORE_AND = 'You must call Then before call And';
export const NEED_USE_CONTEXT_FOR_AND = 'And need be use in context';
export const CONTEXT_HEADER_MESSAGE = 'An error was thrown in';
export const NO_STACK_ERROR = `Stack trace unavailable`;
export const THEN_LABEL_PREFIX = '\n   -> Then ';
export const DEFAULT_LABEL = 'should be success';
