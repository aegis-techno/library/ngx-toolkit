/**
 * It does nothing
 */
export const noop: (...arg: Array<unknown>) => unknown = () => {
    return;
};
noop();
