import {emptyDoneFn} from '../models/done-fn.interface';
import {TestCallback} from '../models/test-callback.interface';
import {
    CONTEXT_HEADER_MESSAGE,
    DEFAULT_LABEL,
    NEED_THEN_CALL_BEFORE_AND,
    NEED_USE_CONTEXT_FOR_AND,
    NO_SPEC_FUNCTION_ERROR,
    NO_STACK_ERROR,
    THEN_LABEL_PREFIX,
} from '../utils/constants';
import {noop} from '../utils/noop';

declare const it: any;
declare const beforeEach: any;
declare const afterEach: any;
declare const describe: any;

/**
 *  It provides a set of functions that you can use to write your tests.
 */
export class NgxTestingTools {
    static currentUserContext: any = null;

    static whenFnsQueue: TestCallback[] = [];

    static isContext = false;
    static contextualFn: {label: string; specFns: TestCallback[]} | null = null;
    static contextualFnsQueue: {label: string; specFns: TestCallback[]}[] = [];

    public static Given(givenSetupFn: TestCallback): void {
        beforeEach(async () => {
            return await this.runGiven(givenSetupFn);
        });
    }

    public static async runGiven(givenSetupFn: TestCallback) {
        try {
            return await this.promisify(givenSetupFn);
        } catch (error: any) {
            throw this.getErrorWithContext('Given', error);
        }
    }

    public static When(whenSetupFn: TestCallback): void {
        beforeEach(() => {
            this.whenFnsQueue.push(whenSetupFn);
        });

        afterEach(() => {
            this.whenFnsQueue.pop();
        });
    }

    public static Then(specFnOrThenLabel: TestCallback | string, specFn?: TestCallback): void {
        let label = specFnOrThenLabel;

        if (typeof label != 'string') {
            specFn = specFnOrThenLabel as TestCallback;
            label = DEFAULT_LABEL;
        }

        if (!specFn) {
            throw new Error(NO_SPEC_FUNCTION_ERROR);
        }

        this.runSpecFunction(THEN_LABEL_PREFIX + (label ?? DEFAULT_LABEL), specFn);
    }

    public static And(specFn: TestCallback) {
        if (!specFn) {
            throw new Error(NO_SPEC_FUNCTION_ERROR);
        }
        if (!this.isContext) {
            throw new Error(NEED_USE_CONTEXT_FOR_AND);
        }
        if (this.contextualFn == null) {
            throw new Error(NEED_THEN_CALL_BEFORE_AND);
        }
        this.contextualFn.specFns.push(specFn);
    }

    public static context(label: string, specFn: () => void) {
        describe(label, () => {
            this.isContext = true;
            specFn();
            this.isContext = false;

            this.runContextualFn();
            this.contextualFn = null;
            this.contextualFnsQueue.splice(0);
        });
    }

    private static runSpecFunction(label: string, givenSetupFnOrSpecFn: TestCallback | undefined, specFn?: TestCallback): void {
        let givenSetupFn: TestCallback | undefined = undefined;
        let actualSpecFn = givenSetupFnOrSpecFn as TestCallback;
        if (specFn !== undefined) {
            actualSpecFn = specFn;
            givenSetupFn = givenSetupFnOrSpecFn;
        }

        if (!this.isContext) {
            this.runIt(label, givenSetupFn, actualSpecFn);
        } else {
            this.contextualFn = {label, specFns: [actualSpecFn]};
            this.contextualFnsQueue.push(this.contextualFn);
        }
    }

    private static runIt(label: string, givenSetupFn: TestCallback | undefined, actualSpecFn: TestCallback) {
        it(label, async () => {
            if (givenSetupFn !== undefined) {
                await this.runGiven(givenSetupFn);
            }
            await this.runAllFunction(this.whenFnsQueue, 'When');
            await this.runAllFunction([actualSpecFn], 'Then');
        });
    }

    private static async runAllFunction(fnQueue: TestCallback[], typeName: string) {
        for (const fn of fnQueue) {
            await this.runFunction(fn, typeName);
        }
    }

    private static async runFunction(fn: TestCallback, typeName: string) {
        try {
            await this.promisify(fn);
        } catch (error: any) {
            throw this.getErrorWithContext(typeName, error);
        }
    }

    private static async promisify(fn: TestCallback): Promise<TestCallback> {
        if (fn.length == 0) {
            return fn.call(this.currentUserContext, emptyDoneFn);
        }

        return new Promise((resolve, reject) => {
            const next = this.getNextFunctionForPromise(resolve, reject);
            fn.call(this.currentUserContext, next);
        });
    }

    private static getNextFunctionForPromise(
        resolve: (value: TestCallback | PromiseLike<TestCallback>) => void,
        reject: (reason?: any) => void
    ) {
        const next = (err?: Error) => {
            if (err) {
                next.fail(err);
            } else {
                resolve(noop);
            }
        };
        next.fail = (err: Error) => {
            reject(err);
        };
        return next;
    }

    private static runContextualFn() {
        for (const contextualFn of this.contextualFnsQueue) {
            it(contextualFn.label, async () => {
                await this.runAllFunction(this.whenFnsQueue, 'When');
                await this.runAllFunction(contextualFn.specFns, 'Then');
            });
        }
    }

    private static getErrorWithContext(originFunctionName: string, error: Error | string): Error {
        if (typeof error === 'string') {
            return this.getErrorWithContextAndMessage(
                originFunctionName,
                `${error}
  ${NO_STACK_ERROR}`
            );
        } else {
            return this.getErrorWithContextAndMessage(originFunctionName, error.stack ?? error.toString());
        }
    }

    private static getErrorWithContextAndMessage(originFunctionName: string, message: string): Error {
        return new Error(`${CONTEXT_HEADER_MESSAGE} ${originFunctionName}():
    ${message}`);
    }
}
