import {DoneFn} from './done-fn.interface';

export interface TestCallback {
    (done: DoneFn): any;
}
