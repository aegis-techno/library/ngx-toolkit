import {noop} from '../utils/noop';

export interface DoneFn {
    fail: (...args: Error[]) => void;

    (...args: Error[]): void;
}

export const emptyDoneFn: DoneFn = noop.bind({}) as DoneFn;
emptyDoneFn.fail = noop;
