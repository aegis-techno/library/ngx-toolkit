import {Injector, PlatformRef, StaticProvider, Type} from '@angular/core';
import {ComponentFixture, TestBed, TestModuleMetadata} from '@angular/core/testing';
import {Given} from '../ngx-testing-tools';

/**
 * It takes a platformRef and an array of providers, and returns a platformRef with the providers added to it.
 * @param {PlatformRef} platformRef - PlatformRef - The platform reference to add the providers to.
 * @param providers - Array<StaticProvider>
 * @returns A new PlatformRef with the new providers added to the injector.
 */
export function addProvidersToPlatformRef(platformRef: PlatformRef, providers: Array<StaticProvider>): PlatformRef {
    (platformRef as any)._injector = Injector.create({
        parent: platformRef.injector,
        providers: providers,
    });

    return platformRef;
}

export const configureTestBed = (moduleDef: TestModuleMetadata = {}) => {
    Given(async () => {
        await TestBed.configureTestingModule(moduleDef).compileComponents();
    });

    afterEach(() => {
        TestBed.resetTestingModule();
    });
};

/**
 * A wrapper class around ComponentFixture, which provides useful accessros:
 * component - to access component instance of current the fixture
 * element - to access underlying native element of the current component
 * detectChanges - to run change detections using current fixture
 * resolve - to resolve a type using current fixture's injector
 */
export class TestContext<T> {
    constructor(public fixture: ComponentFixture<T>) {}

    public get component() {
        return this.fixture.componentInstance;
    }

    public get element(): HTMLElement {
        return this.fixture.debugElement.nativeElement;
    }

    public detectChanges() {
        this.fixture.detectChanges();
    }

    public whenStable() {
        this.fixture.detectChanges();
        return this.fixture.whenStable();
    }
}

/**
 * Creates TestCtx instance for the Angular Component which is not initialized yet (no ngOnInit called)
 * Use case: you can override Component's providers before hooks are called.
 *
 * @param component - type of component to create instance of
 * **/
export const createTestContext = <T>(component: Type<T>) => {
    return new TestContext<T>(TestBed.createComponent<T>(component));
};
