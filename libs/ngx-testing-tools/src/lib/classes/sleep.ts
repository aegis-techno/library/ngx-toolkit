/**
 * sleep() returns a Promise that resolves after a given timeout.
 *
 * The function is asynchronous because it returns a Promise. The Promise is resolved after a given timeout
 * @param {number} timeout - The number of milliseconds to wait before resolving the promise.
 * @returns A promise that resolves after a timeout.
 * @example await sleep(1000);
 */
export function sleep(timeout: number): Promise<unknown> {
    return new Promise((r) => setTimeout(r, timeout));
}
