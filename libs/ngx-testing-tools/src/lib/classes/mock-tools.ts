// import {AbstractType, Type, ValueProvider} from '@angular/core';
// import {TestBed} from '@angular/core/testing';

// type SpyObj<T> = {[K in keyof T]: jest.Mock} & {__originalClass__: Type<T>};
// type SpyObjMethodNames<T> = Array<keyof T>;
// type SpyObjPropertyNames<T> = Array<keyof T>;

/**
 * It returns a mock object of the given class type by TestBed.inject.
 * @param {Type<T> | AbstractType<T>} classType - The class type of the service you want to mock.
 * @returns A spy object of type T
 */
// export function injectMock<T>(classType: Type<T> | AbstractType<T>) {
//     return <SpyObj<T>>TestBed.inject(classType);
// }

/**
 * It takes a class and returns a spy object that has all the methods of the class.
 * @param {Type<T> | AbstractType<T>} baseClass - The class or interface that you want to create a spy for.
 * @returns A SpyObj<T>
 */
// export function createFullSpyObj<T>(baseClass: Type<T> | AbstractType<T>): SpyObj<T> {
//     return createSpyObj(baseClass, getAllMethodNamesFor(baseClass) as any);
// }

/**
 * It creates a spy object for a class, and then provides it as a value provider.
 * @param {Type<T> | AbstractType<T>} baseClass - The class you want to create a spy for.
 * @returns A ValueProvider
 */
// export function provideSpyObjForClass<T>(baseClass: Type<T> | AbstractType<T>): ValueProvider {
//     return provideSpy(createFullSpyObj(baseClass));
// }

/**
 * It takes a spy object and returns a value provider for the class that the spy was created for.
 * @param spy - SpyObj<any>
 * @returns A ValueProvider
 */
// export function provideSpy(spy: SpyObj<any>): ValueProvider {
//     if (!spy.__originalClass__) {
//         throw new Error("Cannot provide spy don't create with custom methods createSpyObj");
//     }
//     return {
//         provide: spy.__originalClass__,
//         useValue: spy,
//     };
// }

/**
 * It creates a spy object that has the same methods and properties as the class you pass in, and it also adds a property
 * to the spy object that tells you what class it's a spy for.
 * @param {Type<T> | AbstractType<T>} baseClass - The class that you want to create a spy object for.
 * @param methodNames - An array of strings, each of which is the name of a method to be spied on.
 * @param [propertyNames] - An array of property names to be added to the spy object.
 * @returns A spy object of type T.
 */
// export function createSpyObj<T>(
//     baseClass: Type<T> | AbstractType<T>,
//     methodNames: SpyObjMethodNames<T>,
//     propertyNames?: SpyObjPropertyNames<T>
// ): SpyObj<T> {
//     const spyObj: SpyObj<T> = {} as SpyObj<T>;
//     spyObj.constructor.prototype.name = baseClass.name;
//     methodNames.forEach((methodName: keyof T) => {
//         spyObj[methodName] = jest.fn() as jest.Mock;
//     });
//     propertyNames?.forEach((propertyName: keyof T) => {
//         spyObj[propertyName] = undefined;
//     });
//     (spyObj as any).__originalClass__ = baseClass;
//     return spyObj;
// }

/**
 * Get all the method names for a class, including inherited methods.
 * @param {Type<any> | AbstractType<any>} classType - Type<any> | AbstractType<any>
 * @returns An array of strings.
 */
// function getAllMethodNamesFor(classType: Type<any> | AbstractType<any>): Array<string> {
//     const properties = new Set<string>();
//     let currentClass = classType;
//     do {
//         if (!currentClass.prototype) {
//             break;
//         }
//         Object.getOwnPropertyNames(currentClass.prototype)
//             .filter((item) => {
//                 try {
//                     return typeof currentClass.prototype[item] === 'function';
//                 } catch (e) {
//                     return false;
//                 }
//             })
//             .forEach((item) => properties.add(item));
//     } while ((currentClass = Object.getPrototypeOf(currentClass)));
//     return Array.from(properties.keys());
// }
