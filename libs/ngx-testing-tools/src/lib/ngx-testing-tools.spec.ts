import {TestCallback} from './models/test-callback.interface';
import {And, context, Given, Then, When} from './ngx-testing-tools';
import {
    CONTEXT_HEADER_MESSAGE,
    DEFAULT_LABEL,
    NEED_THEN_CALL_BEFORE_AND,
    NEED_USE_CONTEXT_FOR_AND,
    NO_SPEC_FUNCTION_ERROR,
    NO_STACK_ERROR,
    THEN_LABEL_PREFIX,
} from './utils/constants';
import {noop} from './utils/noop';

// @ts-expect-error: for retrieve context.
const root = (1, eval)('this');

describe('Jasmine Given', () => {
    let fakeNumber: number | undefined;
    let actualResult: number | undefined;

    function addTwo(num: number | undefined): number {
        if (num == undefined) {
            return 0;
        }
        return num + 2;
    }

    async function getPromiseOf(value: number) {
        return await Promise.resolve(value);
    }

    Given(() => {
        fakeNumber = undefined;
        actualResult = undefined;
    });

    describe('should run "Given" before "When" and "When" before "Then"', () => {
        Given(() => {
            fakeNumber = 2;
        });

        When(() => {
            actualResult = addTwo(fakeNumber);
        });

        Then(() => {
            expect(actualResult).toBe(4);
        });
    });

    describe('should run the "When" after the "Given" even if declared in reverse', () => {
        When(() => {
            actualResult = addTwo(fakeNumber);
        });

        Given(() => {
            fakeNumber = 2;
        });

        Then(() => {
            expect(actualResult).toBe(4);
        });
    });

    describe('should run the "When" after the "Given" even if nested', () => {
        When(() => {
            actualResult = addTwo(fakeNumber);
        });

        describe('even if nested', () => {
            Given(() => {
                fakeNumber = 2;
            });

            Then(() => {
                expect(actualResult).toBe(4);
            });
        });
    });

    describe('should run more than one "When" after the "Given"', () => {
        When(() => {
            fakeNumber = 10;
        });

        When(() => {
            actualResult = addTwo(fakeNumber);
        });

        describe('even if nested', () => {
            Given(() => {
                fakeNumber = 3;
            });

            Then(() => {
                expect(actualResult).toBe(12);
            });
        });
    });

    describe('should handle done function in "Given"', () => {
        Given((done) => {
            fakeNumber = 2;
            done();
        });

        When(() => {
            actualResult = addTwo(fakeNumber);
        });

        Then(() => {
            expect(actualResult).toBe(4);
        });
    });

    describe('should support done function in "When"', () => {
        When((done) => {
            actualResult = addTwo(2);
            done();
        });

        Then(() => {
            expect(actualResult).toBe(4);
        });
    });

    describe('should support done function in "Then"', () => {
        Then((done) => {
            expect(true).toBe(true);
            done();
        });
    });

    describe('should handle async await in "Given"', () => {
        Given(async () => {
            fakeNumber = await getPromiseOf(1);
        });

        Given(async () => {
            fakeNumber = await getPromiseOf(2);
        });

        When(() => {
            actualResult = addTwo(fakeNumber);
        });

        Then(() => {
            expect(actualResult).toBe(4);
        });
    });

    describe('should handle async await in "When"', () => {
        When(async () => {
            fakeNumber = await getPromiseOf(1);
        });

        When(async () => {
            fakeNumber = await getPromiseOf(2);
            actualResult = addTwo(fakeNumber);
        });

        Then(() => {
            expect(actualResult).toBe(4);
        });
    });

    describe('should handle async await in "Then"', () => {
        When(() => {
            actualResult = addTwo(2);
        });

        Then(async () => {
            const expectedResult = await getPromiseOf(4);
            expect(actualResult).toBe(expectedResult);
        });
    });

    describe('Then() should pass an empty string to it() given no label', () => {
        it('should add the label', () => {
            const itSpy = jest.spyOn(root, 'it').mockImplementation();
            Then(noop);
            const actualLabel = itSpy.mock.calls[0]?.[0];
            expect(actualLabel).toBe(THEN_LABEL_PREFIX + DEFAULT_LABEL);
        });
    });

    describe('Then() should be able to have a label', () => {
        it('should add the label', () => {
            const itSpy = jest.spyOn(root, 'it').mockImplementation();
            const label = 'FAKE LABEL';
            Then(label, noop);
            const actualLabel = itSpy.mock.calls[1]?.[0];
            expect(actualLabel).toBe(THEN_LABEL_PREFIX + label);
        });
    });

    describe('Then() is called with only a label', () => {
        it('should throw when called', () => {
            function errorThrowingCall() {
                (Then as any)('FAKE MESSAGE');
            }

            expect(errorThrowingCall).toThrowError(NO_SPEC_FUNCTION_ERROR);
        });
    });

    describe('And() is called without args', () => {
        it('should throw when called', () => {
            function errorThrowingCall() {
                (And as any)();
            }

            expect(errorThrowingCall).toThrowError(NO_SPEC_FUNCTION_ERROR);
        });
    });

    /*describe('should share "this" context', () => {
        Given(function (this: any) {
            this.fakeInitialNumber = 2;
            this.expectedResult = 4;
        });

        When(function (this: any) {
            actualResult = addTwo(this.fakeInitialNumber);
        });

        Then(function (this: any) {
            expect(actualResult).toEqual(this.expectedResult);
        });
    });*/

    describe('if error is thrown in the "Given" should show a meaningful message', () => {
        const FAKE_ERROR = 'FAKE ERROR';
        let actualPromiseFromGiven: Promise<unknown>;

        beforeEach(() => {
            jest.spyOn(root, 'beforeEach').mockImplementation((fn: unknown) => {
                actualPromiseFromGiven = (fn as Function)();
            });
            /*            spyOn(root, 'afterEach').and.callFake((fn: Function) => {
                fn();
            });*/
        });

        it('should work with a regular callback', async () => {
            Given(() => {
                throw new Error(FAKE_ERROR);
            });
            try {
                await actualPromiseFromGiven;
            } catch (err: any) {
                expect(err.message).toContain(`${CONTEXT_HEADER_MESSAGE} Given():`);
                expect(err.message).toContain(FAKE_ERROR);
            }
        });

        it('should work with a done callback', async () => {
            Given((_done) => {
                throw new Error(FAKE_ERROR);
            });
            try {
                await actualPromiseFromGiven;
            } catch (err: any) {
                expect(err.message).toContain(`${CONTEXT_HEADER_MESSAGE} Given():`);
                expect(err.message).toContain(FAKE_ERROR);
            }
        });

        it('should work with a done callback with passed error', async () => {
            Given((done) => {
                done(new Error(FAKE_ERROR));
            });
            try {
                await actualPromiseFromGiven;
            } catch (err: any) {
                expect(err.message).toContain(`${CONTEXT_HEADER_MESSAGE} Given():`);
                expect(err.message).toContain(FAKE_ERROR);
            }
        });

        it('should work with a done callback with passed error via done.fail()', async () => {
            Given((done) => {
                done.fail(new Error(FAKE_ERROR));
            });
            try {
                await actualPromiseFromGiven;
            } catch (err: any) {
                expect(err.message).toContain(`${CONTEXT_HEADER_MESSAGE} Given():`);
                expect(err.message).toContain(FAKE_ERROR);
            }
        });

        it('should work with an async callback', async () => {
            Given(async () => {
                throw new Error(FAKE_ERROR);
            });

            try {
                await actualPromiseFromGiven;
            } catch (err: any) {
                expect(err.message).toContain(`${CONTEXT_HEADER_MESSAGE} Given():`);
                expect(err.message).toContain(FAKE_ERROR);
            }
        });
    });

    describe('if an error gets thrown in "When" or "Then"', () => {
        let afterEachCache: Function[];
        let actualError: any;
        let actualPromiseReturnedFromIt: Promise<any>;

        const FAKE_ERROR_MESSAGE = 'FAKE ERROR';

        beforeEach(() => {
            actualError = undefined;
            afterEachCache = [];

            // make beforeEach run immediately when called inside the next "it" function
            jest.spyOn(root, 'beforeEach').mockImplementation((fn: unknown) => (fn as Function)());

            // queues up afterEach functions for cleanup purposes inside the next "it" function
            jest.spyOn(root, 'afterEach').mockImplementation((fn: unknown) => afterEachCache.push(fn as Function));

            // Because jasmine queues up these function,
            // the following line will get called after all the "it" functions in this spec file will get called
            // The purpose is to enable us to run "it()" immediately inside of another "it callback"
            // which otherwise throws an error...
            jest.spyOn(root, 'it').mockImplementation((_desc: unknown, fn: unknown) => {
                actualPromiseReturnedFromIt = (fn as Function)();
            });
        });

        async function checkOriginOfError(promise: Promise<TestCallback>, origin: string) {
            try {
                await promise;
            } catch (err: any) {
                actualError = err;
            } finally {
                afterEachCache.forEach((fn) => fn());
            }

            expect(actualError.message).toContain(`${CONTEXT_HEADER_MESSAGE} ${origin}():`);
            if (origin !== 'When') {
                expect(actualError.message).not.toContain('When():');
            }
            expect(actualError.message).toContain(FAKE_ERROR_MESSAGE);
        }

        it('should show a meaningful message if it was thrown in "When"', async () => {
            // without a "done"
            When(() => {
                throw new Error(FAKE_ERROR_MESSAGE);
            });
            // We must call "Then" or else the logic of "When" won't be called
            Then(noop);

            await checkOriginOfError(actualPromiseReturnedFromIt, 'When');
        });

        it('should show a meaningful message if it was thrown in "When" with a "done"', async () => {
            // WITH a "done"
            When((_done) => {
                throw new Error(FAKE_ERROR_MESSAGE);
            });
            // We must call "Then" or else the logic of "When" won't be called
            Then(noop);

            await checkOriginOfError(actualPromiseReturnedFromIt, 'When');
        });

        it(`should show a meaningful message ONLY ONCE
         if thrown in "When" with a "done"
         after multiple "When's with "done"s`, async () => {
            When((done) => {
                done();
            });

            When((done) => {
                done();
            });

            When((_done) => {
                throw new Error(FAKE_ERROR_MESSAGE);
            });
            // We must call "Then" or else the logic of "When" won't be called
            Then(noop);

            try {
                await actualPromiseReturnedFromIt;
            } catch (err: any) {
                actualError = err;
            } finally {
                afterEachCache.forEach((fn) => fn());
            }

            const howManyTimesTheContextMessageAppears = actualError.message.match(/An error was thrown in When\(\):/gm).length;

            expect(howManyTimesTheContextMessageAppears).toBe(1);
            expect(actualError.message).toContain(FAKE_ERROR_MESSAGE);
        });

        it(`should show a meaningful message if it was thrown in "When" with a "done"
          passed as a parameter`, async () => {
            // WITH a "done"
            When((done) => {
                done(new Error(FAKE_ERROR_MESSAGE));
            });
            Then(noop);

            await checkOriginOfError(actualPromiseReturnedFromIt, 'When');
        });

        it(`should show a meaningful message if it was thrown in "When" with a "done"
          passed via done.fail()`, async () => {
            // WITH a "done"
            When((done) => {
                done.fail(new Error(FAKE_ERROR_MESSAGE));
            });
            // We must call "Then" or else the logic of "When" won't be called
            Then(noop);

            await checkOriginOfError(actualPromiseReturnedFromIt, 'When');
        });

        it('should show a meaningful message if thrown as ERROR OBJECT in async "When"', async () => {
            When(noop);
            When(async () => {
                throw new Error(FAKE_ERROR_MESSAGE);
            });
            // We must call "Then" or else the logic of "When" won't be called
            Then(noop);

            await checkOriginOfError(actualPromiseReturnedFromIt, 'When');
        });

        it('should show a meaningful message if thrown as STRING in async "When"', async () => {
            When(async () => {
                throw FAKE_ERROR_MESSAGE;
            });
            // We must call "Then" or else the logic of "When" won't be called
            Then(noop);

            await checkOriginOfError(actualPromiseReturnedFromIt, 'When');
            expect(actualError.message).toContain(NO_STACK_ERROR);
        });

        it('should show a meaningful message if thrown as Object without stack in async "When"', async () => {
            When(async () => {
                throw {toString: () => FAKE_ERROR_MESSAGE};
            });
            // We must call "Then" or else the logic of "When" won't be called
            Then(noop);

            await checkOriginOfError(actualPromiseReturnedFromIt, 'When');
        });

        it('should show a meaningful message if it was thrown in "Then"', async () => {
            When(noop);

            Then(() => {
                throw new Error(FAKE_ERROR_MESSAGE);
            });

            await checkOriginOfError(actualPromiseReturnedFromIt, 'Then');
        });

        it('should show a meaningful message if it was thrown in "Then" with "done"', async () => {
            When(noop);

            // WITH a "done"
            Then((_done) => {
                throw new Error(FAKE_ERROR_MESSAGE);
            });

            await checkOriginOfError(actualPromiseReturnedFromIt, 'Then');
        });

        it(`should show a meaningful message if it was thrown in "Then" with a "done"
          passed as a parameter to "done"`, async () => {
            Then((done) => {
                done(new Error(FAKE_ERROR_MESSAGE));
            });

            await checkOriginOfError(actualPromiseReturnedFromIt, 'Then');
        });

        it(`should show a meaningful message if it was thrown in "Then" with a "done" passed via done.fail()`, async () => {
            Then((done) => {
                done.fail(new Error(FAKE_ERROR_MESSAGE));
            });

            await checkOriginOfError(actualPromiseReturnedFromIt, 'Then');
        });

        it('should show a meaningful message if thrown as ERROR OBJECT in async "Then"', async () => {
            When(noop);
            Then(async () => {
                throw new Error(FAKE_ERROR_MESSAGE);
            });

            await checkOriginOfError(actualPromiseReturnedFromIt, 'Then');
        });
    });

    describe('eliminating redundant test execution', () => {
        context('a traditional spec with numerous Then statements', () => {
            let timesGivenWasInvoked = 0,
                timesWhenWasInvoked = 0;
            Given(() => {
                timesGivenWasInvoked++;
            });
            When(() => {
                timesWhenWasInvoked++;
            });
            Then(() => expect(timesGivenWasInvoked).toBe(1));
            Then(() => expect(timesWhenWasInvoked).toBe(2));
            Then(() => expect(timesGivenWasInvoked).toBe(3));
            Then(() => expect(timesWhenWasInvoked).toBe(4));
        });

        context('a spec with one Then and multiple And statements', () => {
            let timesGivenWasInvoked = 0,
                timesWhenWasInvoked = 0;
            Given(() => {
                timesGivenWasInvoked++;
            });
            When(() => {
                timesWhenWasInvoked++;
            });
            Then(() => expect(timesGivenWasInvoked).toBe(1));
            And(() => expect(timesWhenWasInvoked).toBe(1));
            And(() => expect(timesGivenWasInvoked).toBe(1));
            And(() => expect(timesWhenWasInvoked).toBe(1));
            Then(() => expect(timesWhenWasInvoked).toBe(2));
        });
    });

    describe('if an error gets thrown in "context"', () => {
        let afterEachCache: Function[];
        let actualError: any;

        beforeEach(() => {
            actualError = undefined;
            afterEachCache = [];

            jest.spyOn(root, 'describe').mockImplementation((_desc: unknown, fn: unknown) => (fn as Function)());
            jest.spyOn(root, 'beforeEach').mockImplementation((fn: unknown) => (fn as Function)());
            jest.spyOn(root, 'afterEach').mockImplementation((fn: unknown) => afterEachCache.push(fn as Function));
            jest.spyOn(root, 'it').mockImplementation((_desc: unknown, fn: unknown) => (fn as Function)());
        });

        it('should throw a error if no "Then" was call first', async () => {
            context('in a context', () => {
                try {
                    And(noop);
                } catch (err: any) {
                    actualError = err;
                }
            });

            expect(actualError.message).toEqual(NEED_THEN_CALL_BEFORE_AND);
        });

        it('should throw a error if "And" was called without "context"', async () => {
            try {
                And(noop);
            } catch (err: any) {
                actualError = err;
            }
            expect(actualError.message).toEqual(NEED_USE_CONTEXT_FOR_AND);
        });
    });
});
