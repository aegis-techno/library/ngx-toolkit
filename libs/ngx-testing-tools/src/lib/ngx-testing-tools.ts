import {TestCallback} from './models/test-callback.interface';
import {NgxTestingTools} from './services/ngx-testing-tools.service';

beforeEach(function () {
    NgxTestingTools.currentUserContext = eval('this');
});

/**
 * Given Method
 * @param {TestCallback} givenSetupFn - The function that will be called to setup the test.
 */
export function Given(givenSetupFn: TestCallback): void {
    NgxTestingTools.Given(givenSetupFn);
}

/**
 * When Method
 * @param {TestCallback} whenSetupFn - The function that will be called to setup the test.
 */
export function When(whenSetupFn: TestCallback): void {
    NgxTestingTools.When(whenSetupFn);
}

/**
 * Then Method
 * @param {TestCallback | string} specFnOrThenLabel - This is the function that will be executed when the test is run. It
 * can also be a string that will be used as the label for the test.
 * @param {TestCallback} [specFn] - The test function to run.
 */
export function Then(specFnOrThenLabel: TestCallback | string, specFn?: TestCallback): void {
    NgxTestingTools.Then(specFnOrThenLabel, specFn);
}

/**
 * Replace Then for ignore Test
 * @param {TestCallback | string} _specFnOrThenLabel - This is the first parameter. It can be either a string or a
 * function. If it's a string, it's the label for the test. If it's a function, it's the test function.
 * @param {TestCallback} [_specFn] - The function that contains the test code.
 */
export function xThen(_specFnOrThenLabel: TestCallback | string, _specFn?: TestCallback) {
    return;
}

/**
 * Context Method
 * @param {string} label - The name of the test suite.
 * @param specFn - The function that contains the test code.
 */
export function context(label: string, specFn: () => void): void {
    NgxTestingTools.context(label, specFn);
}

/**
 * And Method
 * @param {TestCallback} specFn - The function that contains the test code.
 */
export function And(specFn: TestCallback): void {
    NgxTestingTools.And(specFn);
}
