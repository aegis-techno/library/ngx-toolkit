export interface NgxLocalStorageSerializer<T = unknown> {
    serialize(value: T): string;

    deserialize(storedValue: string): T;
}
