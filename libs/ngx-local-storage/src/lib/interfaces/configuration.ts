import {NgxLocalStorageSerializer} from './serializer.interface';

export interface NgxLocalStorageConfiguration {
    prefix?: string;
    delimiter?: string;
    allowNull?: boolean;
    serializer?: NgxLocalStorageSerializer;
    storageType?: 'localStorage' | 'sessionStorage';
}
