import {Objects} from '@aegis-techno/ngx-core';
import {inject, Inject, Injectable} from '@angular/core';

import {NgxLocalStorageConfiguration} from '../interfaces/configuration';
import {NGX_LOCAL_STORAGE_CONFIG} from '../tokens/config.token';
import {NGX_LOCAL_STORAGE_SERIALIZER} from '../tokens/serializer.token';
import {NGX_LOCAL_STORAGE_STORAGE} from '../tokens/storage.token';

@Injectable({providedIn: 'root'})
export class NgxLocalStorageService {
    private readonly serializer = inject(NGX_LOCAL_STORAGE_SERIALIZER);

    private readonly storage = inject(NGX_LOCAL_STORAGE_STORAGE, {optional: true});

    constructor(@Inject(NGX_LOCAL_STORAGE_CONFIG) public readonly config: Partial<NgxLocalStorageConfiguration>) {}

    private static constructKey(key: string, prefix?: string, delimiter?: string): string {
        if (prefix) {
            return `${prefix}${delimiter ?? '_'}${key}`;
        }
        return key;
    }

    public set(key: string, value: unknown): void {
        if (this.canSerialize(value)) {
            const constructedKey = NgxLocalStorageService.constructKey(key, this.config.prefix, this.config.delimiter);
            this.storage?.setItem(constructedKey, this.serializer.serialize(value));
        } else {
            this.remove(key);
        }
    }

    public get(key: string): any {
        try {
            const constructedKey = NgxLocalStorageService.constructKey(key, this.config.prefix, this.config.delimiter);
            const storedItem = this.storage?.getItem(constructedKey);
            return storedItem ? this.serializer.deserialize(storedItem) : null;
        } catch (_error) {
            return null;
        }
    }

    public remove(key: string): void {
        try {
            const constructedKey = NgxLocalStorageService.constructKey(key, this.config.prefix, this.config.delimiter);
            this.storage?.removeItem(constructedKey);
        } catch (_error) {
            // ignore
        }
    }

    public clear(): void {
        try {
            this.storage?.clear();
        } catch (_error) {
            // ignore
        }
    }

    private canSerialize(value: any) {
        return this.config.allowNull || (!this.config.allowNull && `${value}` !== 'null' && Objects.isDefined(value));
    }
}
