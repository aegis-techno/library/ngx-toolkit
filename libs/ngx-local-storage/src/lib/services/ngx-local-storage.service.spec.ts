import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {TestBed} from '@angular/core/testing';
import {NgxLocalStorageConfiguration} from '../interfaces/configuration';
import {NGX_LOCAL_STORAGE_CONFIG} from '../tokens/config.token';
import {NgxLocalStorageService} from './ngx-local-storage.service';

describe('NgxLocalStorageService', () => {
    let service: NgxLocalStorageService;
    let config: Partial<NgxLocalStorageConfiguration>;

    const prefix = 'TEST';
    const key = 'key';

    Given(() => {
        localStorage.clear();
        config = {
            prefix: prefix,
        };
    });

    When(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: NGX_LOCAL_STORAGE_CONFIG,
                    useValue: config,
                },
            ],
        });
        service = TestBed.inject(NgxLocalStorageService);
    });

    Then('should be created', () => {
        expect(service).toBeDefined();
    });

    describe('when call set', () => {
        let value: any;

        Given(() => {
            value = true;
        });

        When(() => {
            service.set(key, value);
        });

        Then('should set Value', () => {
            expect(localStorage.getItem(`${prefix}_${key}`)).toBe(value.toString());
        });

        describe("and config don't have prefix", () => {
            Given(() => {
                config = {};
            });

            Then('should set Value', () => {
                expect(localStorage.getItem(`${key}`)).toBe(value.toString());
            });
        });

        describe('and call get', () => {
            let valueStorage: any;

            When(() => {
                valueStorage = service.get(key);
            });

            Then('should get Value', () => {
                expect(valueStorage).toBe(true);
            });

            describe('given value is null', () => {
                Given(() => {
                    value = null;
                    config = {
                        allowNull: true,
                    };
                });

                Then('should set Value', () => {
                    expect(valueStorage).toBe(null);
                });

                describe("and config don't allow null", () => {
                    Given(() => {
                        config = {
                            allowNull: false,
                        };
                    });

                    Then('should set Value', () => {
                        expect(valueStorage).toBe(null);
                    });
                });
            });
        });

        describe('and call remove', () => {
            When(() => {
                service.remove(key);
            });

            Then('should remove Value', () => {
                expect(localStorage.getItem(`${prefix}_${key}`)).toBeNull();
            });
        });

        describe('and call clear', () => {
            When(() => {
                service.clear();
            });

            Then('should remove Value', () => {
                expect(localStorage.getItem(`${prefix}_${key}`)).toBeNull();
            });
        });
    });
});
