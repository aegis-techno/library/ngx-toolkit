import {InjectionToken} from '@angular/core';
import {NgxLocalStorageConfiguration} from '../interfaces/configuration';

export const NGX_LOCAL_STORAGE_DEFAULT_CONFIG = (): NgxLocalStorageConfiguration => {
    return {
        allowNull: true,
        storageType: 'localStorage',
        delimiter: '_',
    };
};

/**
 * Provides an injection token for the service configuration.
 */
export const NGX_LOCAL_STORAGE_CONFIG = new InjectionToken<NgxLocalStorageConfiguration>('NgxLocalstorageConfiguration', {
    providedIn: 'root',
    factory: NGX_LOCAL_STORAGE_DEFAULT_CONFIG,
});
