import {DOCUMENT} from '@angular/common';
import {inject, InjectionToken} from '@angular/core';
import {NGX_LOCAL_STORAGE_CONFIG} from './config.token';

export const NGX_LOCAL_STORAGE_STORAGE = new InjectionToken<Storage>('Token providing choosen storage', {
    factory: () => {
        const {storageType} = inject(NGX_LOCAL_STORAGE_CONFIG);
        const {defaultView} = inject(DOCUMENT);
        return defaultView?.[storageType ?? 'localStorage'] as Storage;
    },
});
