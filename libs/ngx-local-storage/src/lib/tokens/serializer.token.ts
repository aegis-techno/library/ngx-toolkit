import {InjectionToken} from '@angular/core';
import {NgxLocalStorageDefaultSerializer} from '../classes/default-serializer';
import {NgxLocalStorageSerializer} from '../interfaces/serializer.interface';

export const NGX_LOCAL_STORAGE_SERIALIZER = new InjectionToken<NgxLocalStorageSerializer>('StorageSerializer', {
    providedIn: 'root',
    factory: () => new NgxLocalStorageDefaultSerializer(),
});
