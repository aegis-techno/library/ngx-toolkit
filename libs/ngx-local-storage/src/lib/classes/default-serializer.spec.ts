import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {NgxLocalStorageDefaultSerializer} from './default-serializer';

describe('NgxLocalStorageDefaultSerializer', () => {
    let serializer: NgxLocalStorageDefaultSerializer;

    When(() => {
        serializer = new NgxLocalStorageDefaultSerializer();
    });

    Then('should be created', () => {
        expect(serializer).toBeDefined();
    });

    describe('when call serialize', () => {
        let result: string;
        let expectResult: string;
        let obj: {id: number};

        Given(() => {
            obj = {id: 1};
            expectResult = '{"id":1}';
        });

        When(() => {
            result = serializer.serialize(obj);
        });

        Then('should be created', () => {
            expect(result).toBe(expectResult);
        });
    });

    describe('when call deserialize', () => {
        let str: any;
        let result: any;
        let expectResult: any;

        When(() => {
            result = serializer.deserialize(str);
        });

        describe('with undefined value', () => {
            Given(() => {
                str = '{"id":1}';
                expectResult = {id: 1};
            });

            Then('should be created', () => {
                expect(result).toEqual(expectResult);
            });
        });
    });
});
