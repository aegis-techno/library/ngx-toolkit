import {NgxLocalStorageSerializer} from '../interfaces/serializer.interface';

export class NgxLocalStorageDefaultSerializer implements NgxLocalStorageSerializer {
    public serialize<T = unknown>(value: T): string {
        return JSON.stringify(value);
    }

    public deserialize<T = unknown>(storedValue: string): T {
        return JSON.parse(storedValue);
    }
}
