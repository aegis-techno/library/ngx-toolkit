# ngx-local-storage

An Angular wrapper for local storage access.

-   [Installation](#installation)
-   [Usage](#usage)
    -   [Serialization](#serialization)
-   [API](#api)

## Installation

Install via npm:

```shell
npm install @aegis-techno/ngx-local-storage --save
```

Install manually:

-   Add `@aegis-techno/ngx-local-storage` into `package.json`.
-   Run `npm install`.
-   Import `NgxLocalStorageModule.forRoot()` into the root module of your default application (or defining a project by
    using the `--project <PROJECT_NAME>` and/or `--module <MODULE_PATH>` CLI parameters).

## Usage

### 1. Import `NgxLocalStorageModule`

```ts
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgxLocalStorageModule} from '@aegis-techno/ngx-local-storage';

@NgModule({
    imports: [BrowserModule, NgxLocalStorageModule.withConfig()],
    bootstrap: [AppComponent],
})
export class AppModule {}
```

#### Configuration (`NgxLocalStorageModule.withConfig(moduleConfig)`)

-   **prefix**
    -   Type: `string`
    -   Determines the key prefix.
    -   Default: **null**
-   **allowNull**
    -   Type: `boolean`
    -   Determines if _null | 'null'_ values should be stored.
    -   Default: **true**
-   **saveEventName**
    -   Type: `string`
    -   Determines which key listen from EventBusService.
    -   Default: **undefined**

## Serialization

### Default serialization

The library utilizes the `JSON.stringify()/JSON.parse()` mechanics to pass values (of any type) to and from localstorage
per default. If you wish you can override that behaviour by injecting your own custom serializer (app wide) or pass one
per stoage call.

#### App wide serializer

Inject your custom serializer implentation using the provided injection token:

```ts
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgxLocalStorageModule, NGX_LOCAL_STORAGE_SERIALIZER} from 'ngx-localstorage';

@NgModule({
    imports: [
        BrowserModule,
        NgxLocalStorageModule.forRoot()
    ],
    bootstrap: [AppComponent],
    providers: [
        {
            provide: NGX_LOCAL_STORAGE_SERIALIZER,
            useClass: <custom serializer implementing StorageSerializer >
        }
    ]
})
export class AppModule {
}
```

#### Per call serializer

Every `set()/get` call on `NgxLocalStorageService` supports to pass an optional (de)seriaizer. If none is provided the
app wide (or default) one is used.

## API

### LocalStorageService

#### Methods

-   `set(key: string, value: string, options? : {prefix?: string, serilizer?: StorageSerializer}): void`: Adds the value
    with the given key or updates an existing entry using either the provided or default serializer.
-   `get(key: string, options? : {prefix?: string, serilizer?: StorageSerializer}): string | null | undefined`: Gets the
    entry specified by the given key or null using either the provided or default serializer.
-   `remove(key: string, prefix?: string): void`: Removes the entry specified by the given key.
-   `clear(): void`: Clears all entries of the applications local storage.

> Example

```ts
import {LocalStorageService} from '@aegis-techno/ngx-local-storage';

@Component({
    selector: 'app-storage-access',
    template: './storage-access.component.html',
})
export class StorageAccessComponent implements OnInit {
    constructor(private _storageService: NgxLocalStorageService) {}

    ngOnInit(): void {
        this._storageService.set('key', 'value');
        console.log('Value : ', this._storageService.get('key')); // expected "Value : value"
    }
}
```

### EventBusService support

> Example

```ts
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {LocalStorageService, NgxLocalStorageModule, NGX_LOCAL_STORAGE_SERIALIZER} from '@aegis-techno/ngx-local-storage';

@NgModule({
    imports: [
        BrowserModule,
        NgxLocalStorageModule.forRoot({
            saveEventName: 'saveEvent',
        }),
    ],
    bootstrap: [StorageAccessComponent],
})
export class AppModule {}

@Component({
    selector: 'app-storage-access',
    template: './storage-access.component.html',
})
export class StorageAccessComponent implements OnInit {
    constructor(private _storageService: NgxLocalStorageService) {}

    ngOnInit(): void {
        EventBusService.getInstance<EventBusService>(EventBusService).emit({
            name: saveEventName,
            value: {
                key: key,
                value: 'value',
            },
        });
        console.log('Value : ', this._storageService.get('key')); // expected "Value : value"
    }
}
```
