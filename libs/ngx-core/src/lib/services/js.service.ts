import {Injectable} from '@angular/core';

/**
 * The service to manage JS operations.
 */
@Injectable({providedIn: 'root'})
export class JsService {
    /**
     * Escape the specified JS string value.
     *
     * @param str The JS string value to espace
     *
     * @return The escaped js
     */
    public escapeJS(str: string | undefined | null): string {
        if (!str) {
            return '';
        }

        return str
            .replaceAll('\r', '')
            .replaceAll('\n', '')
            .replaceAll('\r\n', '')
            .replaceAll(';', '')
            .replaceAll("'", '')
            .replaceAll('"', '');
    }

    /**
     * Evaluate the specified JS string value (escape the string value before execute it).
     *
     * @param str The string to evaluate
     *
     * @return The return value of the evaluated string
     */
    public evaluate(str: string | undefined | null): any {
        return eval(this.escapeJS(str));
    }
}
