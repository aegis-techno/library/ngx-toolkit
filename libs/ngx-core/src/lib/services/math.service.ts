import {Injectable} from '@angular/core';
import {JsService} from './js.service';

/**
 * The service to manage math operations.
 */
@Injectable({providedIn: 'root'})
export class MathService {
    /**
     * Create a new instance.
     *
     * @param jsService The service to manage JS operations
     */
    constructor(private jsService: JsService) {}

    /**
     * Perform calculation on the specified string equation (with real mathematical operations priorities).
     *
     * @param equation The equation to calculate
     *
     * @return The result
     */
    public calculate(equation: string): number {
        return this.jsService.evaluate(
            // To remove all digits directly after a division by 0
            equation.replaceAll(/\/0\d*/g, '')
        );
    }
}
