import {Then, When} from '@aegis-techno/ngx-testing-tools';
import {JsService} from './js.service';
import {MathService} from './math.service';

describe('MathService', () => {
    let mathService: MathService;
    let result!: number;

    When(() => {
        mathService = new MathService(new JsService());
    });

    Then('should be created', () => {
        expect(mathService).toBeTruthy();
    });

    describe('calculate with only +', () => {
        When(() => {
            result = mathService.calculate('1+2+3+4+5');
        });

        Then(() => {
            expect(result).toEqual(15);
        });
    });

    describe('calculate with only -', () => {
        When(() => {
            result = mathService.calculate('1-2-3-4-5');
        });

        Then(() => {
            expect(result).toEqual(-13);
        });
    });

    describe('calculate with only *', () => {
        When(() => {
            result = mathService.calculate('1*2*3*4*5');
        });

        Then(() => {
            expect(result).toEqual(120);
        });
    });

    describe('calculate with only /', () => {
        When(() => {
            result = mathService.calculate('50/25/4');
        });

        Then(() => {
            expect(result).toEqual(0.5);
        });
    });

    describe('calculate with operation priority', () => {
        When(() => {
            result = mathService.calculate('2+2*8');
        });

        Then(() => {
            expect(result).toEqual(18);
        });
    });

    describe('calculate with parenthesis rules', () => {
        When(() => {
            result = mathService.calculate('(2+2)*8');
        });

        Then(() => {
            expect(result).toEqual(32);
        });
    });

    describe('calculate with invalid equation (without error)', () => {
        Then(() => {
            let error = undefined;

            try {
                mathService.calculate('2++++-7');
            } catch (e) {
                error = e;
            }

            expect(error).toBeDefined();
        });
    });

    describe('calculate with invalid divide operation (without error)', () => {
        When(() => {
            result = mathService.calculate('15/0+8');
        });

        Then(() => {
            expect(result).toEqual(23);
        });
    });

    describe('calculate with invalid divide operation followed directly by a valid digit (without error)', () => {
        When(() => {
            result = mathService.calculate('15/015');
        });

        Then(() => {
            expect(result).toEqual(15);
        });
    });
});
