import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {Singleton} from './singleton.decorator';

@Singleton()
class TestSingleton1 {
    public test(): string {
        return '1';
    }
}

@Singleton()
class TestSingleton2 {
    public test(): string {
        return '2';
    }
}

@Singleton()
class TestSingletonChild {
    public test(): string {
        return 'child';
    }
}

@Singleton()
class TestSingletonParent {
    constructor(private readonly child: TestSingletonChild) {}

    public testCallSingletonChild(): string {
        return this.child.test();
    }
}

@Singleton()
class TestSingletonValue {
    constructor(private readonly value: string) {}

    public testValue(): string {
        return this.value;
    }
}

// https://stackoverflow.com/questions/13955157/how-to-define-static-property-in-typescript-interface
describe('SingletonDecorator', () => {
    Given(() => {
        // delete TestSingleton1[singletonInstanceKey];
        // delete TestSingleton2[singletonInstanceKey];
    });

    describe('when create two separate instance', () => {
        let service1: TestSingleton1;
        let service2: TestSingleton2;

        When(() => {
            service1 = new TestSingleton1();
            service2 = new TestSingleton2();
        });

        Then('should be created', () => {
            expect(service1).toBeDefined();
            expect(service2).toBeDefined();
        });

        Then('instance should be unique', () => {
            expect(service1).toBe(new TestSingleton1());
            expect(service2).toBe(new TestSingleton2());
        });

        Then('service should have different behavior', () => {
            expect(service1.test()).toBe('1');
            expect(service2.test()).toBe('2');
        });
    });

    describe('when get instance', () => {
        let service1: TestSingleton1;

        When(() => {
            service1 = new TestSingleton1();
        });

        Then('should be created', () => {
            expect(service1).toBeDefined();
        });

        Then('instance should be unique', () => {
            expect(service1).toBe(new TestSingleton1());
        });

        Then('service should have different behavior', () => {
            expect(new TestSingleton1().test()).toBe('1');
        });
    });

    describe('when create two nested instance', () => {
        let parent: TestSingletonParent;

        When(() => {
            parent = new TestSingletonParent(new TestSingletonChild());
        });

        Then('should can pass args', () => {
            expect(parent.testCallSingletonChild()).toBe('child');
        });
    });

    describe('when constructor take value is recreate', () => {
        let service: TestSingletonValue;

        When(() => {
            service = new TestSingletonValue('Hell');
            service = new TestSingletonValue('Hello');
        });

        Then('should stay first passed args', () => {
            expect(service.testValue()).toBe('Hell');
        });
    });
});
