import {Objects} from '../utils/objects.util';

export const singletonInstanceKey = Symbol();

type SingletonType<BaseClassType extends new (...args: Array<any>) => any> = BaseClassType & {
    [singletonInstanceKey]: BaseClassType extends new (...args: Array<any>) => infer Singleton ? Singleton : never;
};

export function Singleton() {
    return <BaseClassType extends new (...args: Array<any>) => any>(type: BaseClassType): SingletonType<BaseClassType> => {
        return new Proxy(type, {
            construct(target: SingletonType<BaseClassType>, argsList: Array<any>, newTarget) {
                if (Objects.isNotDefined(target[singletonInstanceKey])) {
                    target[singletonInstanceKey] = Reflect.construct(target, argsList, newTarget);
                }
                return target[singletonInstanceKey];
            },
        }) as SingletonType<BaseClassType>;
    };
}
