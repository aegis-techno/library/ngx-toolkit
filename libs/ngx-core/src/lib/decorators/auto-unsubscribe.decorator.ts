import {Objects} from '../utils/objects.util';
import {Strings} from '../utils/strings.util';

export interface AutoUnsubscribeConfig {
    blackList: Array<string>;
    arrayName: string;
    event: string;
}

const defaultConfig: AutoUnsubscribeConfig = {
    blackList: [],
    arrayName: '',
    event: 'ngOnDestroy',
};

export function AutoUnsubscribe(config?: Partial<AutoUnsubscribeConfig>) {
    return function (constructor: Function) {
        const event: string = config?.event ?? defaultConfig.event;
        const arrayName: string = config?.arrayName ?? defaultConfig.arrayName;
        const blackList: Array<string> = config?.blackList ?? defaultConfig.blackList;

        const original = constructor.prototype[event];

        constructor.prototype[event] = function (...args: any[]) {
            if (Objects.isFunction(original)) {
                original.apply(this, args);
            }

            if (Strings.isNotBlank(arrayName)) {
                doUnsubscribeIfArray(this[arrayName]);
                return;
            }

            for (const propName of Object.getOwnPropertyNames(this)) {
                if (!blackList.includes(propName)) {
                    doUnsubscribe(this[propName]);
                }
            }
        };
    };
}

const doUnsubscribeIfArray = (subscriptionsArray: Array<any>) => {
    if (Objects.isArray(subscriptionsArray)) {
        subscriptionsArray.forEach(doUnsubscribe);
    }
};

const doUnsubscribe = (subscription: any) => {
    if (Objects.isFunction(subscription?.unsubscribe)) {
        subscription.unsubscribe();
    }
};
