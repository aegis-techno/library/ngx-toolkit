/**
 * Utils File
 */
import {Optional} from '../models/optional.type';
import {Arrays} from './arrays.util';
import {Strings} from './strings.util';

/**
 * Objects Tools File
 */
export class Objects {
    /**
     * If the value is null, return true, otherwise return false.
     * @param value - Optional<T>
     * @returns A boolean value.
     */
    public static isNull<T>(value: Optional<T>): value is null {
        return value === null;
    }

    /**
     * If the value is undefined, return true, otherwise return false.
     * @param value - Optional<T>
     * @returns A boolean value.
     */
    public static isUndefined<T>(value: Optional<T>): value is undefined {
        return value === undefined;
    }

    /**
     * If the value is null or undefined, return true, otherwise return false.
     * @param value - Optional<T>
     * @returns A boolean value.
     */
    public static isNotDefined<T>(value: Optional<T>): value is null | undefined {
        return Objects.isNull(value) || Objects.isUndefined(value);
    }

    /**
     * If the value is not null, return true.
     * @param value - Optional<T>
     * @returns A boolean value.
     */
    public static isNotNull<T>(value: Optional<T>): value is T | undefined {
        return !Objects.isNull(value);
    }

    /**
     * If the value is not undefined, return true.
     * @param value - Optional<T>
     * @returns A boolean value.
     */
    public static isNotUndefined<T>(value: Optional<T>): value is null | T {
        return !Objects.isUndefined(value);
    }

    /**
     * Returns true if the value is not null or undefined.
     * @param value - Optional<T>
     * @returns A boolean value.
     */
    public static isDefined<T>(value: Optional<T>): value is T {
        return !Objects.isNotDefined(value);
    }

    /**
     * If x is a string, return true, otherwise return false.
     * @param {any} value - any - This is the parameter that we're going to check.
     * @returns A boolean value.
     */
    public static isString(value: any): value is string {
        return typeof value === 'string';
    }

    /**
     * If x is a number, return true, otherwise return false.
     * @param {any} value - any - This is the parameter that we're going to check.
     * @returns A boolean value.
     */
    public static isNumber(value: any): value is number {
        return typeof value === 'number';
    }

    /**
     * If x is not null, is an object, and is not an array, then x is an object.
     * @param {any} value - any - The value to check.
     * @returns A boolean value.
     */
    public static isObject(value: any): value is object {
        return value !== null && typeof value === 'object' && !(value instanceof Array);
    }

    /**
     * If x is not null, and is an object, and is an instance of Array, then x is an array.
     * @param {any} value - any - The parameter we're checking.
     * @returns A boolean value.
     */
    public static isArray(value: any): value is Array<any> {
        return value !== null && typeof value === 'object' && value instanceof Array;
    }

    /**
     * If x is a Function, return true, otherwise return false.
     * @param {any} value - any - This is the parameter that we're going to check.
     * @returns A boolean value.
     */
    public static isFunction(value: any): value is Function {
        return typeof value === 'function';
    }

    /**
     * If the type of the left operand is the same as the type of the right operand, then return true, otherwise return
     * false.
     * @param {unknown} left - unknown
     * @param {unknown} right - The type to check against.
     * @returns a boolean value.
     */
    public static isSameType(left: unknown, right: unknown): left is typeof right {
        return typeof left === typeof right;
    }

    /**
     * Create a copy of the specified value (JSON stringify and parse method).
     * Only properties are availables on the created copy, not methods.
     * If value is not an object return null.
     *
     * @param value The value to copy
     *
     * @return The copy
     */
    public static cloneProperties(value: unknown): object | null {
        if (Objects.isNotDefined(value)) {
            return null;
        }
        if (!Objects.isObject(value) && !Objects.isArray(value)) {
            return null;
        }
        return JSON.parse(JSON.stringify(value));
    }

    /**
     * It compares two objects and returns true if they are equal.
     * @param {unknown} lValue - The left value to compare.
     * @param {unknown} rValue - The right value to compare.
     */
    public static equals(lValue: unknown, rValue: unknown) {
        if (lValue === rValue) {
            return true;
        }

        if (Objects.isNotDefined(lValue) || Objects.isNotDefined(rValue)) {
            return false;
        }

        if (typeof lValue !== typeof rValue) {
            return false;
        }

        if (Objects.isArray(lValue) && Objects.isArray(rValue)) {
            if (lValue.length != rValue.length) {
                return false;
            }
            for (let i = 0; i < lValue.length; i++) {
                if (!Objects.equals(lValue[i], rValue[i])) {
                    return false;
                }
            }
            return true;
        }

        if (Objects.isObject(lValue) && Objects.isObject(rValue)) {
            return (
                JSON.stringify(Object.entries(lValue).reduce((r: any, k) => ((r[k[0]] = k[1]), r), {})) ===
                JSON.stringify(Object.entries(rValue).reduce((r: any, k) => ((r[k[0]] = k[1]), r), {}))
            );
        }
        return JSON.stringify(lValue) == JSON.stringify(rValue);
    }

    /**
     * It clones an object and deletes all blank properties.
     * @param {unknown} value - The object to clone.
     * @returns A copy of the object with all blank properties removed.
     */
    public static cloneAndDeleteBlankProperties(value: unknown): object | null {
        const valueCopy = Objects.cloneProperties(value);

        if (Objects.isNull(valueCopy)) {
            return null;
        }

        Arrays.coerceArray(valueCopy).forEach((o) => {
            Objects.deleteBlankProperties(o);
        });

        return valueCopy;
    }

    /**
     * It takes an object and deletes all the properties that have a blank value
     * @param {object} obj - object - The object to delete blank properties from.
     * @returns The object with the blank properties removed.
     */
    public static deleteBlankProperties(obj: object): object {
        Object.entries(obj).forEach(([key, value]) => {
            if (Objects.isNotDefined(value)) {
                delete (obj as any)[key];
            } else if (Objects.isString(value) && Strings.isBlank(value)) {
                delete (obj as any)[key];
            }
        });

        return obj;
    }
}
