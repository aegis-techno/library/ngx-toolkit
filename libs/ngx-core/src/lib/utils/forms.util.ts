import {AbstractControl, FormArray, FormControl, FormGroup, UntypedFormGroup, ValidationErrors} from '@angular/forms';
import {Objects} from './objects.util';

/**
 * Forms Tools File
 */
export class Forms {
    /**
     * It sets the value of a form control, but only if the value is different from the current value.
     * @param {AbstractControl | null} control - The control to set the value on.
     * @param {any} value - The value to set.
     * @param {any} [options] - {
     * @returns The return value is the value of the control.
     */
    public static setSafeDistinctValue(control: AbstractControl | null, value: any, options?: any): void {
        if (Objects.isNotDefined(control)) {
            return;
        }
        Forms.setDistinctValue(control, value, options);
    }

    /**
     * If the value of the control is not equal to the value passed in, then set the value of the control to the value
     * passed in.
     * @param {AbstractControl} control - AbstractControl - The control to set the value on.
     * @param {any} value - The value to set the control to.
     * @param {any} [options] - This is an optional parameter that can be used to pass in options to the setValue method.
     */
    public static setDistinctValue(control: AbstractControl, value: any, options?: any): void {
        if (control.value !== value) {
            control.setValue(value, options);
        }
    }

    /**
     * It disables a form control if it's not already disabled, and enables it if it's not already enabled.
     * @param {AbstractControl} formControl - AbstractControl - The form control to set the disabled state on.
     * @param {boolean} disable - boolean - Whether to disable the form control or not
     * @param [options] - Partial<{ onlySelf: boolean; emitEvent: boolean }>
     */
    public static setDistinctDisabledState(
        formControl: AbstractControl,
        disable: boolean,
        options?: Partial<{onlySelf: boolean; emitEvent: boolean}>
    ): void {
        if (disable && formControl.enabled) {
            formControl.disable(options);
        } else if (!disable && formControl.disabled) {
            formControl.enable(options);
        }
    }

    /**
     * It returns true if every form group in the array has a form control with the given name and that form control is
     * disabled.
     * @param formGroups - Array<FormGroup> - An array of FormGroups
     * @param {string} formControlName - The name of the form control you want to check.
     * @returns A boolean value.
     */
    public static isEveryControlNamedIsDisabled(formGroups: Array<FormGroup>, formControlName: string): boolean {
        return formGroups.every((formGroup) => formGroup.get(formControlName)?.disabled);
    }

    /**
     * Returns true if every form group in the array has a control with the given name that is enabled.
     *
     * @param formGroups - Array<UntypedFormGroup> - An array of form groups.
     * @param {string} formControlName - The name of the form control you want to check.
     * @returns A boolean value.
     */
    public static isEveryControlNamedIsEnabled(formGroups: Array<UntypedFormGroup>, formControlName: string): boolean {
        return formGroups.every((formGroup: UntypedFormGroup) => formGroup.get(formControlName)!.enabled);
    }

    public static getErrors(formControl?: AbstractControl | null): ValidationErrors | null {
        if (formControl === null) {
            return null;
        }

        if (formControl instanceof FormControl) {
            // Return FormControl errors or null
            return formControl.errors ?? null;
        }

        if (formControl instanceof FormGroup || formControl instanceof FormArray) {
            const groupErrors: ValidationErrors | null = formControl.errors;
            // Form group can contain errors itself, in that case add'em
            const formErrors: ValidationErrors | null = groupErrors ? {groupErrors} : {};
            Object.keys(formControl.controls).forEach((key) => {
                // Recursive call of the FormGroup fields
                const error = this.getErrors(formControl.get(key));
                if (error !== null) {
                    // Only add error if not null
                    formErrors[key] = error;
                }
            });
            // Return FormGroup errors or null
            return Object.keys(formErrors).length > 0 ? formErrors : null;
        }

        return null;
    }
}
