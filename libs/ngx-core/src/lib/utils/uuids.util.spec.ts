import {Then, When} from '@aegis-techno/ngx-testing-tools';
import {UUIDs} from './uuids.util';

describe('UUIDs', () => {
    describe('when generate GUID', () => {
        let guid: string;

        When(() => {
            guid = UUIDs.generateUUID4();
        });

        Then('should generate GUID', () => {
            expect(guid).toBeDefined();
        });

        Then('should respect pattern', () => {
            expect(guid.match(/^[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-[a-z0-9]{4}-[a-z0-9]{12}$/)).toBeDefined();
        });

        Then('should generate different GUID', () => {
            expect(guid).not.toBe(UUIDs.generateUUID4());
        });
    });
});
