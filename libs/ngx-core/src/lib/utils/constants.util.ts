export class Constants {
    public static DISPLAY_LOG = true;

    public static DEBOUNCE_DURATION = 300;
}
