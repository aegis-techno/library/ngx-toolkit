import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {Arrays} from './arrays.util';

describe('Arrays tools', () => {
    let arrValue: Array<any> | null | undefined;
    let actualResult: boolean;

    const noopFn = () => {
        return;
    };
    const emptyObject = {};

    describe('should find if arrays is empty', () => {
        When(() => {
            actualResult = Arrays.isEmpty(arrValue);
        });

        const arr: Array<any> = [
            {desc: 'undefined', obj: undefined, result: true},
            {desc: 'null', obj: null, result: true},
            {desc: 'has many item', obj: ['a', 'a'], result: false},
            {desc: 'has only one item', obj: [1], result: false},
            {desc: 'is empty', obj: [], result: true},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects is ' + arr[i].desc, () => {
                Given(() => {
                    arrValue = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe("should return Array of arg, or arg if it's already a array", () => {
        let arrResult: Array<any> | null | undefined;

        When(() => {
            arrResult = Arrays.coerceArray(arrValue);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: [null]},
            {desc: 'undefined', obj: undefined, result: []},
            {desc: 'empty string', obj: '', result: ['']},
            {desc: 'string', obj: 'a', result: ['a']},
            {desc: 'number', obj: 1, result: [1]},
            {desc: 'object', obj: emptyObject, result: [emptyObject]},
            {desc: 'array', obj: [], result: []},
            {desc: 'array with item', obj: ['a'], result: ['a']},
            {desc: 'function', obj: noopFn, result: [noopFn]},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if object is ' + arr[i].desc, () => {
                Given(() => {
                    arrValue = arr[i].obj;
                });
                Then(() => {
                    expect(arrResult).toEqual(arr[i].result);
                });
            });
        }
    });

    describe('should return Array of args', () => {
        let arrResult: Array<any> | null | undefined;

        When(() => {
            arrResult = Arrays.of(arrValue);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: [null]},
            {desc: 'undefined', obj: undefined, result: []},
            {desc: 'empty string', obj: '', result: ['']},
            {desc: 'string', obj: 'a', result: ['a']},
            {desc: 'number', obj: 1, result: [1]},
            {desc: 'object', obj: emptyObject, result: [emptyObject]},
            {desc: 'array', obj: [], result: [[]]},
            {desc: 'array with item', obj: ['a'], result: [['a']]},
            {desc: 'function', obj: noopFn, result: [noopFn]},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if object is ' + arr[i].desc, () => {
                Given(() => {
                    arrValue = arr[i].obj;
                });
                Then(() => {
                    expect(arrResult).toEqual(arr[i].result);
                });
            });
        }
    });
});
