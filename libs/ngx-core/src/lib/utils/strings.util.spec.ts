import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {Optional} from '../models/optional.type';
import {Strings} from './strings.util';

describe('String tools', () => {
    let obj: Optional<string>;

    describe('should find if a string is Blank', () => {
        let actualResult: boolean;

        When(() => {
            actualResult = Strings.isBlank(obj);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: true},
            {desc: 'undefined', obj: undefined, result: true},
            {desc: 'empty string', obj: '', result: true},
            {desc: 'not blank string', obj: ' a  a ', result: false},
            {desc: 'blank string', obj: '   ', result: true},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if a string is not Blank', () => {
        let actualResult: boolean;

        When(() => {
            actualResult = Strings.isNotBlank(obj);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'not blank string', obj: ' a  a ', result: true},
            {desc: 'blank string', obj: '   ', result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if a string is empty', () => {
        let actualResult: boolean;

        When(() => {
            actualResult = Strings.isEmpty(obj);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: true},
            {desc: 'undefined', obj: undefined, result: true},
            {desc: 'empty string', obj: '', result: true},
            {desc: 'not blank string', obj: ' a  a ', result: false},
            {desc: 'blank string', obj: '   ', result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if a string is not Blank', () => {
        let actualResult: boolean;

        When(() => {
            actualResult = Strings.isNotEmpty(obj);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'not blank string', obj: ' a  a ', result: true},
            {desc: 'blank string', obj: '   ', result: true},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should capitalize the first letter', () => {
        let actualResult: string;

        When(() => {
            actualResult = Strings.capitalizeFirstLetter(obj as any);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: ''},
            {desc: 'undefined', obj: undefined, result: ''},
            {desc: 'empty string', obj: '', result: ''},
            {
                desc: 'not blank and not trim string',
                obj: ' a  a ',
                result: ' a  a ',
            },
            {desc: 'lower string', obj: 'aa', result: 'Aa'},
            {desc: 'upper string', obj: 'Aa', result: 'Aa'},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should format', () => {
        let actualResult: string;
        let params: Array<string>;

        When(() => {
            actualResult = Strings.format(obj as any, ...params);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, params: [], result: ''},
            {desc: 'undefined', obj: undefined, params: [], result: ''},
            {desc: 'empty string', obj: '', params: [], result: ''},
            {desc: 'not param', obj: 'T', params: [], result: 'T'},
            {desc: 'one param', obj: 'T{%0}', params: ['1'], result: 'T1'},
            {desc: 'missing param', obj: 'T{%0}', params: [], result: 'T{%0}'},
            {
                desc: 'two param',
                obj: 'T{%0} {%1}',
                params: ['1', '2'],
                result: 'T1 2',
            },
            {
                desc: 'two same param',
                obj: 'T{%0} {%0}',
                params: ['1', '2'],
                result: 'T1 1',
            },
            {
                desc: 'inverted params',
                obj: 'T{%1} {%0}',
                params: ['1', '2'],
                result: 'T2 1',
            },
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                    params = arr[i].params;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });
});
