/**
 * Strings Tools File
 */
import {Objects} from './objects.util';

/**
 * Booleans Tools File
 */
export class Booleans {
    /**
     * Convert the input to boolean, if input is literral true or number 1 or true return true else return false
     * @param {unknown} object - unknown
     * @returns An bo olean.
     */
    public static coerceBoolean(object: unknown): boolean {
        if (Objects.isNotDefined(object)) {
            return false;
        }
        if (Objects.isString(object)) {
            return object === 'true' || object === '1';
        }
        if (Objects.isNumber(object)) {
            return object === 1;
        }
        return object === true;
    }
}
