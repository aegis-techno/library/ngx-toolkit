import {Strings} from './strings.util';
import {Optional} from '../models/optional.type';

/**
 * Numbers Tools File
 */
export class Numbers {
    public static MAGNITUDE_OF_FLOATING_POINT_ERROR = 0.000000000000001;

    /**
     * Returns 0 if the specified value is 0 or -0. Else returns the specified value.
     * @param value value to normalize.
     */
    public static positifZeroOrValue(value: number): number {
        if (Math.abs(value) > 0) {
            return value;
        }
        return 0;
    }

    /**
     * Truncate the number
     * @param number the number
     * @param digits the digits to keep
     * @returns truncated number
     */
    public static truncateDecimals(number: number, digits: number): number {
        const multiplier = Math.pow(10, digits);
        const adjustedNum = number * multiplier;

        const sign: boolean = adjustedNum < 0;
        const mathRoundMethod: (x: number) => number = Math[sign ? 'ceil' : 'floor'];
        let truncatedNum = mathRoundMethod(adjustedNum);

        const epsilonError: number = this.MAGNITUDE_OF_FLOATING_POINT_ERROR * multiplier;

        if (mathRoundMethod(adjustedNum + epsilonError * (sign ? -1 : 1)) !== truncatedNum) {
            truncatedNum += sign ? -1 : 1;
        }
        return this.positifZeroOrValue(truncatedNum / multiplier);
    }

    /**
     * Round the number
     * @param number the number
     * @param digits the digits to keep
     * @returns rounded number
     */
    public static roundDecimals(number: number, digits: number): number {
        const multiplier = Math.pow(10, digits);
        const adjustedNum = number * multiplier;
        let roundedNum = Math.round(adjustedNum);

        const epsilonError: number = this.MAGNITUDE_OF_FLOATING_POINT_ERROR * multiplier;
        if (Math.round(adjustedNum + epsilonError) !== roundedNum) {
            roundedNum++;
        }
        return this.positifZeroOrValue(roundedNum / multiplier);
    }

    public static parseFormatted(str: string): number | null {
        if (Strings.isBlank(str)) {
            return null;
        }

        str = str.replace(',', '.').replace(/[^0-9.-]/g, '');

        const parsedFloat: number = parseFloat(str);
        if (isNaN(parsedFloat)) {
            return 0;
        }
        return parsedFloat;
    }

    /**
     * Get a not null value from the specified value (get the default value if the specified value is null)
     *
     * @param value The value to check
     * @param defaultValue The default value to get if the value if null, default is 0.0
     *
     * @return The not null value
     */
    public static getNotNullValue(value: Optional<number>, defaultValue = 0.0): number {
        return value ?? defaultValue;
    }

    public static equals(a: number, b: number, tolerance = 0.0): boolean {
        return Math.abs(a - b) <= tolerance;
    }
}
