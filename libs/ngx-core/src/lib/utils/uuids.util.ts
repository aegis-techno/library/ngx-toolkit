import {Singleton} from '../decorators/singleton.decorator';

@Singleton()
export class UUIDs {
    private static readonly UUID_FORMAT = [2, 1, 1, 1, 3];

    public static generateUUID4(): string {
        const randomUUIDFormat = UUIDs.UUID_FORMAT.map(UUIDs.generateNQuadraDigits).join('-');
        return `${randomUUIDFormat.substring(0, 13)}4${randomUUIDFormat.substring(15)}`;
    }

    private static generateNQuadraDigits(nbQuadraDigits: number) {
        let digits = '';
        for (let nb = 0; nb < nbQuadraDigits; nb++) {
            digits += UUIDs.generateQuadraDigits();
        }
        return digits;
    }

    private static generateQuadraDigits() {
        const randomNumber = Math.floor((1 + Math.random()) * 0x10000);
        return randomNumber.toString(16).substring(1);
    }
}
