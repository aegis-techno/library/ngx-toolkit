import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {Numbers} from './numbers.util';

describe('Numbers tools', () => {
    const epsilonError = Numbers.MAGNITUDE_OF_FLOATING_POINT_ERROR;

    describe('should truncate number', () => {
        let obj: number;
        let actualResult: number;

        When(() => {
            actualResult = Numbers.truncateDecimals(obj, 2);
        });

        const arr: Array<any> = [
            {desc: 'a entier', obj: 2, result: 2},
            {desc: 'number with 2 decimal', obj: 2.15, result: 2.15},
            {desc: 'number with other 2 decimal', obj: 2.14, result: 2.14},
            {desc: 'number with other 2 decimal', obj: 2.16, result: 2.16},
            {
                desc: 'number with 2 decimal plus epsilon error',
                obj: 2.15 + epsilonError,
                result: 2.15,
            },
            {
                desc: 'number with other 2 decimal plus epsilon error',
                obj: 2.14 + epsilonError,
                result: 2.14,
            },
            {
                desc: 'number with other 2 decimal plus epsilon error',
                obj: 2.16 + epsilonError,
                result: 2.16,
            },
            {
                desc: 'number with 2 decimal minus epsilon error',
                obj: 2.15 - epsilonError,
                result: 2.15,
            },
            {
                desc: 'number with other 2 decimal minus epsilon error',
                obj: 2.14 - epsilonError,
                result: 2.14,
            },
            {
                desc: 'number with other 2 decimal minus epsilon error',
                obj: 2.16 - epsilonError,
                result: 2.16,
            },
            {desc: 'number with 3 decimal', obj: 2.115, result: 2.11},
            {desc: 'number with other 3 decimal', obj: 2.114, result: 2.11},
            {desc: 'number with other 3 decimal', obj: 2.116, result: 2.11},
            {
                desc: 'number with 3 decimal plus epsilon error',
                obj: 2.115 + epsilonError,
                result: 2.11,
            },
            {
                desc: 'number with other 3 decimal plus epsilon error',
                obj: 2.114 + epsilonError,
                result: 2.11,
            },
            {
                desc: 'number with other 3 decimal plus epsilon error',
                obj: 2.116 + epsilonError,
                result: 2.11,
            },
            {
                desc: 'number with 3 decimal minus epsilon error',
                obj: 2.115 - epsilonError,
                result: 2.11,
            },
            {
                desc: 'number with other 3 decimal minus epsilon error',
                obj: 2.114 - epsilonError,
                result: 2.11,
            },
            {
                desc: 'number with other 3 decimal minus epsilon error',
                obj: 2.116 - epsilonError,
                result: 2.11,
            },
            {desc: 'zero', obj: 0, result: 0},
            {desc: 'zero plus epsilon error', obj: 0 + epsilonError, result: 0},
            {
                desc: 'zero minus epsilon error',
                obj: 0 - epsilonError,
                result: 0,
            },
            {desc: 'a minus entier', obj: -2, result: -2},
            {desc: 'minus number with 2 decimal', obj: -2.15, result: -2.15},
            {
                desc: 'minus number with other 2 decimal',
                obj: -2.14,
                result: -2.14,
            },
            {
                desc: 'minus number with other 2 decimal',
                obj: -2.16,
                result: -2.16,
            },
            {
                desc: 'minus number with 2 decimal plus epsilon error',
                obj: -2.15 + epsilonError,
                result: -2.15,
            },
            {
                desc: 'minus number with other 2 decimal plus epsilon error',
                obj: -2.14 + epsilonError,
                result: -2.14,
            },
            {
                desc: 'minus number with other 2 decimal plus epsilon error',
                obj: -2.16 + epsilonError,
                result: -2.16,
            },
            {
                desc: 'minus number with 2 decimal minus epsilon error',
                obj: -2.15 - epsilonError,
                result: -2.15,
            },
            {
                desc: 'minus number with other 2 decimal minus epsilon error',
                obj: -2.14 - epsilonError,
                result: -2.14,
            },
            {
                desc: 'minus number with other 2 decimal minus epsilon error',
                obj: -2.16 - epsilonError,
                result: -2.16,
            },
            {desc: 'minus number with 3 decimal', obj: -2.115, result: -2.11},
            {
                desc: 'minus number with other 3 decimal',
                obj: -2.114,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal',
                obj: -2.116,
                result: -2.11,
            },
            {
                desc: 'minus number with 3 decimal plus epsilon error',
                obj: -2.115 + epsilonError,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal plus epsilon error',
                obj: -2.114 + epsilonError,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal plus epsilon error',
                obj: -2.116 + epsilonError,
                result: -2.11,
            },
            {
                desc: 'have 3 decimal minus epsilon error',
                obj: -2.115 - epsilonError,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal minus epsilon error',
                obj: -2.114 - epsilonError,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal minus epsilon error',
                obj: -2.116 - epsilonError,
                result: -2.11,
            },
            {desc: 'minus zero', obj: -0, result: 0},
            {
                desc: 'minus zero plus epsilon error',
                obj: -0 + epsilonError,
                result: 0,
            },
            {
                desc: 'minus zero minus epsilon error',
                obj: -0 - epsilonError,
                result: 0,
            },
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if object is ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should round number', () => {
        let obj: number;
        let actualResult: number;

        When(() => {
            actualResult = Numbers.roundDecimals(obj, 2);
        });

        const arr: Array<any> = [
            {desc: 'a entier', obj: 2, result: 2},
            {desc: 'number with 2 decimal', obj: 2.15, result: 2.15},
            {desc: 'number with other 2 decimal', obj: 2.14, result: 2.14},
            {desc: 'number with other 2 decimal', obj: 2.16, result: 2.16},
            {
                desc: 'number with 2 decimal plus epsilon error',
                obj: 2.15 + epsilonError,
                result: 2.15,
            },
            {
                desc: 'number with other 2 decimal plus epsilon error',
                obj: 2.14 + epsilonError,
                result: 2.14,
            },
            {
                desc: 'number with other 2 decimal plus epsilon error',
                obj: 2.16 + epsilonError,
                result: 2.16,
            },
            {
                desc: 'number with 2 decimal minus epsilon error',
                obj: 2.15 - epsilonError,
                result: 2.15,
            },
            {
                desc: 'number with other 2 decimal minus epsilon error',
                obj: 2.14 - epsilonError,
                result: 2.14,
            },
            {
                desc: 'number with other 2 decimal minus epsilon error',
                obj: 2.16 - epsilonError,
                result: 2.16,
            },
            {desc: 'number with 3 decimal', obj: 2.115, result: 2.12},
            {desc: 'number with other 3 decimal', obj: 2.114, result: 2.11},
            {desc: 'number with other 3 decimal', obj: 2.116, result: 2.12},
            {
                desc: 'number with 3 decimal plus epsilon error',
                obj: 2.115 + epsilonError,
                result: 2.12,
            },
            {
                desc: 'number with other 3 decimal plus epsilon error',
                obj: 2.114 + epsilonError,
                result: 2.11,
            },
            {
                desc: 'number with other 3 decimal plus epsilon error',
                obj: 2.116 + epsilonError,
                result: 2.12,
            },
            {
                desc: 'number with 3 decimal minus epsilon error',
                obj: 2.115 - epsilonError,
                result: 2.12,
            },
            {
                desc: 'number with other 3 decimal minus epsilon error',
                obj: 2.114 - epsilonError,
                result: 2.11,
            },
            {
                desc: 'number with other 3 decimal minus epsilon error',
                obj: 2.116 - epsilonError,
                result: 2.12,
            },
            {desc: 'zero', obj: 0, result: 0},
            {desc: 'zero plus epsilon error', obj: 0 + epsilonError, result: 0},
            {
                desc: 'zero minus epsilon error',
                obj: 0 - epsilonError,
                result: 0,
            },
            {desc: 'a minus entier', obj: -2, result: -2},
            {desc: 'minus number with 2 decimal', obj: -2.15, result: -2.15},
            {
                desc: 'minus number with other 2 decimal',
                obj: -2.14,
                result: -2.14,
            },
            {
                desc: 'minus number with other 2 decimal',
                obj: -2.16,
                result: -2.16,
            },
            {
                desc: 'minus number with 2 decimal plus epsilon error',
                obj: -2.15 + epsilonError,
                result: -2.15,
            },
            {
                desc: 'minus number with other 2 decimal plus epsilon error',
                obj: -2.14 + epsilonError,
                result: -2.14,
            },
            {
                desc: 'minus number with other 2 decimal plus epsilon error',
                obj: -2.16 + epsilonError,
                result: -2.16,
            },
            {
                desc: 'minus number with 2 decimal minus epsilon error',
                obj: -2.15 - epsilonError,
                result: -2.15,
            },
            {
                desc: 'minus number with other 2 decimal minus epsilon error',
                obj: -2.14 - epsilonError,
                result: -2.14,
            },
            {
                desc: 'minus number with other 2 decimal minus epsilon error',
                obj: -2.16 - epsilonError,
                result: -2.16,
            },
            {desc: 'minus number with 3 decimal', obj: -2.115, result: -2.11},
            {
                desc: 'minus number with other 3 decimal',
                obj: -2.114,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal',
                obj: -2.116,
                result: -2.12,
            },
            {
                desc: 'minus number with 3 decimal plus epsilon error',
                obj: -2.115 + epsilonError,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal plus epsilon error',
                obj: -2.114 + epsilonError,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal plus epsilon error',
                obj: -2.116 + epsilonError,
                result: -2.12,
            },
            {
                desc: 'minus number with 3 decimal minus epsilon error',
                obj: -2.115 - epsilonError,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal minus epsilon error',
                obj: -2.114 - epsilonError,
                result: -2.11,
            },
            {
                desc: 'minus number with other 3 decimal minus epsilon error',
                obj: -2.116 - epsilonError,
                result: -2.12,
            },
            {desc: 'minus zero', obj: -0, result: 0},
            {
                desc: 'minus zero plus epsilon error',
                obj: -0 + epsilonError,
                result: 0,
            },
            {
                desc: 'minus zero minus epsilon error',
                obj: -0 - epsilonError,
                result: 0,
            },
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if object is ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });
});
