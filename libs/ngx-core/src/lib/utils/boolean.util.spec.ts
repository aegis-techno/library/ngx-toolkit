import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {Booleans} from './boolean.util';

describe('Booleans tools', () => {
    let booleanValue: unknown;
    let actualResult: boolean;

    const noopFn = () => {
        return;
    };
    const emptyObject = {};

    describe('should return Boolean of arg if is true, 1 or "true"', () => {
        When(() => {
            actualResult = Booleans.coerceBoolean(booleanValue);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'false litteral', obj: 'false', result: false},
            {desc: 'string', obj: 'fal', result: false},
            {desc: 'number', obj: 2, result: false},
            {desc: 'object', obj: emptyObject, result: false},
            {desc: 'array', obj: [], result: false},
            {desc: 'array with item', obj: ['a'], result: false},
            {desc: 'function', obj: noopFn, result: false},
            {desc: 'true litteral', obj: 'true', result: true},
            {desc: '1 litteral', obj: '1', result: true},
            {desc: 'number 1', obj: 1, result: true},
            {desc: 'true', obj: true, result: true},
            {desc: 'false', obj: false, result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if object is ' + arr[i].desc, () => {
                Given(() => {
                    booleanValue = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toEqual(arr[i].result);
                });
            });
        }
    });
});
