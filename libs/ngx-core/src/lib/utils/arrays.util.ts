/**
 * Strings Tools File
 */
import {Optional} from '../models/optional.type';
import {Objects} from './objects.util';

/**
 * Arrays Tools File
 */
export class Arrays {
    /**
     * It checks if the array is null, undefined or empty.
     * @param array - Optional<Array<object>>
     * @returns A boolean value.
     */
    public static isEmpty(array: Optional<Array<unknown>>): array is null | undefined | [] {
        return Objects.isNotDefined(array) || array.length === 0;
    }

    /**
     * If the object is an array, return it, otherwise return an array containing the object.
     * @param {unknown} object - unknown
     * @returns An array of the object.
     */
    public static coerceArray<T = unknown>(object: T | Array<T>): Array<T> {
        return Objects.isArray(object) ? object : this.of(object);
    }

    /**
     * If the object is undefined, return an empty array, otherwise return an array containing the object.
     * @param {unknown} object - unknown
     * @returns An array with the object as the only element.
     */
    public static of<T = unknown>(object: T): Array<T> {
        return Objects.isUndefined(object) ? [] : [object];
    }
}
