import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {Objects} from './objects.util';

describe('Objects tools', () => {
    let obj: any;
    let actualResult: any;

    const noopFn = () => {
        return;
    };
    const emptyObject = {};
    describe('should find if object is null or undefined', () => {
        When(() => {
            actualResult = Objects.isNotDefined(obj);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: true},
            {desc: 'undefined', obj: undefined, result: true},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'string', obj: 'a', result: false},
            {desc: 'number', obj: 1, result: false},
            {desc: 'object', obj: emptyObject, result: false},
            {desc: 'array', obj: [], result: false},
            {desc: 'function', obj: noopFn, result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is not null and not undefined', () => {
        When(() => {
            actualResult = Objects.isDefined(obj);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: true},
            {desc: 'string', obj: 'a', result: true},
            {desc: 'number', obj: 1, result: true},
            {desc: 'object', obj: emptyObject, result: true},
            {desc: 'array', obj: [], result: true},
            {desc: 'function', obj: noopFn, result: true},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is null', () => {
        When(() => {
            actualResult = Objects.isNull(obj);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: true},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'string', obj: 'a', result: false},
            {desc: 'number', obj: 1, result: false},
            {desc: 'object', obj: emptyObject, result: false},
            {desc: 'array', obj: [], result: false},
            {desc: 'function', obj: noopFn, result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is not null', () => {
        When(() => {
            actualResult = Objects.isNotNull(obj);
        });
        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: true},
            {desc: 'empty string', obj: '', result: true},
            {desc: 'string', obj: 'a', result: true},
            {desc: 'number', obj: 1, result: true},
            {desc: 'object', obj: emptyObject, result: true},
            {desc: 'array', obj: [], result: true},
            {desc: 'function', obj: noopFn, result: true},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is undefined', () => {
        When(() => {
            actualResult = Objects.isUndefined(obj);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: true},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'string', obj: 'a', result: false},
            {desc: 'number', obj: 1, result: false},
            {desc: 'object', obj: emptyObject, result: false},
            {desc: 'array', obj: [], result: false},
            {desc: 'function', obj: noopFn, result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is not undefined', () => {
        When(() => {
            actualResult = Objects.isNotUndefined(obj);
        });
        const arr: Array<any> = [
            {desc: 'null', obj: null, result: true},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: true},
            {desc: 'string', obj: 'a', result: true},
            {desc: 'number', obj: 1, result: true},
            {desc: 'object', obj: emptyObject, result: true},
            {desc: 'array', obj: [], result: true},
            {desc: 'function', obj: noopFn, result: true},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is a string', () => {
        When(() => {
            actualResult = Objects.isString(obj);
        });

        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: true},
            {desc: 'string', obj: 'a', result: true},
            {desc: 'number', obj: 1, result: false},
            {desc: 'object', obj: emptyObject, result: false},
            {desc: 'array', obj: [], result: false},
            {desc: 'function', obj: noopFn, result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is a number', () => {
        When(() => {
            actualResult = Objects.isNumber(obj);
        });
        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'string', obj: 'a', result: false},
            {desc: 'number', obj: 1, result: true},
            {desc: 'object', obj: emptyObject, result: false},
            {desc: 'array', obj: [], result: false},
            {desc: 'function', obj: noopFn, result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is an object', () => {
        When(() => {
            actualResult = Objects.isObject(obj);
        });
        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'string', obj: 'a', result: false},
            {desc: 'number', obj: '1', result: false},
            {desc: 'object', obj: emptyObject, result: true},
            {desc: 'array', obj: [], result: false},
            {desc: 'function', obj: noopFn, result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is an array', () => {
        When(() => {
            actualResult = Objects.isArray(obj);
        });
        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'string', obj: 'a', result: false},
            {desc: 'number', obj: '1', result: false},
            {desc: 'object', obj: emptyObject, result: false},
            {desc: 'array', obj: [], result: true},
            {desc: 'function', obj: noopFn, result: false},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if object is a function', () => {
        When(() => {
            actualResult = Objects.isFunction(obj);
        });
        const arr: Array<any> = [
            {desc: 'null', obj: null, result: false},
            {desc: 'undefined', obj: undefined, result: false},
            {desc: 'empty string', obj: '', result: false},
            {desc: 'string', obj: 'a', result: false},
            {desc: 'number', obj: '1', result: false},
            {desc: 'object', obj: emptyObject, result: false},
            {desc: 'array', obj: [], result: false},
            {desc: 'function', obj: noopFn, result: true},
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if objects ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });

    describe('should find if two object is equals', () => {
        let secondObj: any;

        When(() => {
            actualResult = Objects.equals(obj, secondObj);
        });

        const emptyObj = {};
        const arr: Array<any> = [
            {desc: 'rObj undefined', lObj: [], rObj: undefined, result: false},
            {desc: 'lObj undefined', lObj: undefined, rObj: [], result: false},
            {
                desc: 'both undefined',
                lObj: undefined,
                rObj: undefined,
                result: true,
            },
            {desc: 'rObj null', lObj: [], rObj: null, result: false},
            {desc: 'lObj null', lObj: null, rObj: [], result: false},
            {desc: 'both null', lObj: null, rObj: null, result: true},
            {
                desc: 'lObj has more item',
                lObj: ['a', 'a'],
                rObj: ['a'],
                result: false,
            },
            {
                desc: 'rObj has more item',
                lObj: ['a'],
                rObj: ['a', 'a'],
                result: false,
            },
            {
                desc: 'array of different type',
                lObj: [1],
                rObj: ['1'],
                result: false,
            },
            {desc: 'array of string', lObj: ['a'], rObj: ['a'], result: true},
            {desc: 'array of number', lObj: [15], rObj: [15], result: true},
            {
                desc: 'array of obj',
                lObj: [emptyObj],
                rObj: [emptyObj],
                result: true,
            },
        ];

        for (let i = 0; i < arr.length; i++) {
            describe('even if ' + arr[i].desc, () => {
                Given(() => {
                    obj = arr[i].lObj;
                    secondObj = arr[i].rObj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i].result);
                });
            });
        }
    });
});
