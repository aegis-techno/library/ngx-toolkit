import {Optional} from '../models/optional.type';
import {Objects} from './objects.util';

/**
 * Strings Tools File
 */
export class Strings {
    /**
     * Returns true if the given value is undefined or an empty string.
     * @param value - Optional<string>
     * @returns A boolean value.
     */
    public static isEmpty(value: Optional<string>): boolean {
        return Objects.isNotDefined(value) || value === '';
    }

    /**
     * Returns true if the given string is not null or undefined and has a length greater than zero.
     * @param value - Optional<string>
     * @returns A boolean value.
     */
    public static isNotEmpty(value: Optional<string>): value is string {
        return !Strings.isEmpty(value);
    }

    /**
     * If the value is not defined or the value is a string that is empty after trimming, then return true.
     * @param value - Optional<string>
     * @returns A boolean value.
     */
    public static isBlank(value: Optional<string>): boolean {
        return Objects.isNotDefined(value) || value.trim() === '';
    }

    /**
     * Returns true if the given string is not null, undefined, or empty.
     * @param value - Optional<string>
     * @returns A boolean value.
     */
    public static isNotBlank(value: Optional<string>): value is string {
        return !Strings.isBlank(value);
    }

    /**
     * It takes a string, checks if it's blank, and if it's not, it returns the first character of the string in uppercase,
     * followed by the rest of the string.
     * @param {string} value - string - The string to capitalize.
     * @returns The first letter of the string is being capitalized and the rest of the string is being returned.
     */
    public static capitalizeFirstLetter(value: string): string {
        if (Strings.isBlank(value)) {
            return '';
        }
        return value.charAt(0).toUpperCase() + value.slice(1);
    }

    /**
     * It takes a string and replaces all instances of {%n} with the nth argument passed to the function.
     * @param {string} value - The string to be formatted.
     * @param args - Array<string>
     * @returns The value of the string with the arguments replaced.
     */
    public static format(value: string, ...args: Array<string>): string {
        if (Strings.isBlank(value)) {
            return '';
        }
        return value?.replace(/{%(\d+)}/g, (match: string, number: number) =>
            Objects.isDefined(number) && Objects.isNotUndefined(args[number]) ? args[number] ?? '' : match ?? ''
        );
    }
}
