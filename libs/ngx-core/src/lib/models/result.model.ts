import {Objects} from '../utils/objects.util';

export class Result<ItemType, ErrorType = string> {
    constructor(public data?: ItemType, public error?: ErrorType) {
        if (Objects.isDefined(this.error)) {
            this.data = undefined;
        }
    }

    static of<ItemType, ErrorType = string>(data: ItemType): Result<ItemType, ErrorType> {
        return new Result<ItemType, ErrorType>(data, undefined);
    }

    static empty(): Result<void> {
        return new Result<void>(void 0, undefined);
    }

    static error<ErrorType = string>(error: ErrorType): Result<any, ErrorType> {
        return new Result<undefined, ErrorType>(undefined, error);
    }
}
