export class Exception extends Error {
    code: string;
    detail: any;

    constructor(code: string, message?: any, detail?: any) {
        super(message);
        this.code = code;
        this.detail = detail;
    }
}
