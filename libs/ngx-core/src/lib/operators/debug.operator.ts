import {Observable, tap} from 'rxjs';
import {Objects} from '../utils/objects.util';

export function debug(...message: Array<any>) {
    return function (source: Observable<any>) {
        return source.pipe(
            tap((val) => {
                console.log(...message.map((value) => (Objects.isFunction(value) ? value() : value)), val);
            })
        );
    };
}
