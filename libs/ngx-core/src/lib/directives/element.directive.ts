import {Directive, ElementRef} from '@angular/core';

@Directive({
    selector: '[ngxElement]',
    exportAs: 'element',
    standalone: true,
})
export class ElementDirective {
    constructor(public elementRef: ElementRef) {}
}
