# NgxCore

This library contains many tools for dev with Angular :

-   BaseSingletonService
-   BaseComponent
-   UUIDService
-   EventBusService

And utils files

-   Objects
-   Strings
-   Arrays
-   Optional
-   Exception
