# ngx-resources

An Angular wrapper for make API call.

-   [Installation](#installation)

## Installation

Install via npm:

```shell
npm install @aegis-techno/ngx-resources --save
```

Install manually:

-   Add `@aegis-techno/ngx-resources` into `package.json`.
-   Run `npm install`.

## Usage

## Repository initialisation

---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.

With the command following :

```
ng new ngx-resources --package-manager=yarn --create-application=false --directory=. --strict --routing --style=scss
ng add @angular-eslint/schematics@next --skip-confirmation=true
ng generate library @aegis-techno/ngx-resources
```

## Contributing

---

### Code linting

Run `yarn run lint` to check the lint.

### Build

Run `yarn run build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `yarn run test` to execute the unit tests via jest.
