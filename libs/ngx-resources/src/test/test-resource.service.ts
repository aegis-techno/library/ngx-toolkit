import {Injectable} from '@angular/core';
import {ResourceAction} from '../lib/decorators/resource-action.decorator';
import {ResourceParam} from '../lib/decorators/resource-param.decorator';
import {ResourceMethod} from '../lib/models/resource.method';
import {Resource} from '../lib/services/resource';

@Injectable({
    providedIn: 'root',
})
@ResourceParam({
    path: '/test',
})
export class TestResourceService extends Resource {
    @ResourceAction()
    public findAll!: ResourceMethod<any>;

    @ResourceAction({
        method: 'post',
    })
    public create!: ResourceMethod<any>;

    @ResourceAction({
        suffix: '{id}',
    })
    public findOne!: ResourceMethod<any>;
}
