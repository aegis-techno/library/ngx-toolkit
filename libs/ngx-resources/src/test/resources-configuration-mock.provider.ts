import {ValueProvider} from '@angular/core';
import {ResourcesConfiguration} from '../lib/models/resource.configuration';
import {ResourcesConfigurationToken} from '../lib/models/resources-configuration-token';

const serverBaseUrl: string = window.location.origin;

export const PROVIDE_RESOURCES_CONFIGURATION_MOCK: ValueProvider = {
    provide: ResourcesConfigurationToken,
    useValue: {
        resource: {
            baseUrl: `${serverBaseUrl}/api`,
        },
    } as ResourcesConfiguration,
};
