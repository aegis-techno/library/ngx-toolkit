import {Injectable} from '@angular/core';
import {ResourceAction} from '../lib/decorators/resource-action.decorator';
import {ResourceParam} from '../lib/decorators/resource-param.decorator';
import {ResourceMethod} from '../lib/models/resource.method';
import {Resource} from '../lib/services/resource';

@Injectable({
    providedIn: 'root',
})
@ResourceParam({
    path: '/testB',
})
export class TestBResourceService extends Resource {
    @ResourceAction()
    public findAll!: ResourceMethod<any>;
}
