import {ResourceAction} from '../lib/decorators/resource-action.decorator';
import {ResourceMethod} from '../lib/models/resource.method';
import {Resource} from '../lib/services/resource';

export abstract class CrudResource<T, C> extends Resource {
    @ResourceAction()
    public findAll!: ResourceMethod<C>;

    @ResourceAction({suffix: '{id}'})
    public findOne!: ResourceMethod<T>;

    @ResourceAction({method: 'post'})
    public create!: ResourceMethod<T>;

    @ResourceAction({method: 'put', suffix: '{id}'})
    public update!: ResourceMethod<T>;

    @ResourceAction({method: 'delete', suffix: '{id}'})
    public delete!: ResourceMethod<void>;
}
