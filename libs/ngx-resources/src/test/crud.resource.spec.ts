import {HttpHeaders} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Injectable} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {configureTestBed, Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {Observable} from 'rxjs';
import {ResourceParam} from '../index';
import {HttpsOptions} from '../index';
import {CrudResource} from './crud.resource';
import {PROVIDE_RESOURCES_CONFIGURATION_MOCK} from './resources-configuration-mock.provider';

@Injectable()
@ResourceParam({
    path: '/test',
})
export class ResourceTestService extends CrudResource<any, Array<any>> {}

@Injectable()
@ResourceParam()
export class ResourceTestWithoutOptionsService extends CrudResource<any, Array<any>> {}

@Injectable()
export class ResourceTestWithoutDecoratorService extends CrudResource<any, Array<any>> {}

describe('CrudResourceService', () => {
    let service: ResourceTestService;
    let serviceWithoutOptions: ResourceTestWithoutOptionsService;
    let serviceWithoutDecorator: ResourceTestWithoutDecoratorService;
    let httpTestingController: HttpTestingController;

    configureTestBed({
        imports: [HttpClientTestingModule],
        providers: [
            PROVIDE_RESOURCES_CONFIGURATION_MOCK,
            ResourceTestService,
            ResourceTestWithoutOptionsService,
            ResourceTestWithoutDecoratorService,
        ],
    });

    Given(async () => {
        httpTestingController = TestBed.inject(HttpTestingController);
        service = TestBed.inject(ResourceTestService);
        serviceWithoutOptions = TestBed.inject(ResourceTestWithoutOptionsService);
        serviceWithoutDecorator = TestBed.inject(ResourceTestWithoutDecoratorService);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    Then('should be created', () => {
        expect(service).toBeDefined();
    });

    describe('should findAll', () => {
        let postObs: Observable<any>;
        let options: HttpsOptions | undefined;

        Given(() => {
            options = undefined;
        });

        When(() => {
            postObs = service.findAll(null, null, null, options);
        });

        Then((done: any) => {
            postObs.subscribe((result) => {
                expect(result).toBeDefined();
                done();
            });
            const req = httpTestingController.expectOne(`${PROVIDE_RESOURCES_CONFIGURATION_MOCK.useValue.resource.baseUrl}/test`);
            expect(req.request.method).toBe('GET');
            req.flush({content: [{title: 'Test1'}]});
        });

        describe('should pass options', () => {
            Given(() => {
                options = {
                    headers: new HttpHeaders({
                        'X-Test-Header': 'Test',
                    }),
                };
            });

            Then((done: any) => {
                postObs.subscribe((result) => {
                    expect(result).toBeDefined();
                    done();
                });
                const req = httpTestingController.expectOne(
                    `${PROVIDE_RESOURCES_CONFIGURATION_MOCK.useValue.resource.baseUrl}/test`
                );
                expect(req.request.method).toBe('GET');
                expect(req.request.headers.get('X-Test-Header')).toBe('Test');
                req.flush({content: [{title: 'Test1'}]});
            });
        });
    });

    describe('should find One', () => {
        let postObs: Observable<any>;

        When(() => {
            postObs = service.findOne(
                null,
                {testQuery: 'test'},
                {
                    id: '1',
                }
            );
        });

        Then((done: any) => {
            postObs.subscribe((result) => {
                expect(result).toBeDefined();
                done();
            });
            const req = httpTestingController.expectOne(
                `${PROVIDE_RESOURCES_CONFIGURATION_MOCK.useValue.resource.baseUrl}/test/1?testQuery=test`
            );
            expect(req.request.method).toBe('GET');
            req.flush({content: [{title: 'Test1'}]});
        });
    });

    describe('should find One with no options', () => {
        let postObs: Observable<any>;

        When(() => {
            postObs = serviceWithoutOptions.findOne(
                null,
                {testQuery: 'test'},
                {
                    id: '1',
                }
            );
        });

        Then((done: any) => {
            postObs.subscribe((result) => {
                expect(result).toBeDefined();
                done();
            });
            const req = httpTestingController.expectOne(
                `${PROVIDE_RESOURCES_CONFIGURATION_MOCK.useValue.resource.baseUrl}/1?testQuery=test`
            );
            expect(req.request.method).toBe('GET');
            req.flush({content: [{title: 'Test1'}]});
        });
    });

    describe('should find One with no decorator', () => {
        let postObs: Observable<any>;

        When(() => {
            postObs = serviceWithoutDecorator.findOne(
                null,
                {testQuery: 'test'},
                {
                    id: '1',
                }
            );
        });

        Then((done: any) => {
            postObs.subscribe((result) => {
                expect(result).toBeDefined();
                done();
            });
            const req = httpTestingController.expectOne(
                `${PROVIDE_RESOURCES_CONFIGURATION_MOCK.useValue.resource.baseUrl}/1?testQuery=test`
            );
            expect(req.request.method).toBe('GET');
            req.flush({content: [{title: 'Test1'}]});
        });
    });

    describe('should create', () => {
        let postObs: Observable<any>;

        When(() => {
            postObs = service.create();
        });

        Then((done: any) => {
            postObs.subscribe((result) => {
                expect(result).toBeDefined();
                done();
            });
            const req = httpTestingController.expectOne(`${PROVIDE_RESOURCES_CONFIGURATION_MOCK.useValue.resource.baseUrl}/test`);
            expect(req.request.method).toBe('POST');
            req.flush({content: [{title: 'Test1'}]});
        });
    });
});
