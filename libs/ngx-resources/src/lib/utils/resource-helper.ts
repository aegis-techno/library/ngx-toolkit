import {Objects, Optional, Strings} from '@aegis-techno/ngx-core';
import {ResourceActionConfig} from '../models/resource-action.config';

export class ResourceHelper {
    public static addLeadingSlash(path: Optional<string>): string {
        return ResourceHelper.handleSlash(path, {leading: true});
    }

    public static removeSlash(path: Optional<string>): string {
        return ResourceHelper.handleSlash(path, {
            trailing: false,
            leading: false,
        });
    }

    public static addTrailingSlash(path: Optional<string>): string {
        return ResourceHelper.handleSlash(path, {trailing: true});
    }

    public static replaceParams(url: Optional<string>, params: Optional<{[key: string]: string | number}>): string {
        if (Objects.isNotDefined(url)) {
            return '';
        }
        if (Objects.isNotDefined(params)) {
            return url;
        }

        const pathParams = url.match(/{([^}]+)}/g) ?? [];
        for (const pathParam of pathParams) {
            url = this.replaceParam(url, params, pathParam);
        }
        return url;
    }

    public static expandQuery(query: Optional<{[key: string]: string}>): string {
        if (Objects.isNotDefined(query)) {
            return '';
        }
        return Object.keys(query)
            .filter((key) => Objects.isDefined(query[key]))
            .map((key) => `${key}=${encodeURI(query[key]!)}`)
            .join('&');
    }

    public static buildUrlAction(apiRestBaseurl: string, actionOptions: ResourceActionConfig, query: any, params: any): string {
        const fullUrl = ResourceHelper.buildBaseUrl(apiRestBaseurl, actionOptions);
        let url = ResourceHelper.replaceParams(fullUrl, params);

        const queryStr = ResourceHelper.expandQuery(query);
        if (Strings.isNotBlank(queryStr)) {
            if (url.includes('?')) {
                url += `&${queryStr}`;
            } else {
                url += `?${queryStr}`;
            }
        }
        return url;
    }

    private static handleSlash(path: Optional<string>, options: {leading?: boolean; trailing?: boolean}): string {
        if (!Strings.isNotBlank(path)) {
            return '';
        }

        const hasLeadingSlash: boolean = path.startsWith('/');
        const hasTrailingSlash: boolean = path.endsWith('/');

        if (hasLeadingSlash && options.leading === false) {
            path = path.substring(1);
        }

        if (hasTrailingSlash && options.trailing === false) {
            path = path.substring(0, path.length - 1);
        }

        if (!hasLeadingSlash && options.leading === true) {
            path = `/${path}`;
        }

        if (!hasTrailingSlash && options.trailing === true) {
            path = `${path}/`;
        }

        return path;
    }

    private static replaceParam(url: string, params: {[p: string]: string | number}, pathParam: string): string {
        const pathKey = pathParam.substr(1, pathParam.length - 2);
        let paramValue: string | number = '';
        const value: any = params[pathKey];
        if (Objects.isDefined(value)) {
            paramValue = value;
        }
        return url.replace(pathParam, paramValue.toString());
    }

    private static buildBaseUrl(baseUrl: string, actionOptions: ResourceActionConfig): string {
        const url = ResourceHelper.removeSlash(baseUrl);
        const version = ResourceHelper.addLeadingSlash(actionOptions.version);
        const path = ResourceHelper.addLeadingSlash(actionOptions.path);
        const suffix = actionOptions.suffix?.startsWith('?')
            ? actionOptions.suffix
            : ResourceHelper.addLeadingSlash(actionOptions.suffix);

        return url + version + path + suffix;
    }
}
