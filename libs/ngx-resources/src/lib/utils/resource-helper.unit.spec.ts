import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {ResourceHelper} from './resource-helper';

describe('Resource Helper', () => {
    let obj: string | null | undefined;
    let actualResult: string;

    describe('when add leading Slash', () => {
        When(() => {
            actualResult = ResourceHelper.addLeadingSlash(obj);
        });

        const arr: Array<{desc: string; obj: any; result: string}> = [
            {
                desc: 'should return empty string if arg is null',
                obj: null,
                result: '',
            },
            {
                desc: 'should return empty string if arg is undefined',
                obj: undefined,
                result: '',
            },
            {
                desc: 'should return empty string if arg is empty string',
                obj: '',
                result: '',
            },
            {
                desc: 'should return arg with leading slash if arg is not blank string',
                obj: 'test',
                result: '/test',
            },
            {
                desc: 'should return arg with leading slash if arg is with trailing slash',
                obj: 'test/',
                result: '/test/',
            },
            {
                desc: 'should return arg if arg is with leading slash',
                obj: '/test',
                result: '/test',
            },
            {
                desc: 'should return arg if arg is with both slash',
                obj: '/test/',
                result: '/test/',
            },
            {
                desc: 'should return empty string if arg is blank string',
                obj: '   ',
                result: '',
            },
        ];

        for (let i = 0; i < arr.length; i++) {
            describe(arr[i]!.desc, () => {
                Given(() => {
                    obj = arr[i]!.obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i]!.result);
                });
            });
        }
    });

    describe('when add trailing Slash', () => {
        When(() => {
            actualResult = ResourceHelper.addTrailingSlash(obj);
        });

        const arr: Array<{desc: string; obj: any; result: string}> = [
            {
                desc: 'should return empty string if arg is null',
                obj: null,
                result: '',
            },
            {
                desc: 'should return empty string if arg is undefined',
                obj: undefined,
                result: '',
            },
            {
                desc: 'should return empty string if arg is empty string',
                obj: '',
                result: '',
            },
            {
                desc: 'should return arg with leading slash if arg is not blank string',
                obj: 'test',
                result: 'test/',
            },
            {
                desc: 'should return arg if arg is with trailing slash',
                obj: 'test/',
                result: 'test/',
            },
            {
                desc: 'should return arg if arg with leading slash is with leading slash',
                obj: '/test',
                result: '/test/',
            },
            {
                desc: 'should return arg if arg is with both slash',
                obj: '/test/',
                result: '/test/',
            },
            {
                desc: 'should return empty string if arg is blank string',
                obj: '   ',
                result: '',
            },
        ];

        for (let i = 0; i < arr.length; i++) {
            describe(arr[i]!.desc, () => {
                Given(() => {
                    obj = arr[i]!.obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i]!.result);
                });
            });
        }
    });

    describe('when remove Slash', () => {
        When(() => {
            actualResult = ResourceHelper.removeSlash(obj);
        });

        const arr: Array<{desc: string; obj: any; result: string}> = [
            {
                desc: 'should return empty string if arg is null',
                obj: null,
                result: '',
            },
            {
                desc: 'should return empty string if arg is undefined',
                obj: undefined,
                result: '',
            },
            {
                desc: 'should return empty string if arg is empty string',
                obj: '',
                result: '',
            },
            {
                desc: 'should return arg if arg is not blank string',
                obj: 'test',
                result: 'test',
            },
            {
                desc: 'should return arg without leading and trailing slash if arg is with trailing slash',
                obj: 'test/',
                result: 'test',
            },
            {
                desc: 'should return arg  without leading and trailing slash if arg with leading slash is with leading slash',
                obj: '/test',
                result: 'test',
            },
            {
                desc: 'should return arg  without leading and trailing slash if arg is with both slash',
                obj: '/test/',
                result: 'test',
            },
            {
                desc: 'should return empty string if arg is blank string',
                obj: '   ',
                result: '',
            },
        ];

        for (let i = 0; i < arr.length; i++) {
            describe(arr[i]!.desc, () => {
                Given(() => {
                    obj = arr[i]!.obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i]!.result);
                });
            });
        }
    });

    describe('when replace Param', () => {
        let param: any = undefined;

        When(() => {
            actualResult = ResourceHelper.replaceParams(obj, param);
        });

        const arr: Array<{desc: string; obj: any; param: any; result: string}> = [
            {
                desc: 'should return empty string if string is null',
                obj: null,
                param: null,
                result: '',
            },
            {
                desc: 'should return empty string if string is undefined',
                obj: undefined,
                param: null,
                result: '',
            },
            {
                desc: 'should return string if param is null',
                obj: 'test',
                param: null,
                result: 'test',
            },
            {
                desc: 'should return string if param is undefined',
                obj: 'test',
                param: undefined,
                result: 'test',
            },
            {desc: 'with no param', obj: 'test', param: {}, result: 'test'},
            {
                desc: 'should return string if param is not defined',
                obj: 'test{id}',
                param: {},
                result: 'test',
            },
            {
                desc: 'should return string if param is null',
                obj: 'test{id}',
                param: {id: null},
                result: 'test',
            },
            {
                desc: 'should return string if param is empty',
                obj: 'test{id}',
                param: {id: ''},
                result: 'test',
            },
            {
                desc: 'should return string with params if param is a number',
                obj: 'test{id}',
                param: {id: 5},
                result: 'test5',
            },
            {
                desc: 'should return string with params if param is a string',
                obj: 'test{id}',
                param: {id: '5'},
                result: 'test5',
            },
        ];

        for (let i = 0; i < arr.length; i++) {
            describe(arr[i]!.desc, () => {
                Given(() => {
                    obj = arr[i]!.obj;
                    param = arr[i]!.param;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i]!.result);
                });
            });
        }
    });

    describe('when expand query', () => {
        let obj: any = undefined;

        When(() => {
            actualResult = ResourceHelper.expandQuery(obj);
        });

        const arr: Array<{desc: string; obj: any; result: string}> = [
            {
                desc: 'should return empty string if arg is null',
                obj: null,
                result: '',
            },
            {
                desc: 'should return empty string if arg is undefined',
                obj: undefined,
                result: '',
            },
            {
                desc: 'should return empty string if arg is empty',
                obj: {},
                result: '',
            },
            {
                desc: 'should return empty string if arg is null param',
                obj: {id: null},
                result: '',
            },
            {
                desc: 'should return empty string if arg is undefined param',
                obj: {id: undefined},
                result: '',
            },
            {
                desc: 'should return queryParam if arg is string param',
                obj: {id: 's'},
                result: 'id=s',
            },
            {
                desc: 'should return queryParam if arg is number param',
                obj: {id: 5},
                result: 'id=5',
            },
            {
                desc: 'should return queryParam if arg is multiple param',
                obj: {id: 5, ida: 7, idb: 9},
                result: 'id=5&ida=7&idb=9',
            },
        ];

        for (let i = 0; i < arr.length; i++) {
            describe(arr[i]!.desc, () => {
                Given(() => {
                    obj = arr[i]!.obj;
                });
                Then(() => {
                    expect(actualResult).toBe(arr[i]!.result);
                });
            });
        }
    });
});
