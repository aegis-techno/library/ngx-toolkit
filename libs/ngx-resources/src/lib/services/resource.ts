import {HttpClient} from '@angular/common/http';
import {inject} from '@angular/core';
import {Objects} from '@aegis-techno/ngx-core';
import {ResourceActionConfig} from '../models/resource-action.config';
import {ResourceConfig} from '../models/resource.config';
import {ResourcesConfiguration} from '../models/resource.configuration';
import {HttpsOptions} from '../models/resource.method';
import {ResourcesConfigurationToken} from '../models/resources-configuration-token';
import {ResourceHelper} from '../utils/resource-helper';

export abstract class Resource {
    protected readonly http: HttpClient = inject(HttpClient);
    protected readonly config: ResourcesConfiguration = inject(ResourcesConfigurationToken);

    public getResourceOptions(): ResourceConfig {
        return {};
    }

    public restAction(actionConfig: {
        query: any;
        body: any;
        params: any;
        options: HttpsOptions;
        actionOptions: ResourceActionConfig;
    }): any {
        const url = ResourceHelper.buildUrlAction(
            this.config.resource.baseUrl,
            actionConfig.actionOptions,
            actionConfig.query,
            actionConfig.params
        );

        const options: any = {
            body: actionConfig.body,
            ...actionConfig.actionOptions.options,
        };

        if (Objects.isDefined(actionConfig.options)) {
            options.observe = actionConfig.options.observe;
            options.headers = actionConfig.options.headers;
            options.params = actionConfig.options.params;
            options.context = actionConfig.options.context;
            options.responseType = actionConfig.options.responseType;
            options.reportProgress = actionConfig.options.reportProgress;
            options.withCredentials = actionConfig.options.withCredentials;
        }

        return this.http.request(actionConfig.actionOptions.method!, url, options);
    }
}
