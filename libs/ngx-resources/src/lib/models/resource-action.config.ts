import {ResourceConfig} from './resource.config';
import {HttpsOptions} from './resource.method';

export interface ResourceActionConfig extends ResourceConfig {
    method?: 'get' | 'post' | 'put' | 'delete';
    suffix?: string;
    options?: Partial<HttpsOptions>;
}
