import {HttpContext, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

export type HttpsOptions = {
    observe?: string;
    headers?:
        | HttpHeaders
        | {
              [header: string]: string | string[];
          };
    context?: HttpContext;
    params?:
        | HttpParams
        | {
              [param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>;
          };
    reportProgress?: boolean;
    responseType?: string;
    withCredentials?: boolean;
};

export interface ResourceMethodBase<B, Q, P, R> {
    (body?: B, query?: Q, params?: P, options?: HttpsOptions): Observable<R>;
}

export type ResourceMethod<R> = ResourceMethodBase<any, any, any, R>;

export type ResourceMethodWithBody<B, R> = ResourceMethodBase<B, any, any, R>;
