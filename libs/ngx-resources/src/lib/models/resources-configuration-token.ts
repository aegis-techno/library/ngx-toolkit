import {InjectionToken} from '@angular/core';
import {ResourcesConfiguration} from './resource.configuration';

export const ResourcesConfigurationToken: InjectionToken<ResourcesConfiguration> = new InjectionToken<ResourcesConfiguration>(
    'ResourcesConfigurationToken'
);
