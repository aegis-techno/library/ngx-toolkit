export interface ResourceConfig {
    path?: string;
    version?: string;
}
