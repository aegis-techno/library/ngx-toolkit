import {ResourceConfig} from '../models/resource.config';

export function ResourceParam(config: ResourceConfig = {}) {
    const resourceConfig = Object.assign(
        {
            path: '',
            version: '',
        } as ResourceConfig,
        config
    );

    return (target: any) => {
        target.prototype.getResourceOptions = () => resourceConfig;
    };
}
