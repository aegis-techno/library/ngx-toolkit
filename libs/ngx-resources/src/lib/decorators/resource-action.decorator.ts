import {ResourceActionConfig} from '../models/resource-action.config';

export function ResourceAction(config: ResourceActionConfig = {}) {
    return function (target: any, propertyKey: string) {
        target[propertyKey] = function (...args: Array<any>): any {
            const [body, query, params, options] = args;
            const actionOptions: ResourceActionConfig = Object.assign(
                {
                    method: 'get',
                    suffix: '',
                } as ResourceActionConfig,
                this.getResourceOptions(),
                config
            );
            return this.restAction({
                body,
                query,
                params,
                options,
                actionOptions,
            });
        };
    };
}
