import {InjectionToken} from '@angular/core';
import {ResourcesConfiguration} from '../models/resources.configuration';

export const ResourcesConfigurationToken: InjectionToken<ResourcesConfiguration> = new InjectionToken<ResourcesConfiguration>(
    'ResourcesConfigurationToken'
);
