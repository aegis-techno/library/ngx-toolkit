export * from './lib/decorators/resource-action.decorator';
export * from './lib/decorators/resource-param.decorator';

export * from './lib/services/resource';

export * from './lib/models/resource.method';
export * from './lib/models/resource.configuration';
export * from './lib/models/resources-configuration-token';

export * from './test/resources-configuration-mock.provider';
