# ngx-entity-manager

An Angular entity manage.

-   [Installation](#installation)
-   [Usage](#usage)
    -   [Serialization](#serialization)
-   [API](#api)

## Installation

Install via npm:

```
npm install @aegis-techno/ngx-entity-manager --save
```

Install manually:

-   Add `@aegis-techno/ngx-entity-manager` into `package.json`.
-   Run `npm install`.

## Usage

```ts
import {NgxEntity, NgxAbstractEntity, NgxRelation} from '@aegis-techno/ngx-entity-manager';
import {forwardRef} from '@angular/core';

@NgxEntity({
    primaryKeys: ['id'],
})
export class MyObject extends NgxAbstractEntity {
    public id: string = null;
    public name: string = null;

    @NgxRelation(forwardRef(() => MyObjectChild))
    public oneChild: MyObjectChild = null;

    @NgxRelation(forwardRef(() => MyObjectChild), true)
    public arrayChild: Array<MyObjectChild2> = null;

    @NgxRelation(forwardRef(() => LegacyObject), true)
    public arrayLegacyChild: Array<LegacyObject> = null;
}

@NgxEntity({
    primaryKeys: ['id'],
})
export class MyObjectChild extends NgxAbstractEntity {
    public name = null;
}

export class MyObjectChild2 extends MyObjectChild {
    public name2 = null;
}

export class LegacyObject {
    public name = null;
}
```

## API

### NgxAbstractEntity

#### Methods

-   `fromObject(object: any): NgxAbstractEntity`: Return instance of object and save it.
-   `findById(id: any): NgxAbstractEntity`: Return instance of object if a saved item has this id.

-   `toJson(deep?: boolean): any`: Return a object with date of instance.

##### Example

```ts
const myObject = MyObject.fromObject({
    id: '1',
    name: 'myObjectName',
    oneChild: {
        id: '1',
        name: 'myObjectChildName',
    },
    arrayChild: [
        {
            id: '2',
            name: 'myObjectChildName',
            name2: 'myObjectChild2Name',
        },
        {
            id: '3',
            name: 'myObjectChildName',
            name2: 'myObjectChild2Name',
        },
    ],
    arrayLegacyChild: [
        {
            name: 'myLegacyObject',
        },
    ],
});

const myObjectChild = MyObjectChild.findById<MyObjectChild>('1');
const myObjectChild2 = MyObjectChild2.findById<MyObjectChild2>('2');
```

## Repository initialisation

---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.

With the command following :

```
ng new ngx-entity-manager --package-manager=yarn --create-application=false --directory=. --strict --routing --style=scss
ng add @angular-eslint/schematics@next --skip-confirmation=true
ng generate library @aegis-techno/ngx-entity-manager
```

## Contributing

---

### Code linting

Run `yarn run lint` to check the lint.

### Build

Run `yarn run build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `yarn run test` to execute the unit tests via jest.
