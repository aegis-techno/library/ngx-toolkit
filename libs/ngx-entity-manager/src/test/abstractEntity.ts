import {Entity} from '../lib/decorators/entity.decorator';
import {Relation} from '../lib/decorators/relation.decorator';
import {AbstractEntity} from '../lib/models/abstract-entity.model';
import {RegisteredEntity} from '../lib/models/registered-entity.model';

@Entity()
export class CustomAbstractEntity extends AbstractEntity {
    public name: string | null = null;
    public other: string | null = null;

    @Relation(() => OtherAbstractEntity)
    public otherItem: OtherAbstractEntity | null = null;

    @Relation(() => OtherAbstractEntity, true)
    public otherItems: OtherAbstractEntity[] = [];

    @Relation(() => LegacyEntity, true)
    public legacyItems: LegacyEntity[] = [];

    @Relation(() => LegacyEntity)
    public legacyItem: LegacyEntity | null = null;

    public legacyItemUnrelated: LegacyEntity | null = null;
}

@Entity()
export class OtherAbstractEntity extends CustomAbstractEntity {
    public otherName: string | null = null;
}

export class LegacyEntity {
    public name: string | null = null;

    public toJSON() {
        return {name: 'Un name in JSON'};
    }
}

@Entity({
    primaryKeys: ['id'],
})
export class CustomRegisteredEntity extends RegisteredEntity {
    public id: number | null = null;
    public name: string | null = null;
    public other: string | null = null;

    @Relation(() => RegisteredEntityChildRegisteredEntity)
    public otherItem: RegisteredEntityChildRegisteredEntity | null = null;

    @Relation(() => CustomAbstractEntity)
    public abstractItem: CustomAbstractEntity | null = null;

    @Relation(() => RegisteredEntityChildRegisteredEntity, true)
    public otherItems: RegisteredEntityChildRegisteredEntity[] = [];
}

@Entity({
    primaryKeys: ['name'],
})
export class RegisteredEntityChildRegisteredEntity extends CustomRegisteredEntity {}

export class RegisteredEntityChild extends CustomRegisteredEntity {}

export class BadRegisteredEntity extends RegisteredEntity {
    public name: string | null = null;
    public other: string | null = null;
}
