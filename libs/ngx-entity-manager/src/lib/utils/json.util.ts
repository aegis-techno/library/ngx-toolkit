import {Objects} from '@aegis-techno/ngx-core';

export class JsonUtils {
    static toJsonPrimitif(
        obj: any extends Array<any> ? never : object & {toJSON: (deepLevel: number) => object},
        deepLevel: number
    ) {
        if (Objects.isDefined(obj) && Objects.isFunction(obj['toJSON'])) {
            return obj.toJSON(deepLevel);
        }
        return obj;
    }

    static toJson(obj: any, deepLevel: number) {
        if (Objects.isArray(obj)) {
            return obj.map((subItem: any) => {
                return JsonUtils.toJsonPrimitif(subItem, deepLevel);
            });
        } else {
            return JsonUtils.toJsonPrimitif(obj, deepLevel);
        }
    }
}
