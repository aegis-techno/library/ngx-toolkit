import {Exception} from '@aegis-techno/ngx-core';
import {Type} from '@angular/core';
import 'reflect-metadata';
import {ReflectType} from './reflect-type';

export class ReflectContext {
    /**
     * A map of Type objects for a given concrete type.
     * @hidden
     */
    private _types: WeakMap<any, ReflectType> = new WeakMap();

    /**
     * Gets type metadata for the specified concrete type.
     * @param type The concrete type.
     */
    getType(type: Type<any>): ReflectType {
        if (type === undefined) {
            throw new Exception('reflect.not.type', 'not good type');
        }

        if (typeof type !== 'function') {
            throw new Exception('reflect.not.type', 'not good type');
        }

        let cachedReflectType = this._types.get(type);
        if (!cachedReflectType) {
            cachedReflectType = new ReflectType(type);
            this._types.set(type, cachedReflectType);
        }
        return cachedReflectType;
    }
}
