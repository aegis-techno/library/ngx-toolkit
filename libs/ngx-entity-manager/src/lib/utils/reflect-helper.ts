import {Type} from '@angular/core';
import 'reflect-metadata';

export function makeDecorator(annotationCtr: Type<any>) {
    return function DecoratorFactory(...args: any[]) {
        const annotationInstance = new annotationCtr(...args);

        return function Decorator(target: object, propertyName?: string, parameterIndex?: number): void {
            // check for parameter annotation
            if (typeof parameterIndex === 'number') {
                const parameters: any[][] = Reflect.getMetadata('parameters', target, propertyName!) ?? [];

                // there might be gaps if some in between parameters do not have annotations.
                // we pad with nulls.
                while (parameters.length <= parameterIndex) {
                    parameters.push(null!);
                }

                parameters[parameterIndex] = parameters[parameterIndex] ?? [];
                parameters[parameterIndex]!.push(annotationInstance);

                Reflect.defineMetadata('parameters', parameters, target, propertyName!);
            }
            // check for property/method annotation
            else if (propertyName) {
                const properties = Reflect.getOwnMetadata('propMetadata', target.constructor) ?? {};
                properties[propertyName] = properties[propertyName] ?? [];
                properties[propertyName].push(annotationInstance);
                Reflect.defineMetadata('propMetadata', properties, target.constructor);
            }
            // otherwise, we have a class annotation
            else {
                const annotations = Reflect.getOwnMetadata('annotations', target) ?? [];
                annotations.push(annotationInstance);
                Reflect.defineMetadata('annotations', annotations, target);
            }
        };
    };
}
