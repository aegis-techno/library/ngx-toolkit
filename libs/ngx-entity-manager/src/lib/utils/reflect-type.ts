import {Type} from '@angular/core';
import 'reflect-metadata';

export class ReflectType {
    constructor(private readonly type: Type<any>) {}

    getBaseType(): Type<any> {
        const basePrototype = this.type && this.type.prototype && Object.getPrototypeOf(this.type.prototype);
        return basePrototype && basePrototype.constructor;
    }

    getPropertyNames(): string[] {
        const properties = Reflect.getOwnMetadata('propMetadata', this.type);
        if (properties) {
            return Object.getOwnPropertyNames(properties).filter(
                (p) => isAccessor(this.type.prototype, p) || typeof this.type.prototype[p] !== 'function'
            );
        }

        return [];
    }

    getAllClassAnnotations(): Type<any>[] {
        return Reflect.getOwnMetadata('annotations', this.type) ?? [];
    }

    getAllPropertyAnnotations(propertyName: string): Type<any>[] {
        return Reflect.getOwnMetadata('propMetadata', this.type)[propertyName] ?? [];
    }

    getClassAnnotations(annotationCtr: Type<any>): any[] {
        return this.getAllClassAnnotations().filter((annotation) => annotation instanceof <any>annotationCtr);
    }

    getPropertyAnnotations(propertyName: string, annotationCtr: Type<any>): Type<any>[] {
        return this.getAllPropertyAnnotations(propertyName).filter((annotation) => annotation instanceof <any>annotationCtr);
    }

    hasClassAnnotation(annotationCtr: Type<any>): boolean {
        return this.getClassAnnotations(annotationCtr).length > 0;
    }

    hasPropertyAnnotations(propertyName: string, annotationCtr: Type<any>): boolean {
        return this.getPropertyAnnotations(propertyName, annotationCtr).length > 0;
    }
}

function isAccessor(obj: any, p: string): boolean {
    const d = Object.getOwnPropertyDescriptor(obj, p);
    return !!(d && (d.get || d.set));
}
