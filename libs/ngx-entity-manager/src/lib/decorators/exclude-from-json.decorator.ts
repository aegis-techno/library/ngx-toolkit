import {makeDecorator} from '../utils/reflect-helper';

export class ExcludeFromJsonMetadata {
    public constructor(public state: boolean) {}
}

export function ExcludeFromJSON(state = true): any {
    return (target: object, propertyName: string, parameterIndex?: number): any => {
        return makeDecorator(ExcludeFromJsonMetadata)(state)(target, propertyName, parameterIndex);
    };
}
