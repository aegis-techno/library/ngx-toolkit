import {Objects} from '@aegis-techno/ngx-core';
import {EntityDecoratorOptions} from '../models/entity-decorator-option.model';
import {makeDecorator} from '../utils/reflect-helper';

export class EntityMetadata {
    public options: EntityDecoratorOptions = {
        primaryKeys: [],
    };

    public constructor(options?: EntityDecoratorOptions) {
        if (Objects.isDefined(options)) {
            Object.assign(this.options, options);
        }
    }
}

export function Entity(options?: EntityDecoratorOptions): any {
    return (target: object, propertyName: string, parameterIndex?: number): any => {
        return makeDecorator(EntityMetadata)(options)(target, propertyName, parameterIndex);
    };
}
