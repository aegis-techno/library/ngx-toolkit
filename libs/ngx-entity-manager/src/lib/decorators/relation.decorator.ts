import {Exception, Objects} from '@aegis-techno/ngx-core';
import {AbstractEntity} from '../models/abstract-entity.model';
import {makeDecorator} from '../utils/reflect-helper';

export class RelationMetadata {
    public constructor(public related: (() => typeof AbstractEntity) | typeof AbstractEntity, public isArray: boolean = false) {}

    public static classify(val: any, annotation: any): any {
        if (Objects.isArray(val)) {
            return this.classifyArray(val, annotation);
        }
        return this.classifyObject(val, annotation);
    }

    private static classifyObject(object: any, annotation: any): any {
        if (!annotation) {
            return object;
        }

        const factory = annotation.related;
        if (Objects.isNotDefined(factory)) {
            throw new Exception('annotation.related.must.exist', 'annotation need related info');
        }
        let type: typeof AbstractEntity;
        if (Objects.isFunction(factory)) {
            type = factory();
        } else {
            type = factory;
        }

        if (object instanceof type) {
            return (object.updateFromObject && object.updateFromObject(object)) || Object.assign(object, object);
        } else {
            return (type.fromObject && type.fromObject(object)) || Object.assign(new type(), object);
        }
    }

    private static classifyArray(array: any[], annotation: any): any[] {
        if (!annotation) {
            return array;
        }

        const collection: any[] = [];
        array.forEach((value) => {
            collection.push(this.classifyObject(value, annotation));
        });
        return collection;
    }
}

export function Relation(related: typeof AbstractEntity | (() => typeof AbstractEntity) | object, isArray = false) {
    return (target: object, propertyName: string, parameterIndex?: number) => {
        return makeDecorator(RelationMetadata)(related, isArray)(target, propertyName, parameterIndex);
    };
}
