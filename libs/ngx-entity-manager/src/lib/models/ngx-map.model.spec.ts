import {waitForAsync} from '@angular/core/testing';
import {NgxMap} from './ngx-map.model';

describe('NgxMap', () => {
    let map: NgxMap<string, string>;

    beforeEach(waitForAsync(() => {
        map = new NgxMap();
    }));

    it('should be created', () => {
        expect(map).toBeDefined();
    });

    it('should get existing item', () => {
        map.set('a', 'a');
        expect(map.getOrThrow('a')).toBeDefined();
    });

    it('should throw if no existing item', () => {
        try {
            map.getOrThrow('a');
            fail();
        } catch (e) {
            expect(e).toBeDefined();
        }
    });
});
