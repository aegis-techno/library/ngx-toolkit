import {CustomAbstractEntity, LegacyEntity, OtherAbstractEntity} from '../../test/abstractEntity';

describe('NgxAbstractEntity', () => {
    let item: CustomAbstractEntity;
    let name: string;

    beforeEach(() => {
        name = 'myObjectName';
        item = CustomAbstractEntity.fromObject({
            name: name,
            otherItem: OtherAbstractEntity.fromObject({
                name: name,
            }),
            otherItems: [
                {
                    name: name,
                    otherName: 'other',
                },
            ],
            legacyItems: [
                {
                    name: name,
                },
                {
                    name: name,
                },
            ],
            legacyItem: {
                name: name,
            },
            legacyItemUnrelated: {
                name: name,
            },
        });
    });

    it('should be created from Object', () => {
        expect(item).toBeDefined();
    });

    it('should be created with good type', () => {
        expect(item.constructor).toBe(CustomAbstractEntity);
    });

    it('should be created with attribute', () => {
        expect(item.name).toBe(name);
    });

    it('should be created with relation good type', () => {
        expect(item.otherItem).toBeDefined();
        expect(item.otherItem?.constructor).toBe(OtherAbstractEntity);
    });

    it('should be created with relation good type in array', () => {
        expect(item.otherItems[0]?.constructor).toBe(OtherAbstractEntity);
    });

    it('should be created with legacy item with good type', () => {
        expect(item.legacyItems[0]!.constructor).toBe(LegacyEntity);
        expect(item.legacyItems[0]!.name).toBe(name);
        expect(item.legacyItem!.constructor).toBe(LegacyEntity);
        expect(item.legacyItem!.name).toBe(name);
        expect(item.legacyItemUnrelated!.constructor).not.toBe(LegacyEntity);
        expect(item.legacyItemUnrelated!.name).toBe(name);
    });

    it('should be updateable', () => {
        expect(item.other).toBe(null);
        item.updateFromObject({
            other: name,
        });
        expect(item.other).toBe(name);
    });

    it('should be clonable', () => {
        const clone = item.clone();
        clone.name = '';
        expect(item.name).toBe(name);
    });

    it('should be jsonable', () => {
        const jsonObject = item.toJSON();
        expect(jsonObject.constructor).not.toEqual(CustomAbstractEntity);
        expect(jsonObject['name']).toBe(name);
        expect(jsonObject['otherItems'].length).toBe(1);
        expect(jsonObject['otherItems'][0].otherName).toBe(undefined);
        expect(jsonObject['otherItems'][0].name).toBe(undefined);
        expect(jsonObject['legacyItemUnrelated'].name).toBe(name);
        expect(jsonObject['legacyItem'].name).toBe('Un name in JSON');
        expect(jsonObject['legacyItems'][0].name).toBe('Un name in JSON');
        expect(jsonObject['legacyItems'].length).toBe(2);
    });

    it('should be jsonable in deep', () => {
        const jsonObject = item.toJSON(1);
        expect(jsonObject.constructor).not.toEqual(CustomAbstractEntity);
        expect(jsonObject['name']).toBe(name);
        expect(jsonObject['otherItem'].name).toBe(name);
        expect(jsonObject['otherItems'].length).toBe(1);
        expect(jsonObject['otherItems'][0].otherName).toBe('other');
        expect(jsonObject['otherItems'][0].name).toBe(name);
        expect(jsonObject['legacyItemUnrelated'].name).toBe(name);
        expect(jsonObject['legacyItem'].name).toBe('Un name in JSON');
        expect(jsonObject['legacyItems'][0].name).toBe('Un name in JSON');
        expect(jsonObject['legacyItems'].length).toBe(2);
    });

    it('should be stryngify', () => {
        const result = JSON.stringify(item);
        expect(result).toBeDefined();
        expect(result).toBe(
            '{"name":"myObjectName","other":null,"otherItem":{},"otherItems":[{}],"legacyItems":[{"name":"Un name in JSON"},{"name":"Un name in JSON"}],"legacyItem":{"name":"Un name in JSON"},"legacyItemUnrelated":{"name":"myObjectName"}}'
        );
    });
});
