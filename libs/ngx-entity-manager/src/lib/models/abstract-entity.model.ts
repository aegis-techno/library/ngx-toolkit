import {Type} from '@angular/core';
import {Exception, Indexable, Objects} from '@aegis-techno/ngx-core';
import {EntityMetadata} from '../decorators/entity.decorator';
import {ExcludeFromJSON, ExcludeFromJsonMetadata} from '../decorators/exclude-from-json.decorator';
import {RelationMetadata} from '../decorators/relation.decorator';
import {EntityAnnotationService} from '../services/entity-annotation.service';
import {ReflectionMetadataService} from '../services/reflection-metadata.service';
import {JsonUtils} from '../utils/json.util';

export class AbstractEntity implements Indexable {
    @ExcludeFromJSON()
    public metadata: any = {};

    @ExcludeFromJSON()
    private appliedRelationDecorators = false;
    @ExcludeFromJSON()
    private readonly preventRelationDecorators: boolean = false;

    public constructor(autoApplyDecorators = true, preventApplyRelationDecorators = false) {
        this.preventRelationDecorators = preventApplyRelationDecorators;

        if (autoApplyDecorators && !this.preventRelationDecorators) {
            setTimeout(() => {
                this._applyRelationDecorators();
            }, 1);
        }
    }

    public static fromObject<U extends AbstractEntity>(object: any = {}): U {
        if (Objects.isNotDefined(object)) {
            throw new Exception('object.must.exist', 'object is null or undefined');
        }

        const item = new this(false) as U;

        const entityAnnotationService = new EntityAnnotationService();
        const annotation = entityAnnotationService.getAnnotation<EntityMetadata>(EntityMetadata, this);
        if (Objects.isDefined(annotation)) {
            annotation.options.primaryKeys.forEach((primaryKey: string) => {
                if (object[primaryKey]) {
                    (item as Indexable)[primaryKey] = object[primaryKey];
                }
            });
        }

        item.updateFromObject(object);

        return item;
    }

    public updateFromObject(object: any): this {
        if (!this.preventRelationDecorators) {
            this._applyRelationDecorators();
        }
        if (Objects.isDefined(object)) {
            const reflectionMetadataService = new ReflectionMetadataService();
            const fields = reflectionMetadataService.from(this).getFields(true);

            fields.forEach((key: string) => {
                if (!Objects.isUndefined(object[key]) && !Objects.isFunction(object[key])) {
                    (this as Indexable)[key] = object[key];
                    if (!(object instanceof AbstractEntity)) {
                        delete object[key];
                    }
                }
            });

            if (!(object instanceof AbstractEntity)) {
                const leftKeys = Object.keys(object);

                if (leftKeys.length > 0) {
                    leftKeys.forEach((key) => {
                        this.metadata[key] = object[key];
                    });
                }
            }

            if (Object.prototype.hasOwnProperty.call(object, 'metadata')) {
                Object.keys(object.metadata).forEach((value) => {
                    this.metadata[value] = object.metadata[value];
                });
            }
        }

        return this as any;
    }

    public clone(): this {
        const entityAnnotationService = new EntityAnnotationService();
        const newInstance = new (this.constructor as Type<AbstractEntity>)();

        newInstance.updateFromObject(this);

        Object.keys(newInstance).forEach((key) => {
            const thisValue = (this as Indexable)[key];
            if (Array.isArray(thisValue)) {
                (newInstance as Indexable)[key] = thisValue.slice();
            } else if (
                !entityAnnotationService.hasAnnotation(RelationMetadata, this.constructor as Type<AbstractEntity>, key) &&
                Objects.isObject(thisValue)
            ) {
                (newInstance as Indexable)[key] = JSON.parse(JSON.stringify(thisValue));
            }
        });

        return newInstance as this;
    }

    public toJSON(deepLevel = 0): Indexable {
        const entityAnnotationService = new EntityAnnotationService();
        const json: Indexable = {};

        const reflectionMetadataService = new ReflectionMetadataService();
        reflectionMetadataService
            .from(this)
            .getFields(true)
            .filter((key) => Objects.isNotUndefined((this as Indexable)[key]) && !Objects.isFunction((this as Indexable)[key]))
            .filter((key) => {
                if (!entityAnnotationService.hasAnnotation(ExcludeFromJsonMetadata, this.constructor as any, key)) {
                    return true;
                }

                const annotation = entityAnnotationService.getAnnotation<ExcludeFromJsonMetadata>(
                    ExcludeFromJsonMetadata,
                    this.constructor as any,
                    key
                );
                if (Objects.isNotDefined(annotation)) {
                    throw new Error('annotation is null');
                }
                return !annotation.state;
            })
            .forEach((key) => {
                const constructor = this.constructor as Type<AbstractEntity>;

                if (
                    !Objects.isNull((this as Indexable)[key]) &&
                    entityAnnotationService.hasAnnotation(RelationMetadata, constructor, key)
                ) {
                    const annotation = entityAnnotationService.getAnnotation<RelationMetadata>(
                        RelationMetadata,
                        constructor,
                        key
                    );
                    if (Objects.isNotDefined(annotation)) {
                        throw new Error('annotation is null');
                    }
                    let type: any = annotation.related;
                    if (Objects.isFunction(annotation.related)) {
                        type = type();
                    }

                    const entityMetadatas = entityAnnotationService.getAnnotation<EntityMetadata>(EntityMetadata, type);

                    if (Objects.isDefined(entityMetadatas)) {
                        if (annotation.isArray) {
                            json[key] = this.toJsonEntityPropertyArray(key, entityMetadatas, deepLevel);
                        } else {
                            json[key] = this.toJsonEntityProperty(key, entityMetadatas, deepLevel);
                        }
                    } else {
                        json[key] = this.toJsonPrimitiveProperty(key, deepLevel);
                    }
                } else {
                    json[key] = this.toJsonPrimitiveProperty(key, deepLevel);
                }
            });

        return json;
    }

    private toJsonPrimitiveProperty(key: string, deepLevel: number) {
        return JsonUtils.toJson((this as Indexable)[key], deepLevel ? deepLevel - 1 : 0);
    }

    private toJsonEntityProperty(key: string, entityMetadatas: EntityMetadata, deepLevel: number) {
        if (deepLevel) {
            return JsonUtils.toJson((this as Indexable)[key], deepLevel ? deepLevel - 1 : 0);
        }
        const json: Indexable = {};
        entityMetadatas.options.primaryKeys.forEach((primaryKey: string) => {
            json[primaryKey] = (this as Indexable)[key][primaryKey];
        });
        return json;
    }

    private toJsonEntityPropertyArray<T extends AbstractEntity>(key: string, entityMetadatas: EntityMetadata, deepLevel: number) {
        return (this as Indexable)[key].map((_entity: T, index: number, arr: Array<T>) => {
            return this.toJsonEntityProperty.call(arr, '' + index, entityMetadatas, deepLevel);
            // if (deepLevel) {
            //     return JsonUtils.toJson(entity, deepLevel ? deepLevel - 1 : 0);
            // }
            // const related: Indexable = {};
            // entityMetadatas.options.primaryKeys.forEach((primaryKey: string) => {
            //     related[primaryKey] = (entity as Indexable)[primaryKey];
            // });
            // return related;
        });
    }

    private _applyRelationDecorators(): void {
        if (!this.appliedRelationDecorators) {
            this.appliedRelationDecorators = true;

            const reflectionMetadataService = new ReflectionMetadataService();
            const metadata = reflectionMetadataService.from(this);
            const decorableFields = metadata.getDecorableFields();
            decorableFields.forEach((metadata, key) => {
                let val: any = null;

                const propertyDescriptor = Object.getOwnPropertyDescriptor(this as any, key);

                if (Objects.isDefined(propertyDescriptor)) {
                    const oldSet = propertyDescriptor.set;
                    const oldGet = propertyDescriptor.get;
                    val =
                        propertyDescriptor.value ||
                        (Objects.isFunction(propertyDescriptor.get) && Objects.isDefined(propertyDescriptor.get)
                            ? propertyDescriptor.get()
                            : null);

                    const annotation = this._getAnnotationOf(metadata, key);

                    propertyDescriptor.set = (value: any) => {
                        val = value;
                        if (Objects.isDefined(val)) {
                            val = RelationMetadata.classify(val, annotation);

                            if (Objects.isFunction(oldSet) && Objects.isDefined(oldSet)) {
                                oldSet(val);
                            }
                        }
                    };

                    propertyDescriptor.get = () => {
                        if (Objects.isFunction(oldGet) && Objects.isDefined(oldGet)) {
                            return oldGet();
                        }
                        return val;
                    };

                    delete propertyDescriptor.value;
                    delete propertyDescriptor.writable;

                    Object.defineProperty(this, key, propertyDescriptor);
                } else {
                    console.warn('Warning you are trying to apply an Relation decorator on a non initialized property', this);
                }
            });
        }
    }

    private _getAnnotationOf(metadata: any, key: string) {
        const annotationService = new EntityAnnotationService();

        let annotation = null;

        const ctr = metadata ?? this.constructor;

        if (ctr instanceof RelationMetadata) {
            annotation = ctr;
        } else if (annotationService.hasAnnotation(RelationMetadata, ctr, key)) {
            annotation = annotationService.getAnnotation<RelationMetadata>(RelationMetadata, ctr, key);
        }
        return annotation;
    }
}
