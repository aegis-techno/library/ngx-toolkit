export class ReflectionMetadata {
    private fields: Set<string> = new Set();
    private decorableFields: Map<string, any> = new Map();

    public setFields(fields: Array<string> | Set<string>): void {
        if (fields instanceof Set) {
            this.fields = fields;
        } else {
            fields.forEach((value) => {
                this.fields.add(value);
            });
        }
    }

    public addField(field: string): void {
        this.fields.add(field);
    }

    public removeField(field: string): void {
        this.fields.delete(field);
    }

    public clearFields(): void {
        this.fields.clear();
    }

    public getFields(asArray: true): Array<string>;
    public getFields(asArray: false): Set<string>;
    public getFields(asArray = false): Set<string> | Array<string> {
        if (asArray) {
            return Array.from(this.fields.values());
        }
        return this.fields;
    }

    public setDecorableField(field: string, decorator: any): void {
        this.decorableFields.set(field, decorator);
    }

    public removeDecorableField(field: string): void {
        this.decorableFields.delete(field);
    }

    public clearDecorableFields(): void {
        this.decorableFields.clear();
    }

    public getDecorableFields(): Map<string, any> {
        return this.decorableFields;
    }
}
