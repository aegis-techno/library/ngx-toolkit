import {Exception, Objects} from '@aegis-techno/ngx-core';
import {EntityRegistryService} from '../services/entity-registry.service';
import {AbstractEntity} from './abstract-entity.model';

export class RegisteredEntity extends AbstractEntity {
    public static override fromObject<U extends AbstractEntity>(object: any): U {
        if (Objects.isNotDefined(object)) {
            throw new Exception('object.must.exist', 'object is null or undefined');
        }

        const registry = new EntityRegistryService();
        let item: any = registry.getEntityFromTypeAndId(this, registry.getId(this, object));

        if (Objects.isNotDefined(item)) {
            item = super.fromObject<U>(object);
            this._registerInRegistry<U>(item);
        } else {
            item.updateFromObject(object);
        }

        return item;
    }

    public static findById<U extends AbstractEntity>(id: any): U | null {
        const registry = new EntityRegistryService();
        return registry.getEntityFromTypeAndId<U>(this, id);
    }

    public static findAll<U extends AbstractEntity>(): Array<U> {
        const registry = new EntityRegistryService();
        return registry.getEntitiesFromTypeAndPredicate<U>(this);
    }

    public static findByPredicate<U extends AbstractEntity>(predicate: (entity: U) => boolean): Array<U> {
        const registry = new EntityRegistryService();
        return registry.getEntitiesFromTypeAndPredicate<U>(this, predicate);
    }

    private static _registerInRegistry<U>(item: U): void {
        const registry = new EntityRegistryService();
        registry.addInRegistry(item as any);
    }
}
