import {Exception, Objects} from '@aegis-techno/ngx-core';

/**
 * Map for throw and not found element
 */
export class NgxMap<K, V> extends Map<K, V> {
    private errorGet = "key don't exist in map";

    getOrThrow(key: K): V {
        const object = this.get(key);
        if (Objects.isUndefined(object)) {
            throw new Exception('object.must.exist', this.errorGet);
        }
        return object;
    }
}
