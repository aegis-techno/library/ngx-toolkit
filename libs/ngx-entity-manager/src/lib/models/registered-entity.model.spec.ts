import {
    BadRegisteredEntity,
    CustomAbstractEntity,
    CustomRegisteredEntity,
    RegisteredEntityChild,
    RegisteredEntityChildRegisteredEntity,
} from '../../test/abstractEntity';

describe('NgxRegisteredEntity', () => {
    let item: CustomRegisteredEntity;
    let name: string;
    let otherItemName: string;
    let abstractItemName: string;

    beforeEach(() => {
        name = 'myObjectName';
        otherItemName = 'myOtherObjectName';
        abstractItemName = 'myAbstractObjectName';
        item = CustomRegisteredEntity.fromObject({
            id: 1,
            name: name,
            otherItem: RegisteredEntityChildRegisteredEntity.fromObject({
                id: 1,
                name: otherItemName,
            }),
            abstractItem: CustomAbstractEntity.fromObject({
                id: 1,
                name: abstractItemName,
            }),
            otherItems: [
                {
                    name: otherItemName,
                    other: 'other',
                },
            ],
        });
    });

    it('should be created from Object', () => {
        expect(item).toBeDefined();
    });

    it('should be created with good type', () => {
        expect(item.constructor).toBe(CustomRegisteredEntity);
    });

    it('should be created with attribute', () => {
        expect(item.name).toBe(name);
    });

    it('should be created with relation good type', () => {
        expect(item.otherItem && item.otherItem.constructor).toBe(RegisteredEntityChildRegisteredEntity);
    });

    it('should be created with relation good type in array', () => {
        expect(item.otherItems[0]!.constructor).toBe(RegisteredEntityChildRegisteredEntity);
        expect(item.otherItems[0]!.name).toBe(otherItemName);
        expect(item.otherItems[0]!.id).toBe(1);
    });

    it('should be find', () => {
        const findAll = CustomRegisteredEntity.findAll();
        expect(findAll.length).toBe(1);
    });

    it('should be find by id', () => {
        const findedItem = CustomRegisteredEntity.findById<CustomRegisteredEntity>(item.id);
        expect(findedItem).toBe(item);
    });

    it('should be find by predicate', () => {
        const findedItem = CustomRegisteredEntity.findByPredicate<CustomRegisteredEntity>(
            (entity: CustomRegisteredEntity) => entity.name === name
        );
        expect(findedItem[0]).toBe(item);
    });

    it('should be updateable', () => {
        expect(item.other).toBe(null);
        item.updateFromObject({
            other: name,
        });
        expect(item.other).toBe(name);
    });

    it('should be clonable', () => {
        const clone = item.clone();
        clone.name = '';
        expect(item.name).toBe(name);
    });

    it('should be jsonable', () => {
        const jsonObject = item.toJSON();
        expect(jsonObject.constructor).not.toEqual(CustomRegisteredEntity);
        expect(jsonObject['name']).toBe(name);
        expect(jsonObject['otherItems'].length).toBe(1);
        expect(jsonObject['otherItems'][0].name).toBe(otherItemName);
        expect(jsonObject['otherItems'][0].otherItem).toBe(undefined);
    });

    it('should be jsonable in deep', () => {
        const jsonObject = item.toJSON(1);
        expect(jsonObject.constructor).not.toEqual(CustomAbstractEntity);
        expect(jsonObject['name']).toBe(name);
        expect(jsonObject['otherItems'].length).toBe(1);
        expect(jsonObject['otherItems'][0].name).toBe(otherItemName);
        expect(jsonObject['otherItems'][0].other).toBe('other');
    });

    it('should be find by primary keys config', () => {
        const findedItem = RegisteredEntityChildRegisteredEntity.findById<RegisteredEntityChildRegisteredEntity>(
            item.otherItem?.name
        );
        expect(findedItem).toBe(item.otherItem);
    });

    it('should be find by primary keys config', () => {
        const findedItem = RegisteredEntityChild.findById<RegisteredEntityChild>(item.id);
        expect(findedItem).toBe(item);
    });

    it('should throw error on NgxRegisteredEntity without annotation', () => {
        try {
            BadRegisteredEntity.fromObject({
                id: '1',
                name: name,
            });
            fail();
        } catch (err: any) {
            expect(err).toBeDefined();
        }
    });
});
