import {OtherAbstractEntity} from '../../test/abstractEntity';
import {ReflectionMetadataService} from './reflection-metadata.service';

describe('NgxReflectionMetadataService', () => {
    let service: ReflectionMetadataService;

    beforeAll(() => {
        service = new ReflectionMetadataService();
    });

    it('should be created', () => {
        expect(service).toBeDefined();
    });

    it('should find all fields', () => {
        const itemTest = new OtherAbstractEntity();
        const reflectionMetadata = service.from(itemTest);
        expect(reflectionMetadata.getFields(true).length).toBe(11);
    });

    it('should find all decorables fields', () => {
        const itemTest = new OtherAbstractEntity();
        const reflectionMetadata = service.from(itemTest);
        expect(reflectionMetadata.getDecorableFields().size).toBe(4);
    });
});
