import {Objects, Singleton} from '@aegis-techno/ngx-core';
import {Injectable, Type} from '@angular/core';
import {EntityMetadata} from '../decorators/entity.decorator';
import {AbstractEntity} from '../models/abstract-entity.model';
import {NgxMap} from '../models/ngx-map.model';
import {ReflectContext} from '../utils/reflect-context';
import {ReflectType} from '../utils/reflect-type';

type PropertyIdentifiant = string | number | symbol;

@Singleton()
@Injectable()
export class EntityAnnotationService {
    private annotationCache: NgxMap<Type<any>, NgxMap<Type<AbstractEntity>, NgxMap<PropertyIdentifiant, any>>> = new NgxMap();
    private baseTypeCache: NgxMap<Type<AbstractEntity>, Type<AbstractEntity>> = new NgxMap();

    private reflectContext = new ReflectContext();

    public getBaseType(type: Type<AbstractEntity>): Type<AbstractEntity> {
        if (this.baseTypeCache.has(type)) {
            return this.baseTypeCache.getOrThrow(type);
        }
        const annotationType: ReflectType = this.reflectContext.getType(type);

        let baseType: Type<AbstractEntity> = type;

        if (!this._isBaseType(annotationType)) {
            baseType = this.getBaseType(annotationType.getBaseType() as any);
        }

        this.baseTypeCache.set(type, baseType);

        return baseType;
    }

    public hasAnnotation(annotationClass: any, type: Type<AbstractEntity>, propertyName?: string): boolean {
        const annotation: EntityMetadata | null = this.getAnnotation<EntityMetadata>(annotationClass, type, propertyName);
        return Objects.isDefined(annotation);
    }

    public getAnnotation<T>(annotationClass: any, type: Type<AbstractEntity>, propertyName?: string): T | null {
        let annotation: T | null = this._getCachedAnnotation(annotationClass, type, propertyName ?? '');

        if (Objects.isDefined(annotation)) {
            return annotation;
        }

        annotation = this._resolveAnnotation<T>(annotationClass, this.reflectContext.getType(type), propertyName);

        this._putAnnotationInCache(annotationClass, type, propertyName ?? '', annotation);

        return annotation;
    }

    private _isBaseType(annotationType: ReflectType) {
        return (
            !annotationType.getBaseType() ||
            annotationType.hasClassAnnotation(EntityMetadata) ||
            annotationType.getBaseType().name === 'Object'
        );
    }

    private _putAnnotationInCache(
        annotationClass: any,
        type: Type<AbstractEntity>,
        propertyName: PropertyIdentifiant,
        annotation: any
    ) {
        if (!this.annotationCache.has(annotationClass)) {
            this.annotationCache.set(annotationClass, new NgxMap());
        }

        const annotationMap = this.annotationCache.getOrThrow(annotationClass);
        if (!annotationMap.has(type)) {
            annotationMap.set(type, new NgxMap());
        }

        const annotationTypeMap = annotationMap.getOrThrow(type);
        annotationTypeMap.set(propertyName, annotation);
    }

    private _resolveAnnotation<T>(annotationClass: any, type: ReflectType, propertyName: string | undefined): T | null {
        if (Objects.isDefined(propertyName)) {
            return this._resolveAnnotationByPropertyName<T>(annotationClass, type, propertyName);
        } else if (Objects.isDefined(type) && type.hasClassAnnotation(annotationClass)) {
            return type.getClassAnnotations(annotationClass).shift() || null;
        } else {
            const baseClass = type.getBaseType();
            if (baseClass) {
                return this.getAnnotation(annotationClass, baseClass, propertyName);
            }
        }
        return null;
    }

    private _resolveAnnotationByPropertyName<T>(annotationClass: any, type: ReflectType, propertyName: string): T | null {
        const exist: boolean = type.getPropertyNames().includes(propertyName as any);

        if (exist) {
            if (type.hasPropertyAnnotations(propertyName, annotationClass)) {
                return (type.getPropertyAnnotations(propertyName, annotationClass).shift() as any) || null;
            }
        } else if (Objects.isDefined(type.getBaseType())) {
            return this.getAnnotation(annotationClass, type.getBaseType() as any, propertyName);
        }
        return null;
    }

    private _getCachedAnnotation(
        annotationClass: any,
        type: Type<AbstractEntity>,
        propertyName: PropertyIdentifiant
    ): any | null {
        if (this.annotationCache.has(annotationClass)) {
            const annotationClassCached = this.annotationCache.get(annotationClass)!;
            if (annotationClassCached.has(type)) {
                const annotationTypeCached = annotationClassCached.get(type)!;
                if (annotationTypeCached.has(propertyName)) {
                    return annotationTypeCached.get(propertyName)!;
                }
            }
        }
        return null;
    }
}
