import {Indexable, Objects, Singleton} from '@aegis-techno/ngx-core';
import {Injectable, Type} from '@angular/core';
import {RelationMetadata} from '../decorators/relation.decorator';
import {AbstractEntity} from '../models/abstract-entity.model';
import {NgxMap} from '../models/ngx-map.model';
import {ReflectionMetadata} from '../models/reflection-metadata.model';
import {EntityAnnotationService} from './entity-annotation.service';

@Singleton()
@Injectable()
export class ReflectionMetadataService {
    private entityMetadatas: NgxMap<Type<AbstractEntity>, ReflectionMetadata> = new NgxMap();

    public from(instance: AbstractEntity): ReflectionMetadata {
        const constructor = instance.constructor as Type<AbstractEntity>;
        if (!this.entityMetadatas.has(constructor)) {
            this._putInCacheReflectionMetadata(instance, constructor);
        }

        return this.entityMetadatas.getOrThrow(constructor);
    }

    private _putInCacheReflectionMetadata(instance: AbstractEntity, constructor: Type<AbstractEntity>) {
        const reflectionMetadata = new ReflectionMetadata();
        reflectionMetadata.setFields(this._getPrototypeFields(instance));
        reflectionMetadata
            .getFields(true)
            .filter((field) => this._isDecorableField(instance, field, constructor))
            .forEach((field: string) => {
                reflectionMetadata.setDecorableField(
                    field,
                    new EntityAnnotationService().getAnnotation<RelationMetadata>(RelationMetadata, constructor, field)
                );
            });
        this.entityMetadatas.set(constructor, reflectionMetadata);
    }

    private _isDecorableField(instance: AbstractEntity, field: string, constructor: Type<AbstractEntity>) {
        return (
            !Objects.isFunction((instance as Indexable)[field]) &&
            new EntityAnnotationService().hasAnnotation(RelationMetadata, constructor, field)
        );
    }

    private _getPrototypeFields(proto: any): Array<string> {
        let keys = Object.keys(proto);
        if (Objects.isDefined(proto.__proto__)) {
            keys = keys.concat(this._getPrototypeFields(proto.__proto__));
        }

        return keys;
    }
}
