import {CustomRegisteredEntity, RegisteredEntityChild} from '../../test/abstractEntity';
import {EntityMetadata} from '../decorators/entity.decorator';
import {RelationMetadata} from '../decorators/relation.decorator';
import {EntityAnnotationService} from './entity-annotation.service';

describe('NgxEntityAnnotationService', () => {
    let service: EntityAnnotationService;

    beforeAll(() => {
        service = new EntityAnnotationService();
    });

    it('should be created', () => {
        expect(service).toBeDefined();
    });

    it('should get NgxEntityMetadata', () => {
        expect(service.getAnnotation<EntityMetadata>(EntityMetadata, CustomRegisteredEntity)).toBeDefined();
    });

    it('should get NgxRelationMetadata', () => {
        expect(service.getAnnotation(RelationMetadata, CustomRegisteredEntity, 'otherItem')).toBeDefined();
    });

    it('should get NgxEntityMetadata by heritance', () => {
        expect(service.getAnnotation<EntityMetadata>(EntityMetadata, RegisteredEntityChild)).toBeDefined();
    });

    it('should get NgxRelationMetadata by heritance', () => {
        expect(service.getAnnotation(RelationMetadata, RegisteredEntityChild, 'otherItem')).toBeDefined();
    });

    it('should get BaseType', () => {
        expect(service.getBaseType(CustomRegisteredEntity)).toBe(CustomRegisteredEntity);
        expect(service.getBaseType(RegisteredEntityChild)).toBe(CustomRegisteredEntity);
    });
});
