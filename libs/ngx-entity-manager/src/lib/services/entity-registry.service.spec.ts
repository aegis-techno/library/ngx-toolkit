import {CustomRegisteredEntity} from '../../test/abstractEntity';
import {EntityRegistryService} from './entity-registry.service';

describe('NgxEntityRegistryService', () => {
    let service: EntityRegistryService;

    beforeAll(() => {
        service = new EntityRegistryService();
    });

    afterAll(() => {
        service.flush(CustomRegisteredEntity);
    });

    it('should be created', () => {
        expect(service).toBeDefined();
    });

    it('should add entity', () => {
        const itemTest = new CustomRegisteredEntity();
        itemTest.id = 11;
        service.addInRegistry(itemTest);
        expect(CustomRegisteredEntity.findById(service.getId(CustomRegisteredEntity as any, itemTest))).toBe(itemTest);
    });

    it('should get entity by type and id', () => {
        const itemTest = new CustomRegisteredEntity();
        itemTest.id = 12;
        service.addInRegistry(itemTest);
        expect(
            service.getEntityFromTypeAndId<CustomRegisteredEntity>(
                CustomRegisteredEntity as any,
                service.getId(CustomRegisteredEntity as any, itemTest)
            )
        ).toBe(itemTest);
    });

    it('should get entity by type and id', () => {
        const itemTest = new CustomRegisteredEntity();
        itemTest.id = 13;
        service.addInRegistry(itemTest);
        expect(
            service.getEntitiesFromTypeAndPredicate<CustomRegisteredEntity>(
                CustomRegisteredEntity as any,
                (entity: any) => entity.id === itemTest.id
            )[0]
        ).toBe(itemTest);
    });
});
