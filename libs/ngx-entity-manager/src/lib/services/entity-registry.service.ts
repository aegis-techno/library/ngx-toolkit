import {Exception, Objects, Singleton} from '@aegis-techno/ngx-core';
import {Injectable, Type} from '@angular/core';
import {EntityMetadata} from '../decorators/entity.decorator';
import {AbstractEntity} from '../models/abstract-entity.model';
import {NgxMap} from '../models/ngx-map.model';
import {EntityAnnotationService} from './entity-annotation.service';

@Singleton()
@Injectable()
export class EntityRegistryService {
    private entityStore: NgxMap<Type<AbstractEntity>, NgxMap<any, any>> = new NgxMap();

    public constructor(private readonly annotationService?: EntityAnnotationService) {
        if (Objects.isNotDefined(this.annotationService)) {
            this.annotationService = new EntityAnnotationService();
        }
    }

    private static addInstanceWithIdInMap(instance: AbstractEntity, id: string, map: Map<any, any>) {
        if (!map.has(id) || map.get(id).constructor !== instance.constructor) {
            map.set(id, instance);
        } else {
            console.warn('Another instance exists for this instance', instance);
        }
    }

    public addInRegistry(instance: AbstractEntity): void {
        const entityAnnotationService = new EntityAnnotationService();
        const constructor: Type<AbstractEntity> = entityAnnotationService.getBaseType(
            instance.constructor as typeof AbstractEntity
        );

        const id = this.getId(constructor, instance);
        if (Objects.isDefined(id)) {
            if (!this.entityStore.has(constructor)) {
                this.addMapInStore(constructor);
            }
            EntityRegistryService.addInstanceWithIdInMap(instance, id, this.entityStore.getOrThrow(constructor));
        } else {
            console.error('Attempting to store an object with an invalid id', instance);
        }
    }

    public getEntityFromTypeAndId<U>(type: typeof AbstractEntity, id: any): U | null {
        if (id === undefined) {
            return null;
        }
        const entityAnnotationService = new EntityAnnotationService();
        const baseType = entityAnnotationService.getBaseType(type);
        if (this.entityStore.has(baseType)) {
            return this.entityStore.getOrThrow(baseType).get(id.toString()) as U;
        }

        return null;
    }

    public getEntitiesFromTypeAndPredicate<U>(type: typeof AbstractEntity, predicate?: (entity: U) => boolean): Array<U> {
        const entityAnnotationService = new EntityAnnotationService();
        const baseType = entityAnnotationService.getBaseType(type);
        const collection = new Array<U>();
        if (this.entityStore.has(baseType)) {
            this.entityStore.getOrThrow(baseType).forEach((entity: U) => {
                if (!Objects.isFunction(predicate) || (Objects.isDefined(predicate) && predicate(entity))) {
                    collection.push(entity);
                }
            });
        }

        return collection;
    }

    public getId(constructor: Type<AbstractEntity>, instance: AbstractEntity | any): string {
        const entityAnnotationService = new EntityAnnotationService();
        if (!entityAnnotationService.hasAnnotation(EntityMetadata, constructor)) {
            throw new Exception('not.have.annotation', "don't have NgxEntityMetadata annotation");
        }

        const ids: any[] = [];
        const annotation = entityAnnotationService.getAnnotation<EntityMetadata>(EntityMetadata, constructor);
        if (Objects.isNotDefined(annotation)) {
            throw new Exception('not.have.annotation', "don't have NgxENtityMetadata annotation");
        }
        annotation.options.primaryKeys.forEach((key: string) => {
            if (Objects.isDefined(instance[key])) {
                ids.push(instance[key]);
            }
        });

        if (ids.length === 0) {
            throw new Exception('not.have.primary-key', "don't have primaryKeys");
        }

        return ids.join('-');
    }

    public flush(entities?: Array<Type<AbstractEntity>> | Type<AbstractEntity>): void {
        if (entities && !Array.isArray(entities)) {
            entities = [entities];
        }
        const entityAnnotationService = new EntityAnnotationService();

        this.entityStore.forEach((typeMap, key) => {
            if (entities && Array.isArray(entities)) {
                if (entities.indexOf(entityAnnotationService.getBaseType(key)) > -1) {
                    typeMap.clear();
                }
            } else {
                typeMap.clear();
            }
        });
    }

    private addMapInStore(constructor: any) {
        this.entityStore.set(constructor, new NgxMap());
    }
}
